## 安装
```$xslt
npm install 或者
yarn install
```

## 配置修改
```$xslt
public\cdn\env.json

{
  "development":{
    "ssoPrefixUrl": "http://localhost:8400/sso-server",
    "wsUrl": "ws://localhost:8879",
    "flowUrl": "http://localhost:8080/flowable-engine",
    "onlinePreviewUrl": "http://192.168.1.110:8012"

  },
  "test":{
    "ssoPrefixUrl": "http://192.168.1.191:8400/sso-server",
    "wsUrl": "ws://localhost:8879",
    "flowUrl": "http://localhost:8080/flowable-engine",
    "onlinePreviewUrl": "http://192.168.1.110:8012"
  },
  "production":{
    "ssoPrefixUrl": "http://localhost:8400/sso-server",
    "wsUrl": "ws://localhost:8879",
    "flowUrl": "http://localhost:8080/flowable-engine",
    "onlinePreviewUrl": "http://192.168.1.110:8012"
  }
}


```
## 启动
```$xslt
yarn run serve   开发环境
yarn run dev      开发环境
yarn run test      开发环境
yarn run prod       生产环境

```
## 构建
```$xslt
yarn run build       生产构建
yarn run build:prod  生产构建
yarn run build:test  测试构建
yarn run build:dev   开发构建

```
