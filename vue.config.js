let target = 'http://192.168.1.115:8080';

// let target = 'http://192.168.1.191:8080';
// let target = 'http://192.168.1.177:8080';
// let target = 'http://121.36.100.47:8080';
// http://192.168.1.133:8210/column/forumTheme

const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  //路径前缀
  publicPath: "/",
  lintOnSave: true,
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src')
      },
      modules: [path.resolve('node_modules'), 'node_modules'],
    }
  },
  chainWebpack: (config) => {
    //忽略的打包文件
    config.externals({
      'vue': 'Vue',
      'vue-router': 'VueRouter',
      'vuex': 'Vuex',
      'axios': 'axios',
      'element-ui': 'ELEMENT',
    });
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    config.plugins.delete('prefetch')

    config.module
      .rule('swf')
      .test(/\.swf$/)
      .use('url-loader')
      .loader('url-loader')
      .options({limit: 1000})
      .end()
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
            chunks: 'all',
            cacheGroups: {
              libs: {
                name: 'chunk-libs',
                test: /[\\/]node_modules[\\/]/,
                priority: 10,
                chunks: 'initial'
              },
              elementUI: {
                name: 'chunk-elementUI',
                priority: 20,
                test: /[\\/]node_modules[\\/]_?element-ui(.*)/
              },
              commons: {
                name: 'chunk-commons',
                test: resolve('src/components'),
                minChunks: 3,
                priority: 5,
                reuseExistingChunk: true
              }
            }
          })
          config.optimization.runtimeChunk('single')
        }
      )

    const entry = config.entry('app');
    entry.add('babel-polyfill').end();
    entry.add('classlist-polyfill').end();
    entry.add('@/mock').end();
  },
  devServer: {
    port: 1890,
    proxy: {
      '/api/flowable-engine': {
        target: target,
        ws: true,
        pathRewrite: {
          '^/api/flowable-engine': '/flowable-engine'
        }
      },
      // '/api/xpaas-resource': {
      //     target: 'http://localhost:8010',
      //     ws: true,
      //     pathRewrite: {
      //         '^/api/xpaas-resource': '/'
      //     }
      // },
      // 施训管理模块
      '/api/trainingManagement': {
          target: "http://hzimo.xyz/",
          ws: true,
          pathRewrite: {
          '^/api/trainingManagement': ''
          }
        },
      '/api': {
        target: target,
        ws: true,
        // logLevel: 'debug',
        pathRewrite: {
          '^/api': '/'
        }
      },
    }
  }
};
