import Layout from '@/page/index/'
export default [
    // ============================== 施训管理 =============================
    /**
     * 施训管理模块
     */


    // 1 制定专业班次学期执行计划-----------------------------------------------------------------------------
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/termExecutionPlan',
        children: [{
            path: 'termExecutionPlan',
            name: '制定专业班次学期执行计划',
            meta: {
                $keepAlive: false,
                i18n: 'info',
                title: "制定专业班次学期执行计划"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/termExecutionPlan')
        }]
    },
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/planImport',
        children: [{
            path: 'planImport',
            name: '批量从培养方案导入',
            meta: {
                i18n: 'info',
                title: "批量从培养方案导入"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/planImport')
        }]
    },
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/planImportConfirm',
        children: [{
            path: 'planImportConfirm',
            name: '批量从培养方案导入',
            meta: {
                i18n: 'info',
                title: "批量从培养方案导入"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/planImportConfirm')
        }]
    },
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/profCourse',
        children: [{
            path: 'profCourse',
            name: '专业班次课程',
            meta: {
                i18n: 'info',
                title: "专业班次课程"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/profCourse')
        }]
    },
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/beforeExecutePlan',
        children: [{
            path: 'beforeExecutePlan',
            name: '以往班次执行计划',
            meta: {
                i18n: 'info',
                title: "以往班次执行计划"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/beforeExecutePlan')
        }]
    },
    {
        path: "/admin/teachingTaskManage",
        component: Layout,
        redirect: '/admin/teachingTaskManage/addCourse',
        children: [{
            path: 'addCourse',
            name: '添加课程',
            meta: {
                i18n: 'info',
                title: "添加课程"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/addCourse')
        }]
    },

    // 2 教学班管理-----------------------------------------------------------------------------
    {
        path: "/admin/teachingTaskManage/teachingclass",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/index',
        children: [{
            path: 'index',
            name: '教学班管理',
            meta: {
                i18n: 'info',
                title: "教学班管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/index')
        }]
    },
    {
        path: "/admin/teachingTaskManage/teachingclass",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/teachingResearchOffice',
        children: [{
            path: 'teachingResearchOffice',
            name: '教学班管理',
            meta: {
                i18n: 'info',
                title: "教学班管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/teachingResearchOffice')
        }]
    },
    {
        path: "/admin/teachingTaskManage/teachingclass",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/setTeachingClass/:id',
        children: [{
            path: 'setTeachingClass',
            name: '设置教学班',
            meta: {
                i18n: 'info',
                title: "设置教学班"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/setTeachingClass')
        }]
    },
    {
        path: "/admin/teachingTaskManage/teachingclass",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/editTeachClassInfo',
        children: [{
            path: 'editTeachClassInfo',
            name: '修改教学班信息',
            meta: {
                i18n: 'info',
                title: "修改教学班信息"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/editTeachClassInfo')
        }]
    },
    {
        path: "/admin/teachingTaskManage/teachingclass",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/insertTeachClassInfo',
        children: [{
            path: 'insertTeachClassInfo',
            name: '新增教学班信息',
            meta: {
                i18n: 'info',
                title: "新增教学班信息"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/editTeachClassInfo')
        }]
    },

    // 3 ===================================================================================================================

    /*管理员 审核教学任务 审核教学任务 */
    {
        path: "/admin/teachingTaskManage/checkteachingtask/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/checkteachingtask/index',
        children: [{
            path: 'index',
            name: '审核教学任务',
            meta: {
                i18n: 'info',
                title: "审核教学任务"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/checkteachingtask/index')
        }]
    },

    // /*管理员 审核教学任务 教材信息核对 */
    // {
    //     path: "/admin/teachingTaskManage/checkteachingtask/",
    //     component: Layout,
    //     redirect: '/admin/teachingTaskManage/checkteachingtask/taskAndBookInfoCheck',
    //     children: [{
    //         path: 'taskAndBookInfoCheck',
    //         name: '审核教学任务',
    //         meta: {
    //             i18n: 'info',
    //             title: "审核教学任务"
    //         },
    //         component: () =>
    //             import('@/views/admin/trainingManagement/teachingTaskManage/checkteachingtask/taskAndBookInfoCheck')
    //     }]
    // },

    // /*管理员 审核教学任务 教学任务和教材信息审核 */
    // {
    //     path: "/admin/teachingTaskManage/checkteachingtask/",
    //     component: Layout,
    //     redirect: '/admin/teachingTaskManage/checkteachingtask/taskAndBookInfoAudit',
    //     children: [{
    //         path: 'taskAndBookInfoAudit',
    //         name: '审核教学任务',
    //         meta: {
    //             i18n: 'info',
    //             title: "审核教学任务"
    //         },
    //         component: () =>
    //             import('@/views/admin/trainingManagement/teachingTaskManage/checkteachingtask/taskAndBookInfoAudit')
    //     }]
    // },

    /*管理员 审核教学任务 添加教材 */
    {
        path: "/admin/teachingTaskManage/teachingclass/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/addTextbook',
        children: [{
            path: 'addTextbook',
            name: '添加教材',
            meta: {
                i18n: 'info',
                title: "添加教材"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/addTextbook')
        }]
    },

    /*管理员 审核教学任务 添加教材 */
    {
        path: "/admin/teachingTaskManage/teachingclass/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/editTextbook',
        children: [{
            path: 'editTextbook',
            name: '修改教材',
            meta: {
                i18n: 'info',
                title: "修改教材"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/addTextbook')
        }]
    },

    /*管理员 审核教学任务 查看教学班信息 */
    {
        path: "/admin/teachingTaskManage/teachingclass/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/viewClassInformation',
        children: [{
            path: 'viewClassInformation',
            name: '查看教学班信息',
            meta: {
                i18n: 'info',
                title: "查看教学班信息"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/viewClassInformation')
        }]
    },

    /*管理员 审核教学任务 最后修改人 */
    {
        path: "/admin/teachingTaskManage/teachingclass/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/LastModified',
        children: [{
            path: 'LastModified',
            name: '最后修改人',
            meta: {
                i18n: 'info',
                title: "最后修改人"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/LastModified')
        }]
    },

    /*管理员 审核教学任务 教材库管理 */
    {
        path: "/admin/teachingTaskManage/teachingclass/",
        component: Layout,
        redirect: '/admin/teachingTaskManage/teachingclass/TextbookSelect',
        children: [{
            path: 'TextbookSelect',
            name: '教材库管理',
            meta: {
                i18n: 'info',
                title: "教材库管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/teachingTaskManage/teachingclass/TextbookSelect')
        }]
    },

    // 4 ===================================================================================================================

    /*管理员 教学任务查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/teachingTaskQuery',
        children: [{
            path: 'teachingTaskQuery',
            name: '教学任务查询',
            meta: {
                i18n: 'info',
                title: "教学任务查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/teachingTaskQuery')
        }]
    },

    // 5 ===================================================================================================================

    /*管理员 不排课时间 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/noClassSchedule',
        children: [{
            path: 'noClassSchedule',
            name: '不排课时间',
            meta: {
                i18n: 'info',
                title: "不排课时间"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/noClassSchedule')
        }]
    },
    // 6 ===================================================================================================================

    /*管理员 不检测冲突教学场地管理 */
    {
        path: "/admin/place",
        component: Layout,
        redirect: '/admin/place/teachingSiteConflict',
        children: [{
            path: 'teachingSiteConflict',
            name: '不检测冲突教学场地管理',
            meta: {
                i18n: 'info',
                title: "不检测冲突教学场地管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/place/teachingSiteConflict')
        }]
    },

    // /*管理员 不检测冲突教学场地管理 */
    // {
    //     path: "/admin/place",
    //     component: Layout,
    //     redirect: '/admin/place/teachingSiteConflict',
    //     children: [{
    //         path: 'teachingSiteConflict',
    //         name: '不排课时间',
    //         meta: {
    //             i18n: 'info',
    //             title: "不排课时间"
    //         },
    //         component: () =>
    //             import('@/views/admin/trainingManagement/place/teachingSiteConflict')
    //     }]
    // },

    // 7 ===================================================================================================================

    /*管理员 排课 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/workOutScheduleTeaching',
        children: [{
            path: 'workOutScheduleTeaching',
            name: '排课',
            meta: {
                i18n: 'info',
                title: "排课"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/workOutScheduleTeaching')
        }]
    },

    /*管理员 推荐场地 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/recommendedSite',
        children: [{
            path: 'recommendedSite',
            name: '推荐场地',
            meta: {
                i18n: 'info',
                title: "推荐场地"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/recommendedSite')
        }]
    },

    // 8 ===================================================================================================================

    /*管理员 调课 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/courseAdjustment',
        children: [{
            path: 'courseAdjustment',
            name: '调课',
            meta: {
                i18n: 'info',
                title: "调课"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/courseAdjustment')
        }]
    },

    // 9 ===================================================================================================================

    /*管理员 教研室排课情况统计 */
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/workOutTeaching',
        children: [{
            path: 'workOutTeaching',
            name: '教研室排课情况统计',
            meta: {
                i18n: 'info',
                title: "教研室排课情况统计"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/workOutTeaching')
        }]
    },

    /*管理员 教学任务详情 */
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/teachingTaskDetails',
        children: [{
            path: 'teachingTaskDetails',
            name: '教学任务详情',
            meta: {
                i18n: 'info',
                title: "教学任务详情"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/teachingTaskDetails')
        }]
    },

    // 10 ===================================================================================================================

    /*管理员 课表安排学时统计*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/scheduleClassHours',
        children: [{
            path: 'scheduleClassHours',
            name: '课表安排学时统计',
            meta: {
                i18n: 'info',
                title: "课表安排学时统计"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/scheduleClassHours')
        }]
    },

    /*管理员 上课门数详情*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/numberCourseDetails',
        children: [{
            path: 'numberCourseDetails',
            name: '上课门数详情',
            meta: {
                i18n: 'info',
                title: "上课门数详情"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/numberCourseDetails')
        }]
    },

    /*管理员 教员数详情*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/numberTeachers',
        children: [{
            path: 'numberTeachers',
            name: '教员数详情',
            meta: {
                i18n: 'info',
                title: "教员数详情"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/numberTeachers')
        }]
    },

    // 11 ===================================================================================================================

    /*管理员 教员工作量统计*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/teacherWorkload',
        children: [{
            path: 'teacherWorkload',
            name: '教员工作量统计',
            meta: {
                i18n: 'info',
                title: "教员工作量统计"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/teacherWorkload')
        }]
    },

    // 12 ===================================================================================================================

    /*管理员 学时出入情况统计*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/classHours',
        children: [{
            path: 'classHours',
            name: '学时出入情况统计',
            meta: {
                i18n: 'info',
                title: "学时出入情况统计"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/classHours')
        }]
    },

    // 13 ===================================================================================================================

    /*管理员 空闲场地查询*/
    {
        path: "/admin/place",
        component: Layout,
        redirect: '/admin/place/freeSpaceQuery',
        children: [{
            path: 'freeSpaceQuery',
            name: '空闲场地查询',
            meta: {
                i18n: 'info',
                title: "空闲场地查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/place/freeSpaceQuery')
        }]
    },

    // 14 ===================================================================================================================

    /*管理员 场地使用登记*/
    {
        path: "/admin/place",
        component: Layout,
        redirect: '/admin/place/usedRegister',
        children: [{
            path: 'usedRegister',
            name: '场地使用登记',
            meta: {
                i18n: 'info',
                title: "场地使用登记"
            },
            component: () =>
                import('@/views/admin/trainingManagement/place/usedRegister')
        }]
    },

    /*管理员 修改/添加场地使用登记*/
    {
        path: "/admin/place",
        component: Layout,
        redirect: '/admin/place/addUsedRegister',
        children: [{
            path: 'addUsedRegister',
            name: '修改/添加场地使用登记',
            meta: {
                i18n: 'info',
                title: "修改/添加场地使用登记"
            },
            component: () =>
                import('@/views/admin/trainingManagement/place/addUsedRegister')
        }]
    },

    // 15 ===================================================================================================================

    /*管理员 选修课开课管理 */
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/startClassManagement',
        children: [{
            path: 'startClassManagement',
            name: '选修课开课管理',
            meta: {
                i18n: 'info',
                title: "选修课开课管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/startClassManagement')
        }]
    },
    /*管理员 选修课开课管理 添加选修课 */
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/addElectiveCourses',
        children: [{
            path: 'addElectiveCourses',
            name: '添加选修课',
            meta: {
                i18n: 'info',
                title: "添加选修课"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/addElectiveCourses')
        }]
    },
    /*管理员 选修课开课管理 添加选修课 下一步*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/generateElectiveCourses',
        children: [{
            path: 'generateElectiveCourses',
            name: '生成所选选修课教学班信息',
            meta: {
                i18n: 'info',
                title: "生成所选选修课教学班信息"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/generateElectiveCourses')
        }]
    },
    /*管理员 选修课开课管理 从培养计划添加选修课 */
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/addElectiveCoursesFormPlan',
        children: [{
            path: 'addElectiveCoursesFormPlan',
            name: '从培养计划添加选修课',
            meta: {
                i18n: 'info',
                title: "从培养计划添加选修课"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/addElectiveCoursesFormPlan')
        }]
    },
    /*管理员 选修课开课管理 从培养计划添加选修课 下一步*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/generateElectiveCoursesPlan',
        children: [{
            path: 'generateElectiveCoursesPlan',
            name: '生成所选选修课教学班信息',
            meta: {
                i18n: 'info',
                title: "生成所选选修课教学班信息"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/generateElectiveCoursesPlan')
        }]
    },
    /*管理员 选修课开课管理 设置选修课要求*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/setClassRequire',
        children: [{
            path: 'setClassRequire',
            name: '设置选修课要求',
            meta: {
                i18n: 'info',
                title: "设置选修课要求"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/setClassRequire')
        }]
    },

    /*管理员 选修课开课管理 设置正规或预选人员*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/setPrimaryPeople',
        children: [{
            path: 'setPrimaryPeople',
            name: '设置正选或预选人员',
            meta: {
                i18n: 'info',
                title: "设置正选或预选人员"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/setPrimaryPeople')
        }]
    },

    /*管理员 选修课开课管理 修改*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/editInfo',
        children: [{
            path: 'editInfo',
            name: '修改',
            meta: {
                i18n: 'info',
                title: "修改"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/editInfo')
        }]
    },

    /*管理员 选修课开课管理 排课*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/workOutClass',
        children: [{
            path: 'workOutClass',
            name: '排课',
            meta: {
                i18n: 'info',
                title: "排课"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/workOutClass')
        }]
    },
    /*管理员 选修课开课管理 批量设置主讲教员*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/batchSettingTeacher',
        children: [{
            path: 'batchSettingTeacher',
            name: '批量设置主讲教员',
            meta: {
                i18n: 'info',
                title: "批量设置主讲教员"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/batchSettingTeacher')
        }]
    },

    /*管理员 选修课开课管理 选择教室场地*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/selectClassRoom',
        children: [{
            path: 'selectClassRoom',
            name: '选择教室场地',
            meta: {
                i18n: 'info',
                title: "选择教室场地"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/selectClassRoom')
        }]
    },

    /*管理员 选修课开课管理 调整学员*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/adjustmentStudents',
        children: [{
            path: 'adjustmentStudents',
            name: '调整学员',
            meta: {
                i18n: 'info',
                title: "调整学员"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/adjustmentStudents')
        }]
    },

    /*管理员 选修课开课管理 转教学班*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/turnClass',
        children: [{
            path: 'turnClass',
            name: '转教学班',
            meta: {
                i18n: 'info',
                title: "转教学班"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/turnClass')
        }]
    },

    /*管理员 选修课开课管理 添加学员*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/addStudent',
        children: [{
            path: 'addStudent',
            name: '添加学员',
            meta: {
                i18n: 'info',
                title: "添加学员"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/addStudent')
        }]
    },

    /*管理员 选修课开课管理 教学班查看*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/teachingView',
        children: [{
            path: 'teachingView',
            name: '教学班查看',
            meta: {
                i18n: 'info',
                title: "教学班查看"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/teachingView')
        }]
    },

    /*管理员 选修课开课管理 查看课程*/
    {
        path: "/admin/elective",
        component: Layout,
        redirect: '/admin/elective/classNumberView',
        children: [{
            path: 'classNumberView',
            name: '查看课程',
            meta: {
                i18n: 'info',
                title: "查看课程"
            },
            component: () =>
                import('@/views/admin/trainingManagement/elective/classNumberView')
        }]
    },

    // 16 ===================================================================================================================
    /*管理员 选课批次管理 */
    {
        path: "/admin/course/selection",
        component: Layout,
        redirect: '/admin/course/selection/courseBatchManagement',
        children: [{
            path: 'courseBatchManagement',
            name: '选课批次管理',
            meta: {
                i18n: 'info',
                title: "选课批次管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/selection/courseBatchManagement')
        }]
    },

    /*管理员 增加选课批次 */
    {
        path: "/admin/course/selection",
        component: Layout,
        redirect: '/admin/course/selection/addCourseSelection',
        children: [{
            path: 'addCourseSelection',
            name: '增加选课批次',
            meta: {
                i18n: 'info',
                title: "增加选课批次"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/selection/addCourseSelection')
        }]
    },

    /*管理员 设置选课要求 */
    {
        path: "/admin/course/selection",
        component: Layout,
        redirect: '/admin/course/selection/setCourseRequirement/:xkpcId',
        children: [{
            path: 'setCourseRequirement',
            name: '设置选课要求',
            meta: {
                i18n: 'info',
                title: "设置选课要求"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/selection/setCourseRequirement')
        }]
    },

    /*管理员 设置教学班 */
    {
        path: "/admin/course/selection",
        component: Layout,
        redirect: '/admin/course/selection/setTeachingClass/:xkpcId',
        children: [{
            path: 'setTeachingClass',
            name: '设置教学班',
            meta: {
                i18n: 'info',
                title: "设置教学班"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/selection/setTeachingClass')
        }]
    },
  {
    path:'/admin/course/selection',
    component: Layout,
    redirect: '/admin/course/selection/addTeachingClass/:xkpcId',
    children: [{
      path: 'addTeachingClass',
      name: '添加教学班',
      meta: {
        i18n: 'info',
        title: "添加教学班"
      },
      component: () =>
        import('@/views/admin/trainingManagement/course/selection/addTeachingClass')
    }]
  },

    /*管理员 选课学员管理 */
    {
        path: "/admin/course/selection",
        component: Layout,
        redirect: '/admin/course/selection/studentManagement',
        children: [{
            path: 'studentManagement',
            name: '选课学员管理',
            meta: {
                $keepAlive: true,
                i18n: 'info',
                title: "选课学员管理"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/selection/studentManagement')
        }]
    },

    // 17 ===================================================================================================================

    /*管理员 选课门次少于N门的学员 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/studentsLessThanN',
        children: [{
            path: 'studentsLessThanN',
            name: '选课门次少于N门的学员',
            meta: {
                i18n: 'info',
                title: "选课门次少于N门的学员"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/studentsLessThanN')
        }]
    },

    // 18 ===================================================================================================================
    /*管理员 互联网选修课登记 学员*/
    {
        path: "/admin/course/elective",
        component: Layout,
        redirect: '/admin/course/elective/electiveCourseRegistration',
        children: [{
            path: 'electiveCourseRegistration',
            name: '互联网选修课登记',
            meta: {
                i18n: 'info',
                title: "互联网选修课登记"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/elective/electiveCourseRegistration')
        }]
    },
    // 19 ===================================================================================================================

    /*管理员 互联网选修课登记审核*/
    {
        path: "/admin/course/elective",
        component: Layout,
        redirect: '/admin/course/elective/registrationReview',
        children: [{
            path: 'registrationReview',
            name: '互联网选修课审核',
            meta: {
                i18n: 'info',
                title: "互联网选修课审核"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/elective/registrationReview')
        }]
    },

    // 20 ===================================================================================================================

    /*管理员 选修课开课情况统计*/
    {
        path: "/admin/statistic",
        component: Layout,
        redirect: '/admin/statistic/electiveCoursesOpen',
        children: [{
            path: 'electiveCoursesOpen',
            name: '选修课开课情况统计',
            meta: {
                i18n: 'info',
                title: "选修课开课情况统计"
            },
            component: () =>
                import('@/views/admin/trainingManagement/statistic/electiveCoursesOpen')
        }]
    },

    // 21 ===================================================================================================================

    /*管理员 网上选课 学员*/
    {
        path: "/student/course",
        component: Layout,
        redirect: '/student/course/courseSelection',
        children: [{
            path: 'courseSelection',
            name: '网上选课',
            meta: {
                i18n: 'info',
                title: "网上选课"
            },
            component: () =>
                import('@/views/student/trainingManagement/course/courseSelection')
        }]
    },
    /*管理员 进入选课 学员*/
    {
        path: "/student/course",
        component: Layout,
        redirect: '/student/course/enterCourseSelection',
        children: [{
            path: 'enterCourseSelection',
            name: '进入选课',
            meta: {
                i18n: 'info',
                title: "进入选课"
            },
            component: () =>
                import('@/views/student/trainingManagement/course/enterCourseSelection')
        }]
    },

    /*管理员 查看已选课程 学员*/
    {
        path: "/student/course",
        component: Layout,
        redirect: '/student/course/courseSelectionView',
        children: [{
            path: 'courseSelectionView',
            name: '查看已选课程',
            meta: {
                i18n: 'info',
                title: "查看已选课程"
            },
            component: () =>
                import('@/views/student/trainingManagement/course/courseSelectionView')
        }]
    },

    // 22 ===================================================================================================================

    /*管理员 选修课选课情况查询*/
    {
        path: "/admin/course/elective",
        component: Layout,
        redirect: '/admin/course/elective/courseSelectionQuery',
        children: [{
            path: 'courseSelectionQuery',
            name: '选修课选课情况查询',
            meta: {
                i18n: 'info',
                title: "选修课选课情况查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/elective/courseSelectionQuery')
        }]
    },
    /*管理员 本队上课人数*/
    {
        path: "/admin/course/elective",
        component: Layout,
        redirect: '/admin/course/elective/teamNumber',
        children: [{
            path: 'teamNumber',
            name: '本队上课人数',
            meta: {
                i18n: 'info',
                title: "本队上课人数"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/elective/teamNumber')
        }]
    },

    // 23 ===================================================================================================================

    // /*管理员 课表查询 学员*/
    // {
    //     path: "/student/course",
    //     component: Layout,
    //     redirect: '/student/course/queryTimetable',
    //     children: [{
    //         path: 'queryTimetable',
    //         name: '课表查询',
    //         meta: {
    //             i18n: 'info',
    //             title: "课表查询"
    //         },
    //         component: () =>
    //             import('@/views/student/trainingManagement/course/queryTimetable')
    //     }]
    // },

    /*管理员 课表查询 队长*/
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/queryTimetable',
        children: [{
            path: 'queryTimetable',
            name: '课表查询',
            meta: {
                i18n: 'info',
                title: "课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/queryTimetable')
        }]
    },

    // 24 ===================================================================================================================

    /*管理员 时间段课表查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/timeScheduleQuery',
        children: [{
            path: 'timeScheduleQuery',
            name: '时间段课表查询',
            meta: {
                i18n: 'info',
                title: "时间段课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/timeScheduleQuery')
        }]
    },

    // 25 ===================================================================================================================

    // /*管理员 专业班次课表查询 */
    // {
    //     path: "/admin/course",
    //     component: Layout,
    //     redirect: '/admin/course/professionalScheduleQuery',
    //     children: [{
    //         path: 'professionalScheduleQuery',
    //         name: '专业班次课表查询',
    //         meta: {
    //             i18n: 'info',
    //             title: "专业班次课表查询"
    //         },
    //         component: () =>
    //             import('@/views/admin/trainingManagement/course/professionalScheduleQuery')
    //     }]
    // },
    /*管理员 专业班次课表查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/professionalScheduleQueryMain',
        children: [{
            path: 'professionalScheduleQueryMain',
            name: '专业班次课表查询',
            meta: {
                i18n: 'info',
                title: "专业班次课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/professionalScheduleQueryMain')
        }]
    },

    // 26 ===================================================================================================================

    /*管理员 场地课表查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/venueScheduleQuery',
        children: [{
            path: 'venueScheduleQuery',
            name: '场地课表查询',
            meta: {
                i18n: 'info',
                title: "场地课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/venueScheduleQuery')
        }]
    },

    // 27 ===================================================================================================================

    /*管理员 部门课表查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/departmentScheduleQuery',
        children: [{
            path: 'departmentScheduleQuery',
            name: '部门课表查询',
            meta: {
                i18n: 'info',
                title: "部门课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/departmentScheduleQuery')
        }]
    },
    /*管理员 部门课表详情 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/departmentScheduleDetails',
        children: [{
            path: 'departmentScheduleDetails',
            name: '部门课表详情',
            meta: {
                i18n: 'info',
                title: "部门课表详情"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/departmentScheduleDetails')
        }]
    },

    // 28 ===================================================================================================================

    /*管理员 教员课表查询 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/teacherScheduleQuery',
        children: [{
            path: 'teacherScheduleQuery',
            name: '教员课表查询',
            meta: {
                i18n: 'info',
                title: "教员课表查询"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/teacherScheduleQuery')
        }]
    },

    // 29 ===================================================================================================================

    /*管理员 选修课花名册打印 */
    {
        path: "/admin/course",
        component: Layout,
        redirect: '/admin/course/rosterPrinting',
        children: [{
            path: 'rosterPrinting',
            name: '选修课花名册打印',
            meta: {
                i18n: 'info',
                title: "选修课花名册打印"
            },
            component: () =>
                import('@/views/admin/trainingManagement/course/rosterPrinting')
        }]
    },



]
