import Layout from "@/page/index/";

export default [
  {
    path: "/systemPages",
    component: Layout,
    redirect: "/systemPages/chiefSpace", //重定向路由
    children: [
      {
        path: "homepage",
        name: "基地空间",
        meta: {
          i18n: "dashboard", //
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/systemPages/homepage"
          ),
      },
      {
        path: "chiefSpace",
        name: "首页",
        meta: {
          i18n: "chiefSpace", //配置中英文索引
          title: "首页",
          affix: true,
          nomenu: false,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/systemPages/chiefSpace"
          ),
      },
    ],
  },
  {
    path: "/student/home",
    component: Layout,
    redirect: "/student/home/index", //???
    children: [
      {
        path: "index",
        name: "表单",
        meta: {
          i18n: "table",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/home/suIndex"
          ),
      },
    ],
  },
  {
    path: "/info",
    component: Layout,
    redirect: "/info/index",
    children: [
      {
        path: "index",
        name: "个人信息",
        meta: {
          i18n: "info",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/system/userinfo"),
      },
    ],
  },

  //  学员
  {
    path: "/student/home",
    component: Layout,
    redirect: "/student/home/suIndex",
    children: [
      {
        path: "suIndex",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/home/suIndex"
          ),
      },
    ],
  },

  /*课程考核实施计划*/
  {
    path: "/student/assessmentPlan",
    component: Layout,
    redirect: "/student/assessmentPlan/detail",
    children: [
      {
        path: "detail",
        name: "申请变更课程考核实施计划详情",
        meta: {
          i18n: "info",
          title: "申请变更课程考核实施计划详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/assessment/detail"
          ),
      },
    ],
  },
  {
    path: "/student/assessment",
    component: Layout,
    redirect: "/student/assessment/plan",
    children: [
      {
        path: "plan",
        name: "课程考核实施计划查询",
        meta: {
          i18n: "info",
          title: "课程考核实施计划查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/assessment/plan"
          ),
      },
    ],
  },

  /*修改课程考核实施计划*/
  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/editPlan",
    children: [
      {
        path: "editPlan",
        name: "查看/修改课程考核实施计划",
        meta: {
          i18n: "info",
          title: "查看/修改课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/editPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/student/approvalDelayedExam",
    component: Layout,
    redirect: "/student/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考申请",
        meta: {
          i18n: "info",
          title: "修改缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/student/approvalDelayedExam",
    component: Layout,
    redirect: "/student/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },

  /*督导评价*/
  // 评教结果查看---学员
  {
    path: "/student/InformationInquiry",
    component: Layout,
    redirect: "/student/InformationInquiry/evaluationTeaching",
    children: [
      {
        path: "evaluationTeaching",
        name: "评教结果查看",
        meta: {
          i18n: "info",
          title: "评教结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },
  // 评教结果查看---大队长
  {
    path: "/student/InformationInquiry",
    component: Layout,
    redirect: "/student/InformationInquiry/duizhangEvaluation",
    children: [
      {
        path: "duizhangEvaluation",
        name: "评教结果查看",
        meta: {
          i18n: "info",
          title: "评教结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },
  // 评教结果查看---大队长
  {
    path: "/battalionChief/InformationInquiry",
    component: Layout,
    redirect: "/battalionChief/InformationInquiry/evaluationTeaching",
    children: [
      {
        path: "evaluationTeaching",
        name: "评教结果查看",
        meta: {
          i18n: "info",
          title: "评教结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },

  //教员
  {
    path: "/teacher/home",
    component: Layout,
    redirect: "/teacher/home/teIndex",
    children: [
      {
        path: "teIndex",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/home/teIndex"
          ),
      },
    ],
  },

  /*填报课程考核实施计划*/
  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/plan",
    children: [
      {
        path: "plan",
        name: "填报课程考核实施计划",
        meta: {
          i18n: "info",
          title: "填报课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/plan"
          ),
      },
    ],
  },

  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/editPlan",
    children: [
      {
        path: "editPlan",
        name: "查看/修改课程考核实施计划",
        meta: {
          i18n: "info",
          title: "查看/修改课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/editPlan"
          ),
      },
    ],
  },

  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加课程考核实施计划",
        meta: {
          i18n: "info",
          title: "增加课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/addPlan"
          ),
      },
    ],
  },

  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/changePlan",
    children: [
      {
        path: "changePlan",
        name: "申请变更课程考核实施计划",
        meta: {
          i18n: "info",
          title: "申请变更课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/changePlan"
          ),
      },
    ],
  },
  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/changeRecord",
    children: [
      {
        path: "changeRecord",
        name: "课程考核实施计划变更申请记录",
        meta: {
          i18n: "info",
          title: "课程考核实施计划变更申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/changeRecord"
          ),
      },
    ],
  },
  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/changeRecord2",
    children: [
      {
        path: "changeRecord2",
        name: "课程考核实施计划变更申请记录",
        meta: {
          i18n: "info",
          title: "课程考核实施计划变更申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/changeRecord2"
          ),
      },
    ],
  },
  {
    path: "/teacher/assessment",
    component: Layout,
    redirect: "/teacher/assessment/changePlanDetail",
    children: [
      {
        path: "changePlanDetail",
        name: "申请变更课程考核实施计划详情",
        meta: {
          i18n: "info",
          title: "申请变更课程考核实施计划详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/assessment/changePlanDetail"
          ),
      },
    ],
  },

  /*录入、打印课程成绩*/
  // {
  //   path: "/teacher/courseAchievement",
  //   component: Layout,
  //   redirect: '/teacher/courseAchievement/printEntry',
  //   children: [{
  //     path: 'printEntry',
  //     name: '录入、打印课程成绩',
  //     meta: {
  //       i18n: 'info',
  //       title: "录入、打印课程成绩"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/teacher/courseAchievement/printEntry')
  //   }]
  // },
  {
    path: "/teacher/courseAchievement",
    component: Layout,
    redirect: "/teacher/courseAchievement/manualEntry",
    children: [
      {
        path: "manualEntry",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/courseAchievement/manualEntry"
          ),
      },
    ],
  },

  {
    path: "/teacher/courseAchievement",
    component: Layout,
    redirect: "/teacher/courseAchievement/barcodeEntry",
    children: [
      {
        path: "barcodeEntry",
        name: "扫描条形码录入",
        meta: {
          i18n: "info",
          title: "扫描条形码录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/courseAchievement/barcodeEntry"
          ),
      },
    ],
  },

  {
    path: "/teacher/courseAchievement",
    component: Layout,
    redirect: "/teacher/courseAchievement/entryResults",
    children: [
      {
        path: "entryResults",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/courseAchievement/entryResults"
          ),
      },
    ],
  },

  {
    path: "/teacher/courseAchievement",
    component: Layout,
    redirect: "/teacher/courseAchievement/makeupScoreEntry",
    children: [
      {
        path: "makeupScoreEntry",
        name: "补缓考成绩录入",
        meta: {
          i18n: "info",
          title: "补缓考成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/courseAchievement/makeupScoreEntry"
          ),
      },
    ],
  },

  {
    path: "/teacher/paperPrint",
    component: Layout,
    redirect: "/teacher/paperPrint/paperPrint",
    children: [
      {
        path: "paperPrint",
        name: "试卷册材料打印",
        meta: {
          i18n: "info",
          title: "试卷册材料打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/paperPrint/paperPrint"
          ),
      },
    ],
  },

  /*试卷命题和审批*/
  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/approval",
    children: [
      {
        path: "approval",
        name: "试卷命题和审批",
        meta: {
          i18n: "info",
          title: "试卷命题和审批",
        },
        component: () =>
          // import( /* webpackChunkName: "views" */ '@/views/teacher/propositionReview/approval')
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/approval"
          ),
      },
    ],
  },

  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/subpoena",
    children: [
      {
        path: "subpoena",
        name: "送印传票",
        meta: {
          i18n: "info",
          title: "送印传票",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/subpoena"
          ),
      },
    ],
  },

  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/paperAnalysis",
    children: [
      {
        path: "paperAnalysis",
        name: "试卷分析表",
        meta: {
          i18n: "info",
          title: "试卷分析表",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/paperAnalysis"
          ),
      },
    ],
  },

  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/propositionPlan",
    children: [
      {
        path: "propositionPlan",
        name: "考核命题计划",
        meta: {
          i18n: "info",
          title: "考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/propositionPlan"
          ),
      },
    ],
  },
  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/propositionaddPlan",
    children: [
      {
        path: "propositionaddPlan",
        name: "考核命题计划",
        meta: {
          i18n: "info",
          title: "填写考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/propositionaddPlan"
          ),
      },
    ],
  },

  /*督导评价*/
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/supervision",
    children: [
      {
        path: "supervision",
        name: "督导评教",
        meta: {
          i18n: "info",
          title: "督导评教",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/supervision"
          ),
      },
    ],
  },

  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/statistical",
    children: [
      {
        path: "statistical",
        name: "督导信息统计",
        meta: {
          i18n: "info",
          title: "督导信息统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/statistical"
          ),
      },
    ],
  },

  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "新增督导信息",
        meta: {
          i18n: "info",
          title: "新增督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/addSupervisionInformation"
          ),
      },
    ],
  },
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/addSupervisionInformations",
    children: [
      {
        path: "addSupervisionInformations",
        name: "修改督导信息",
        meta: {
          i18n: "info",
          title: "修改督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/addSupervisionInformations"
          ),
      },
    ],
  },
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/special",
    children: [
      {
        path: "special",
        name: "专项督导信息",
        meta: {
          i18n: "info",
          title: "专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/special"
          ),
      },
    ],
  },

  /*评教信息核对*/
  {
    path: "/teacher/teachingPlan",
    component: Layout,
    redirect: "/teacher/teachingPlan/infoCheck",
    children: [
      {
        path: "infoCheck",
        name: "评教信息核对",
        meta: {
          i18n: "info",
          title: "评教信息核对",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/teachingPlan/infoCheck"
          ),
      },
    ],
  },
  {
    path: "/teacher/teachingPlan",
    component: Layout,
    redirect: "/teacher/teachingPlan/leadershipInfoCheck",
    children: [
      {
        path: "leadershipInfoCheck",
        name: "评教信息核对",
        meta: {
          i18n: "info",
          title: "评教信息核对",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/teachingPlan/leadershipInfoCheck"
          ),
      },
    ],
  },
  {
    path: "/teacher/teachingPlan",
    component: Layout,
    redirect: "/teacher/teachingPlan/directorInfoCheck",
    children: [
      {
        path: "directorInfoCheck",
        name: "评教信息核对",
        meta: {
          i18n: "info",
          title: "评教信息核对",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/teachingPlan/directorInfoCheck"
          ),
      },
    ],
  },
  /*查看参评课程*/
  {
    path: "/teacher/eligible",
    component: Layout,
    redirect: "/teacher/eligible/eligibleCourse",
    children: [
      {
        path: "eligibleCourse",
        name: "查看参评课程",
        meta: {
          i18n: "info",
          title: "查看参评课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/eligible/eligibleCourse"
          ),
      },
    ],
  },

  /*填报课时量*/
  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/addHours",
    children: [
      {
        path: "addHours",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/addHours"
          ),
      },
    ],
  },

  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/classHoursAddList",
    children: [
      {
        path: "classHoursAddList",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/classHoursAddList"
          ),
      },
    ],
  },

  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/classHoursAddList",
    children: [
      {
        path: "classHoursList",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/classHoursAddList"
          ),
      },
    ],
  },

  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/propositionPlan",
    children: [
      {
        path: "propositionPlan",
        name: "考核命题计划",
        meta: {
          i18n: "info",
          title: "考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/propositionPlan"
          ),
      },
    ],
  },
  {
    path: "/teacher/propositionReview",
    component: Layout,
    redirect: "/teacher/propositionReview/propositionaddPlan",
    children: [
      {
        path: "propositionaddPlan",
        name: "考核命题计划",
        meta: {
          i18n: "info",
          title: "填写考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/propositionReview/propositionaddPlan"
          ),
      },
    ],
  },

  /*督导评价*/
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/supervision",
    children: [
      {
        path: "supervision",
        name: "督导评教",
        meta: {
          i18n: "info",
          title: "督导评教",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/supervision"
          ),
      },
    ],
  },

  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/statistical",
    children: [
      {
        path: "statistical",
        name: "督导信息统计",
        meta: {
          i18n: "info",
          title: "督导信息统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/statistical"
          ),
      },
    ],
  },

  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "新增督导信息",
        meta: {
          i18n: "info",
          title: "新增督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/addSupervisionInformation"
          ),
      },
    ],
  },
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/addSupervisionInformations",
    children: [
      {
        path: "addSupervisionInformations",
        name: "查看督导信息",
        meta: {
          i18n: "info",
          title: "查看督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/addSupervisionInformations"
          ),
      },
    ],
  },
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/special",
    children: [
      {
        path: "special",
        name: "专项督导信息",
        meta: {
          i18n: "info",
          title: "专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/special"
          ),
      },
    ],
  },

  /*评教信息核对*/
  {
    path: "/teacher/teachingPlan",
    component: Layout,
    redirect: "/teacher/teachingPlan/infoCheck",
    children: [
      {
        path: "infoCheck",
        name: "评教信息核对",
        meta: {
          i18n: "info",
          title: "评教信息核对",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/teachingPlan/infoCheck"
          ),
      },
    ],
  },

  /*查看参评课程*/
  {
    path: "/teacher/eligible",
    component: Layout,
    redirect: "/teacher/eligible/eligibleCourse",
    children: [
      {
        path: "eligibleCourse",
        name: "查看参评课程",
        meta: {
          i18n: "info",
          title: "查看参评课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/eligible/eligibleCourse"
          ),
      },
    ],
  },
  /* 查看学期院历 */
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/Viewcalendar",
    children: [
      {
        path: "Viewcalendar",
        name: "查看学期院历",
        meta: {
          i18n: "info",
          title: "查看学期院历",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Viewcalendar"
          ),
      },
    ],
  },

  /*填报课时量*/
  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/addHours",
    children: [
      {
        path: "addHours",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/addHours"
          ),
      },
    ],
  },

  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/classHoursAddList",
    children: [
      {
        path: "classHoursAddList",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/classHoursAddList"
          ),
      },
    ],
  },

  {
    path: "/teacher/classHours",
    component: Layout,
    redirect: "/teacher/classHours/classHoursAddList",
    children: [
      {
        path: "classHoursList",
        name: "填报课时量",
        meta: {
          i18n: "info",
          title: "填报课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/classHours/classHoursAddList"
          ),
      },
    ],
  },

  //课时量
  {
    path: "/teacher/workload",
    component: Layout,
    redirect: "/teacher/workload/ClassTimeQuery",
    children: [
      {
        path: "ClassTimeQuery",
        name: "课时量查询",
        meta: {
          i18n: "info",
          title: "课时量查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/ClassTimeQuery"
          ),
      },
    ],
  },

  /*安排考试(调整监考)*/
  // {
  //   path: "/teacher/arrange",
  //   component: Layout,
  //   redirect: '/teacher/arrange/test',
  //   children: [{
  //     path: 'test',
  //     name: '安排考试(调整监考)',
  //     meta: {
  //       i18n: 'info',
  //       title: "安排考试(调整监考)"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/teacher/arrange/test')
  //   }]
  // },
  {
    path: "/teacher/arrange",
    component: Layout,
    redirect: "/teacher/arrange/test",
    children: [
      {
        path: "test",
        name: "安排考试",
        meta: {
          i18n: "info",
          title: "安排考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Printage"
          ),
      },
    ],
  },

  /* 查看全部考试安排*/
  {
    path: "/teacher/workload",
    component: Layout,
    redirect: "/teacher/workload/examArrange",
    children: [
      {
        path: "examArrange",
        name: "安排考试",
        meta: {
          i18n: "info",
          title: "安排考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/workload/examArrange"
          ),
      },
    ],
  },

  /* 查看全部缓考安排*/
  {
    path: "/teacher/workload",
    component: Layout,
    redirect: "/teacher/workload/arrangement",
    children: [
      {
        path: "arrangement",
        name: "查看全部缓考安排",
        meta: {
          i18n: "info",
          title: "查看全部缓考安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/workload/arrangement"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/arrangeTestBatch",
    children: [
      {
        path: "arrangeTestBatch",
        name: "安排考试",
        meta: {
          i18n: "info",
          title: "安排考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/arrangeTestBatch"
          ),
      },
    ],
  },
  /* 学员查看全部缓考安排*/
  {
    path: "/student/workload",
    component: Layout,
    redirect: "/student/workload/arrangement",
    children: [
      {
        path: "arrangement",
        name: "补缓考安排",
        meta: {
          i18n: "info",
          title: "补缓考安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/workload/arrangement"
          ),
      },
    ],
  },

  /* 调整监考申请记录*/
  {
    path: "/teacher/workload",
    component: Layout,
    redirect: "/teacher/workload/AdjustInvigilators",
    children: [
      {
        path: "AdjustInvigilators",
        name: "调整监考申请记录",
        meta: {
          i18n: "info",
          title: "调整监考申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/workload/AdjustInvigilators"
          ),
      },
    ],
  },

  /*查看安排考试*/
  {
    path: "/teacher/examination",
    component: Layout,
    redirect: "/teacher/examination/examination",
    children: [
      {
        path: "examination",
        name: "查看全部安排考试",
        meta: {
          i18n: "info",
          title: "查看全部考试",
        },
        component: () =>
          // import( /* webpackChunkName: "views" */ '@/views/teacher/examination/examination')
          import(
            /* webpackChunkName: "views" */ "@/views/student/examination/examinationlist"
          ),
      },
    ],
  },

  /*学生查看安排考试*/
  {
    path: "/student/examination",
    component: Layout,
    redirect: "/student/examination/examination",
    children: [
      {
        path: "examination",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/examination/examination"
          ),
      },
    ],
  },

  /*学生查看网上评教*/
  {
    path: "/student/evaluation",
    component: Layout,
    redirect: "/student/evaluation/evaluation",
    children: [
      {
        path: "evaluation",
        name: "网上评教",
        meta: {
          i18n: "info",
          title: "网上评教",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/evaluation/evaluation"
          ),
      },
    ],
  },

  /*学生查看网上评教*/
  {
    path: "/student/evaluation",
    component: Layout,
    redirect: "/student/evaluation/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "网上评教",
        meta: {
          i18n: "info",
          title: "网上评教",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/evaluation/addSupervisionInformation"
          ),
      },
    ],
  },

  /*学生查看网上评教*/
  {
    path: "/student/evaluation",
    component: Layout,
    redirect: "/student/evaluation/supervision",
    children: [
      {
        path: "supervision",
        name: "网上评教",
        meta: {
          i18n: "info",
          title: "网上评教",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/evaluation/supervision"
          ),
      },
    ],
  },

  /*督导组长查看*/
  {
    path: "/teacher/SupervisionTeam",
    component: Layout,
    redirect: "/teacher/SupervisionTeam/supervision",
    children: [
      {
        path: "supervision",
        name: "查看督导情况",
        meta: {
          i18n: "info",
          title: "查看督导情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/SupervisionTeam/supervisonMain"
          ),
      },
    ],
  },
  {
    path: "/teacher/SupervisionTeam",
    component: Layout,
    redirect: "/teacher/SupervisionTeam/supervisonMain",
    children: [
      {
        path: "supervisonMain",
        name: "系督导数据统计分析",
        meta: {
          i18n: "info",
          title: "系督导数据统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/SupervisionTeam/supervisonMain"
          ),
      },
    ],
  },
  
  {
    path: "/teacher/SupervisionTeam",
    component: Layout,
    redirect: "/teacher/appraisal/supervisioner",
    children: [
      {
        path: "supervisioner",
        name: "系督导结果查看",
        meta: {
          i18n: "info",
          title: "系督导结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/supervisioner"
          ),
      },
    ],
  },

  /*学员查看全部考试安排*/
  {
    path: "/student/examination",
    component: Layout,
    redirect: "/student/examination/examinationlist",
    children: [
      {
        path: "examinationlist",
        name: "查看考试安排",
        meta: {
          i18n: "info",
          title: "查看考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/examination/examinationlist"
          ),
      },
    ],
  },
  //管理员
  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/ClassHourAudit",
    children: [
      {
        path: "ClassHourAudit",
        name: "课时量审核",
        meta: {
          i18n: "info",
          title: "课时量审核",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/ClassHourAudit"
          ),
      },
    ],
  },

  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/classAddHours",
    children: [
      {
        path: "classAddHours",
        name: "录入课时量",
        meta: {
          i18n: "info",
          title: "录入课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/classAddHours"
          ),
      },
    ],
  },
  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/classHoursDetails",
    children: [
      {
        path: "classHoursDetails",
        name: "查看课时量",
        meta: {
          i18n: "info",
          title: "查看课时量",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/classHoursDetails"
            ),
      },
    ],
  },
  //课时量
  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/ClassTimeQuery",
    children: [
      {
        path: "ClassTimeQuery",
        name: "课时量查询、统计",
        meta: {
          i18n: "info",
          title: "课时量查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/ClassTimeQuery"
          ),
      },
    ],
  },
  //查看统计
  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/viewStatistics",
    children: [
      {
        path: "viewStatistics",
        name: "查看统计",
        meta: {
          i18n: "info",
          title: "查看统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/viewStatistics"
          ),
      },
    ],
  },

  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/Print",
    children: [
      {
        path: "Print",
        name: "课程考核实施计划审核",
        meta: {
          i18n: "info",
          title: "课程考核实施计划审核",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Print"
          ),
      },
    ],
  },
  /*修改课程考核实施计划*/
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/editPlanManage",
    children: [
      {
        path: "editPlanManage",
        name: "修改课程考核实施计划",
        meta: {
          i18n: "info",
          title: "修改课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/editPlanManage"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/PlanQuery",
    children: [
      {
        path: "PlanQuery",
        name: "课程考核实施计划审核查询",
        meta: {
          i18n: "info",
          title: "课程考核实施计划审核查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/PlanQuery"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/planag",
    children: [
      {
        path: "planag",
        name: "课程考核实施计划",
        meta: {
          i18n: "info",
          title: "课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/planag"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/planega",
    children: [
      {
        path: "planega",
        name: "课程考核实施计划变更申请",
        meta: {
          i18n: "info",
          title: "课程考核实施计划变更申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/planega"
          ),
      },
    ],
  },
  //教研室主任---课程考核实施计划变更申请
  {
    path: "/director/planManagement",
    component: Layout,
    redirect: "/director/planManagement/planega",
    children: [
      {
        path: "planega",
        name: "课程考核实施计划变更申请",
        meta: {
          i18n: "info",
          title: "课程考核实施计划变更申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/planega"
          ), //变更
      },
    ],
  },
  //教研室主任---课程考核实施计划
  {
    path: "/director/planManagement",
    component: Layout,
    redirect: "/director/planManagement/planag",
    children: [
      {
        path: "planag",
        name: "课程考核实施计划",
        meta: {
          i18n: "info",
          title: "课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Print"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/changePlanage",
    children: [
      {
        path: "changePlanage",
        name: "申请变更课程考核实施计划",
        meta: {
          i18n: "info",
          title: "申请变更课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/changePlanage"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/entryResultsage",
    children: [
      {
        path: "entryResultsage",
        name: "考试安排",
        meta: {
          i18n: "info",
          title: "考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/entryResultsage"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/Printage",
    children: [
      {
        path: "Printage",
        name: "开始排考",
        meta: {
          i18n: "info",
          title: "开始排考",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Printage"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/setCourse",
    children: [
      {
        path: "setCourse",
        name: "设置考试课程",
        meta: {
          i18n: "info",
          title: "设置考试课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/setCourse"
          ),
      },
    ],
  },

  //教员
  {
    path: "/captain/home",
    component: Layout,
    redirect: "/captain/home/index",
    children: [
      {
        path: "index",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/captain/home/index"),
      },
    ],
  },
  //教员
  {
    path: "/director/home",
    component: Layout,
    redirect: "/director/home/index",
    children: [
      {
        path: "index",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/director/home/index"),
      },
    ],
  },
  //教员
  {
    path: "/battalionChief/home",
    component: Layout,
    redirect: "/battalionChief/home/index",
    children: [
      {
        path: "index",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/home/index"
          ),
      },
    ],
  },
  //教员
  {
    path: "/leadership/home",
    component: Layout,
    redirect: "/leadership/home/index",
    children: [
      {
        path: "index",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/home/index"
          ),
      },
    ],
  },
  //教员
  {
    path: "/EducationLeader/home",
    component: Layout,
    redirect: "/EducationLeader/home/index",
    children: [
      {
        path: "index",
        name: "首页",
        meta: {
          i18n: "info",
          title: "首页",
          affix: true,
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/home/index"
          ),
      },
    ],
  },

  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/ResultsPrint",
    children: [
      {
        path: "ResultsPrint",
        name: "专业班次成绩单打印",
        meta: {
          i18n: "info",
          title: "专业班次成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/ResultsPrint"
          ),
      },
    ],
  },

  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/TranscriptPrinting",
    children: [
      {
        path: "TranscriptPrinting",
        name: "课程成绩单打印",
        meta: {
          i18n: "info",
          title: "课程成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/TranscriptPrinting"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/requditByInfo",
    children: [
      {
        path: "requditByInfo",
        name: "预览分项成绩",
        meta: {
          i18n: "info",
          title: "预览分项成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/requditByInfo"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/requditByInfoF",
    children: [
      {
        path: "requditByInfo",
        name: "预览总评成绩",
        meta: {
          i18n: "info",
          title: "预览总评成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/requditByInfo"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/requditByInfoC",
    children: [
      {
        path: "requditByInfo",
        name: "预览课程信息",
        meta: {
          i18n: "info",
          title: "预览课程信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/requditByInfo"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/report",
    children: [
      {
        path: "report",
        name: "学员成绩单打印",
        meta: {
          i18n: "info",
          title: "学员成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/report"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/examTranscript",
    children: [
      {
        path: "examTranscript",
        name: "补考课程成绩单打印",
        meta: {
          i18n: "info",
          title: "补考课程成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/examTranscript"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/examTranscriptTwo",
    children: [
      {
        path: "examTranscriptTwo",
        name: "第二次补考课程成绩单打印",
        meta: {
          i18n: "info",
          title: "第二次补考课程成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/examTranscriptTwo"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment",
    component: Layout,
    redirect: "/admin/courseAssessment/browseResults",
    children: [
      {
        path: "browseResults",
        name: "浏览总评成绩",
        meta: {
          i18n: "info",
          title: "浏览总评成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/browseResults"
          ),
      },
    ],
  },
  {
    path: "/captain/courseAssessment",
    component: Layout,
    redirect: "/captain/courseAssessment/browseResults",
    children: [
      {
        path: "browseResults",
        name: "专业班次成绩单打印",
        meta: {
          i18n: "info",
          title: "专业班次成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/courseAssessment/browseResults"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/courseAssessment",
    component: Layout,
    redirect: "/battalionChief/courseAssessment/browseResults",
    children: [
      {
        path: "browseResults",
        name: "专业班次成绩单打印",
        meta: {
          i18n: "info",
          title: "专业班次成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/courseAssessment/browseResults"
          ),
      },
    ],
  },

  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/professionalCourses",
    children: [
      {
        path: "professionalCourses",
        name: "学员评教结果查看",
        meta: {
          i18n: "info",
          title: "学员评教结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/professionalCourses"
          ),
      },
    ],
  },

  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/professionalCoursesList",
    children: [
      {
        path: "professionalCoursesList",
        name: "学员评教结果查看",
        meta: {
          i18n: "info",
          title: "学员评教结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/professionalCoursesList"
          ),
      },
    ],
  },

  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/optionalCourse",
    children: [
      {
        path: "optionalCourse",
        name: "查看专业课数据分析",
        meta: {
          i18n: "info",
          title: "查看专业课数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/optionalCourse"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/averageScore",
    children: [
      {
        path: "averageScore",
        name: "查看教员平均分数据分析",
        meta: {
          i18n: "info",
          title: "查看教员平均分数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/averageScore"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/scoreDistribution",
    children: [
      {
        path: "scoreDistribution",
        name: "查看校区分析",
        meta: {
          i18n: "info",
          title: "查看校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/scoreDistribution"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/distributionCourse",
    children: [
      {
        path: "distributionCourse",
        name: "查看专业课通识课校区分析",
        meta: {
          i18n: "info",
          title: "查看专业课通识课校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/distributionCourse"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/updateProfessional",
    children: [
      {
        path: "updateProfessional",
        name: "查看评教结果",
        meta: {
          i18n: "info",
          title: "查看评教结果",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/updateProfessional"
          ),
      },
    ],
  },

  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/studentScoreEntry',
  //   children: [{
  //     path: 'studentScoreEntry',
  //     name: '学员成绩录入',
  //     meta: {
  //       i18n: 'info',
  //       title: "学员成绩录入"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/studentScoreEntry')
  //   }]
  // },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTestes",
    children: [
      {
        path: "delayTestes",
        name: "录入第二次补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入第二次补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTestes"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/scoreManagement",
    component: Layout,
    redirect: "/EducationLeader/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/scoreManagement/manually"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/deferredExamination',
  //   children: [{
  //     path: 'deferredExamination',
  //     name: '录入第二次补缓考成绩',
  //     meta: {
  //       i18n: 'info',
  //       title: "录入第二次补缓考成绩"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/deferredExamination')
  //   }]
  // },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/ruditedResults",
    children: [
      {
        path: "ruditedResults",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/ruditedResults"
          ),
      },
    ],
  },
  /* 333333333333333333333 */
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 111111111111111 */
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adming',
  //   children: [{
  //     path: 'adming',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adming')
  //   }]
  // },
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adminC',
  //   children: [{
  //     path: 'adminC',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adminC')
  //   }]
  // // },
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adminj',
  //   children: [{
  //     path: 'adminj',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adminj')
  //   }]
  // },
  /* 111111111111111111111 */
  /* 333333333333333333333 */
  // {
  //   path: "/captain/scoreManagement",
  //   component: Layout,
  //   redirect: '/captain/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/captain/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333 */
  // {
  //   path: "/leadership/scoreManagement",
  //   component: Layout,
  //   redirect: '/leadership/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333333 */
  // {
  //   path: "/EducationLeader/scoreManagement",
  //   component: Layout,
  //   redirect: '/EducationLeader/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/EducationLeader/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333333333333 */
  // {
  //   path: "/battalionChief/scoreManagement",
  //   component: Layout,
  //   redirect: '/battalionChief/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/battalionChief/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 3333333333 */
  // {
  //   path: "/director/scoreManagement",
  //   component: Layout,
  //   redirect: '/director/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  {
    path: "/EducationLeader/examination",
    component: Layout,
    redirect: "/EducationLeader/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/captain/examination",
    component: Layout,
    redirect: "/captain/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/leadership/examination",
    component: Layout,
    redirect: "/leadership/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/examination",
    component: Layout,
    redirect: "/EducationLeader/examination/examinationlist",
    children: [
      {
        path: "examinationlist",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/examination/examinationlist"
          ),
      },
    ],
  },
  {
    path: "/leadership/examination",
    component: Layout,
    redirect: "/leadership/examination/examinationlist",
    children: [
      {
        path: "examinationlist",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/examination/examinationlist"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/examination",
    component: Layout,
    redirect: "/battalionChief/examination/examinationlist",
    children: [
      {
        path: "examinationlist",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/examination/examinationlist"
          ),
      },
    ],
  },
  {
    path: "/captain/examination",
    component: Layout,
    redirect: "/captain/examination/examinationlist",
    children: [
      {
        path: "examinationlist",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/examination/examinationlist"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/InformationInquiry",
    component: Layout,
    redirect: "/battalionChief/InformationInquiry/evaluationTeaching",
    children: [
      {
        path: "evaluationTeaching",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/manage",
    children: [
      {
        path: "manage",
        name: "督导信息管理",
        meta: {
          i18n: "info",
          title: "督导信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/manage"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "新增专项督导信息",
        meta: {
          i18n: "info",
          title: "新增专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultStatisticor",
    children: [
      {
        path: "resultStatisticor",
        name: "督导结果查看",
        meta: {
          i18n: "info",
          title: "督导结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultStatisticor"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultStatisticer",
    children: [
      {
        path: "resultStatisticer",
        name: "教员平均分查看",
        meta: {
          i18n: "info",
          title: "教员平均分查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultStatisticer"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultAnalysiser",
    children: [
      {
        path: "resultAnalysiser",
        name: "督导月份分析",
        meta: {
          i18n: "info",
          title: "督导月份分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultAnalysiser"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultAnalysisor",
    children: [
      {
        path: "resultAnalysisor",
        name: "督导专家数据分析",
        meta: {
          i18n: "info",
          title: "督导专家数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultAnalysisor"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/assignRoleTargeter",
    children: [
      {
        path: "assignRoleTargeter",
        name: "督导指标体系选择",
        meta: {
          i18n: "info",
          title: "督导指标体系选择",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/assignRoleTargeter"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/managers",
    children: [
      {
        path: "managers",
        name: "专项督导信息",
        meta: {
          i18n: "info",
          title: "专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/managers"
          ),
      },
    ],
  },
  {
    path: "/leadership/appraisal",
    component: Layout,
    redirect: "/leadership/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/director/appraisal",
    component: Layout,
    redirect: "/director/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "新增督导信息",
        meta: {
          i18n: "info",
          title: "新增督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/addSupervisionInformation"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultAnalysis",
    children: [
      {
        path: "resultAnalysis",
        name: "督导结果分析",
        meta: {
          i18n: "info",
          title: "督导结果分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultStatistic",
    children: [
      {
        path: "resultStatistic",
        name: "督导结果统计",
        meta: {
          i18n: "info",
          title: "督导结果统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultStatistic"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/assignRoleTarget",
    children: [
      {
        path: "assignRoleTarget",
        name: "角色分配和指标选择",
        meta: {
          i18n: "info",
          title: "角色分配和指标选择",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/assignRoleTarget"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setClassroom",
    children: [
      {
        path: "setClassroom",
        name: "设置专业班教室",
        meta: {
          i18n: "info",
          title: "设置专业班教室",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setClassroom"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/printBarCode",
    children: [
      {
        path: "printBarCode",
        name: "补缓考报名",
        meta: {
          i18n: "info",
          title: "补缓考报名",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/printBarCode"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/examination",
  //   component: Layout,
  //   redirect: '/admin/examination/secPrintBarCode',
  //   children: [{
  //     path: 'secPrintBarCode',
  //     name: '第二次补考报名',
  //     meta: {
  //       i18n: 'info',
  //       title: "第二次补考报名"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/examination/secPrintBarCode')
  //   }]
  // },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/printBarCode2",
    children: [
      {
        path: "printBarCode2",
        name: "学员条形码打印",
        meta: {
          i18n: "info",
          title: "学员条形码打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/printBarCode2"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/statistical",
    children: [
      {
        path: "statistical",
        name: "考试数据统计",
        meta: {
          i18n: "info",
          title: "考试数据统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/statistical"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/addTestCourse",
    children: [
      {
        path: "addTestCourse",
        name: "添加考试课程",
        meta: {
          i18n: "info",
          title: "添加考试课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/addTestCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setTestClassroom",
    children: [
      {
        path: "setTestClassroom",
        name: "设置考场位数",
        meta: {
          i18n: "info",
          title: "设置考场位数",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setTestClassroom"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/updateScore",
    children: [
      {
        path: "updateScore",
        name: "成绩修改日志查询",
        meta: {
          i18n: "info",
          title: "成绩修改日志查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/updateScore"
          ),
      },
    ],
  },

  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/professionalCourses",
    children: [
      {
        path: "professionalCourses",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/professionalCourses"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/optionalCourse",
    children: [
      {
        path: "optionalCourse",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/optionalCourse"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/averageScore",
    children: [
      {
        path: "averageScore",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/averageScore"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/scoreDistribution",
    children: [
      {
        path: "scoreDistribution",
        name: "查看校区分析",
        meta: {
          i18n: "info",
          title: "查看校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/scoreDistribution"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/distributionCourse",
    children: [
      {
        path: "distributionCourse",
        name: "查看校区分析",
        meta: {
          i18n: "info",
          title: "查看校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/distributionCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/studentScoreEntry",
    children: [
      {
        path: "studentScoreEntry",
        name: "学员成绩录入",
        meta: {
          i18n: "info",
          title: "学员成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTestes",
    children: [
      {
        path: "delayTestes",
        name: "录入第二次补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入第二次补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTestes"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/scoreManagement",
    component: Layout,
    redirect: "/EducationLeader/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/deferredExamination",
    children: [
      {
        path: "deferredExamination",
        name: "第二次补缓考成绩录入",
        meta: {
          i18n: "info",
          title: "第二次补缓考成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/deferredExamination"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/ruditedResults",
    children: [
      {
        path: "ruditedResults",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/ruditedResults"
          ),
      },
    ],
  },
  /* 333333333333333 */
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333 */
  // {
  //   path: "/captain/scoreManagement",
  //   component: Layout,
  //   redirect: '/captain/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/captain/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 33333333333333333 */
  // {
  //   path: "/leadership/scoreManagement",
  //   component: Layout,
  //   redirect: '/leadership/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 3333333333333333333333 */
  // {
  //   path: "/EducationLeader/scoreManagement",
  //   component: Layout,
  //   redirect: '/EducationLeader/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/EducationLeader/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333 */
  // {
  //   path: "/battalionChief/scoreManagement",
  //   component: Layout,
  //   redirect: '/battalionChief/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/battalionChief/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 33333333333 */
  // {
  //   path: "/director/scoreManagement",
  //   component: Layout,
  //   redirect: '/director/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  {
    path: "/EducationLeader/examination",
    component: Layout,
    redirect: "/EducationLeader/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/captain/examination",
    component: Layout,
    redirect: "/captain/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/leadership/examination",
    component: Layout,
    redirect: "/leadership/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/examination/allExaminationlist"
          ),
      },
    ],
  },
  // {
  //   path: "/EducationLeader/examination",
  //   component: Layout,
  //   redirect: '/EducationLeader/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/EducationLeader/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/leadership/examination",
  //   component: Layout,
  //   redirect: '/leadership/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/battalionChief/examination",
  //   component: Layout,
  //   redirect: '/battalionChief/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/battalionChief/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/captain/examination",
  //   component: Layout,
  //   redirect: '/captain/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/captain/examination/examinationlist')
  //   }]
  // },
  {
    path: "/battalionChief/InformationInquiry",
    component: Layout,
    redirect: "/battalionChief/InformationInquiry/evaluationTeaching",
    children: [
      {
        path: "evaluationTeaching",
        name: "考试安排查询",
        meta: {
          i18n: "info",
          title: "考试安排查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/manage",
    children: [
      {
        path: "manage",
        name: "督导信息管理",
        meta: {
          i18n: "info",
          title: "督导信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/manage"
          ),
      },
    ],
  },
  {
    path: "/leadership/appraisal",
    component: Layout,
    redirect: "/leadership/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/director/appraisal",
    component: Layout,
    redirect: "/director/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "新增督导信息",
        meta: {
          i18n: "info",
          title: "新增督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/addSupervisionInformation"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultAnalysis",
    children: [
      {
        path: "resultAnalysis",
        name: "督导结果分析",
        meta: {
          i18n: "info",
          title: "督导结果分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultStatistic",
    children: [
      {
        path: "resultStatistic",
        name: "督导结果统计",
        meta: {
          i18n: "info",
          title: "督导结果统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultStatistic"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/assignRoleTarget",
    children: [
      {
        path: "assignRoleTarget",
        name: "角色分配和指标选择",
        meta: {
          i18n: "info",
          title: "角色分配和指标选择",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/assignRoleTarget"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setClassroom",
    children: [
      {
        path: "setClassroom",
        name: "设置专业班教室",
        meta: {
          i18n: "info",
          title: "设置专业班教室",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setClassroom"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/examination",
  //   component: Layout,
  //   redirect: '/admin/examination/printBarCode',
  //   children: [{
  //     path: 'printBarCode',
  //     name: '补缓考报名',
  //     meta: {
  //       i18n: 'info',
  //       title: "补缓考报名"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/examination/printBarCode')
  //   }]
  // },
  // {
  //   path: "/admin/examination",
  //   component: Layout,
  //   redirect: '/admin/examination/secPrintBarCode',
  //   children: [{
  //     path: 'secPrintBarCode',
  //     name: '第二次补考报名',
  //     meta: {
  //       i18n: 'info',
  //       title: "第二次补考报名"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/examination/secPrintBarCode')
  //   }]
  // },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/printBarCode2",
    children: [
      {
        path: "printBarCode2",
        name: "学员条形码打印",
        meta: {
          i18n: "info",
          title: "学员条形码打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/printBarCode2"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/statistical",
    children: [
      {
        path: "statistical",
        name: "考试数据统计",
        meta: {
          i18n: "info",
          title: "考试数据统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/statistical"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/addTestCourse",
    children: [
      {
        path: "addTestCourse",
        name: "添加考试课程",
        meta: {
          i18n: "info",
          title: "添加考试课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/addTestCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setTestClassroom",
    children: [
      {
        path: "setTestClassroom",
        name: "设置考场位数",
        meta: {
          i18n: "info",
          title: "设置考场位数",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setTestClassroom"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/updateScore",
    children: [
      {
        path: "updateScore",
        name: "成绩修改日志查询",
        meta: {
          i18n: "info",
          title: "成绩修改日志查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/updateScore"
          ),
      },
    ],
  },

  {
    path: "/director/workload",
    component: Layout,
    redirect: "/director/workload/AdjustInvigilators",
    children: [
      {
        path: "AdjustInvigilators",
        name: "调整监考申请记录",
        meta: {
          i18n: "info",
          title: "调整监考申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/workload/AdjustInvigilators"
          ),
      },
    ],
  },
  {
    path: "/director/examination",
    component: Layout,
    redirect: "/director/examination/arrangeTest",
    children: [
      {
        path: "arrangeTest",
        name: "安排考试",
        meta: {
          i18n: "info",
          title: "安排考试",
        },
        component: () =>
          // import( /* webpackChunkName: "views" */ '@/views/director/examination/arrangeTest')
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/Printage"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approval",
    children: [
      {
        path: "approval",
        name: "标准等级管理",
        meta: {
          i18n: "info",
          title: "标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approval"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaler",
    children: [
      {
        path: "approvaler",
        name: "标准体系管理",
        meta: {
          i18n: "info",
          title: "标准体系管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaler"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaleg",
    children: [
      {
        path: "approvaleg",
        name: "指标项目维护",
        meta: {
          i18n: "info",
          title: "指标项目维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaleg"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/professionalCourses",
    children: [
      {
        path: "professionalCourses",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/professionalCourses"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/optionalCourse",
    children: [
      {
        path: "optionalCourse",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/optionalCourse"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/averageScore",
    children: [
      {
        path: "averageScore",
        name: "查看数据分析",
        meta: {
          i18n: "info",
          title: "查看数据分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/averageScore"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/scoreDistribution",
    children: [
      {
        path: "scoreDistribution",
        name: "查看校区分析",
        meta: {
          i18n: "info",
          title: "查看校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/scoreDistribution"
          ),
      },
    ],
  },
  {
    path: "/leadership/InformationInquiry",
    component: Layout,
    redirect: "/leadership/InformationInquiry/distributionCourse",
    children: [
      {
        path: "distributionCourse",
        name: "查看校区分析",
        meta: {
          i18n: "info",
          title: "查看校区分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/InformationInquiry/distributionCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/studentScoreEntry",
    children: [
      {
        path: "studentScoreEntry",
        name: "学员成绩录入",
        meta: {
          i18n: "info",
          title: "学员成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/delayTest",
    children: [
      {
        path: "delayTest",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/delayTest"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/delayTestes",
    children: [
      {
        path: "delayTestes",
        name: "录入第二次补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入第二次补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/delayTestes"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/scoreManagement/manually"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/scoreManagement",
    component: Layout,
    redirect: "/EducationLeader/scoreManagement/manually",
    children: [
      {
        path: "manually",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/scoreManagement/manually"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/deferredExamination',
  //   children: [{
  //     path: 'deferredExamination',
  //     name: '录入第二次补缓考成绩',
  //     meta: {
  //       i18n: 'info',
  //       title: "录入第二次补缓考成绩"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/deferredExamination')
  //   }]
  // },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/ruditedResults",
    children: [
      {
        path: "ruditedResults",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/ruditedResults"
          ),
      },
    ],
  },
  /* 3333333333333333333 */
  // {
  //   path: "/admin/scoreManagement",
  //   component: Layout,
  //   redirect: '/admin/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 3333333333333333333333 */
  // {
  //   path: "/captain/scoreManagement",
  //   component: Layout,
  //   redirect: '/captain/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/captain/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 33333333333333333 */
  // {
  //   path: "/leadership/scoreManagement",
  //   component: Layout,
  //   redirect: '/leadership/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 333333333333333333 */
  // {
  //   path: "/EducationLeader/scoreManagement",
  //   component: Layout,
  //   redirect: '/EducationLeader/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/EducationLeader/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 33333333333333333333 */
  // {
  //   path: "/battalionChief/scoreManagement",
  //   component: Layout,
  //   redirect: '/battalionChief/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/battalionChief/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  /* 33333333333333 */
  // {
  //   path: "/director/scoreManagement",
  //   component: Layout,
  //   redirect: '/director/scoreManagement/adminCourseAssessment',
  //   children: [{
  //     path: 'adminCourseAssessment',
  //     name: '课程考核成绩查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "课程考核成绩查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/scoreManagement/adminCourseAssessment')
  //   }]
  // },
  {
    path: "/EducationLeader/examination",
    component: Layout,
    redirect: "/EducationLeader/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/captain/examination",
    component: Layout,
    redirect: "/captain/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/examination/allExaminationlist"
          ),
      },
    ],
  },
  {
    path: "/leadership/examination",
    component: Layout,
    redirect: "/leadership/examination/allExaminationlist",
    children: [
      {
        path: "allExaminationlist",
        name: "查看全部考试安排",
        meta: {
          i18n: "info",
          title: "查看全部考试安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/examination/allExaminationlist"
          ),
      },
    ],
  },
  // {
  //   path: "/EducationLeader/examination",
  //   component: Layout,
  //   redirect: '/EducationLeader/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/EducationLeader/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/leadership/examination",
  //   component: Layout,
  //   redirect: '/leadership/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/battalionChief/examination",
  //   component: Layout,
  //   redirect: '/battalionChief/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/battalionChief/examination/examinationlist')
  //   }]
  // },
  // {
  //   path: "/captain/examination",
  //   component: Layout,
  //   redirect: '/captain/examination/examinationlist',
  //   children: [{
  //     path: 'examinationlist',
  //     name: '考试安排查询',
  //     meta: {
  //       i18n: 'info',
  //       title: "考试安排查询"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/captain/examination/examinationlist')
  //   }]
  // },

  {
    path: "/battalionChief/InformationInquiry",
    component: Layout,
    redirect: "/battalionChief/InformationInquiry/evaluationTeaching",
    children: [
      {
        path: "evaluationTeaching",
        name: "评教结果查询",
        meta: {
          i18n: "info",
          title: "评教结果查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/InformationInquiry/evaluationTeaching"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/manage",
    children: [
      {
        path: "manage",
        name: "督导信息管理",
        meta: {
          i18n: "info",
          title: "督导信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/manage"
          ),
      },
    ],
  },
  {
    path: "/leadership/appraisal",
    component: Layout,
    redirect: "/leadership/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/director/appraisal",
    component: Layout,
    redirect: "/director/appraisal/addSpecial",
    children: [
      {
        path: "addSpecial",
        name: "填报专项督导信息",
        meta: {
          i18n: "info",
          title: "填报专项督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/appraisal/addSpecial"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/addSupervisionInformation",
    children: [
      {
        path: "addSupervisionInformation",
        name: "新增督导信息",
        meta: {
          i18n: "info",
          title: "新增督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/addSupervisionInformation"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultAnalysis",
    children: [
      {
        path: "resultAnalysis",
        name: "督导结果分析",
        meta: {
          i18n: "info",
          title: "督导结果分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/auditSupervision",
    children: [
      {
        path: "auditSupervision",
        name: "审核专项督导情况",
        meta: {
          i18n: "info",
          title: "审核专项督导情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/auditSupervision"
          ),
      },
    ],
  },
  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/resultStatistic",
    children: [
      {
        path: "resultStatistic",
        name: "督导结果统计",
        meta: {
          i18n: "info",
          title: "督导结果统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/resultStatistic"
          ),
      },
    ],
  },

  {
    path: "/admin/appraisal",
    component: Layout,
    redirect: "/admin/appraisal/assignRoleTarget",
    children: [
      {
        path: "assignRoleTarget",
        name: "角色分配和指标选择",
        meta: {
          i18n: "info",
          title: "角色分配和指标选择",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/appraisal/assignRoleTarget"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setClassroom",
    children: [
      {
        path: "setClassroom",
        name: "设置专业班教室",
        meta: {
          i18n: "info",
          title: "设置专业班教室",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setClassroom"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/examination",
  //   component: Layout,
  //   redirect: '/admin/examination/printBarCode',
  //   children: [{
  //     path: 'printBarCode',
  //     name: '补缓考报名',
  //     meta: {
  //       i18n: 'info',
  //       title: "补缓考报名"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/examination/printBarCode')
  //   }]
  // },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/secPrintBarCode",
    children: [
      {
        path: "secPrintBarCode",
        name: "第二次补考报名",
        meta: {
          i18n: "info",
          title: "第二次补考报名",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/secPrintBarCode"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/printBarCode2",
    children: [
      {
        path: "printBarCode2",
        name: "学员条形码打印",
        meta: {
          i18n: "info",
          title: "学员条形码打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/printBarCode2"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/statistical",
    children: [
      {
        path: "statistical",
        name: "考试数据统计",
        meta: {
          i18n: "info",
          title: "考试数据统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/statistical"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/addTestCourse",
    children: [
      {
        path: "addTestCourse",
        name: "添加考试课程",
        meta: {
          i18n: "info",
          title: "添加考试课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/addTestCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setTestClassroom",
    children: [
      {
        path: "setTestClassroom",
        name: "设置考场位数",
        meta: {
          i18n: "info",
          title: "设置考场位数",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setTestClassroom"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/updateScore",
    children: [
      {
        path: "updateScore",
        name: "成绩修改日志查询",
        meta: {
          i18n: "info",
          title: "成绩修改日志查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/updateScore"
          ),
      },
    ],
  },

  {
    path: "/director/workload",
    component: Layout,
    redirect: "/director/workload/AdjustInvigilators",
    children: [
      {
        path: "AdjustInvigilators",
        name: "调整监考申请记录",
        meta: {
          i18n: "info",
          title: "调整监考申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/workload/AdjustInvigilators"
          ),
      },
    ],
  },
  // {
  //   path: "/director/examination",
  //   component: Layout,
  //   redirect: '/director/examination/arrangeTest',
  //   children: [{
  //     path: 'arrangeTest',
  //     name: '安排考试',
  //     meta: {
  //       i18n: 'info',
  //       title: "安排考试"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/examination/arrangeTest')
  //   }]
  // },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approval",
    children: [
      {
        path: "approval",
        name: "标准等级管理",
        meta: {
          i18n: "info",
          title: "标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approval"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaler",
    children: [
      {
        path: "approvaler",
        name: "标准等级管理",
        meta: {
          i18n: "info",
          title: "标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaler"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaleg",
    children: [
      {
        path: "approvaleg",
        name: "指标项目维护",
        meta: {
          i18n: "info",
          title: "指标项目维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaleg"
          ),
      },
    ],
  },
  {
    path: "/director/workload",
    component: Layout,
    redirect: "/director/workload/AdjustInvigilators",
    children: [
      {
        path: "AdjustInvigilators",
        name: "调整监考申请记录",
        meta: {
          i18n: "info",
          title: "调整监考申请记录",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/workload/AdjustInvigilators"
          ),
      },
    ],
  },
  // {
  //   path: "/director/examination",
  //   component: Layout,
  //   redirect: '/director/examination/arrangeTest',
  //   children: [{
  //     path: 'arrangeTest',
  //     name: '安排考试',
  //     meta: {
  //       i18n: 'info',
  //       title: "安排考试"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/examination/arrangeTest')
  //   }]
  // },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approval",
    children: [
      {
        path: "approval",
        name: "标准等级管理",
        meta: {
          i18n: "info",
          title: "标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approval"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaler",
    children: [
      {
        path: "approvaler",
        name: "标准等级管理",
        meta: {
          i18n: "info",
          title: "标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaler"
          ),
      },
    ],
  },

  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/seeStandardManagement",
    children: [
      {
        path: "seeStandardManagement",
        name: "查看标准等级管理",
        meta: {
          i18n: "info",
          title: "查看标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/seeStandardManagement"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/approvaleg",
    children: [
      {
        path: "approvaleg",
        name: "指标项目维护",
        meta: {
          i18n: "info",
          title: "指标项目维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/approvaleg"
          ),
      },
    ],
  },
  {
    path: "/admin/propositionReview",
    component: Layout,
    redirect: "/admin/propositionReview/paperAnalysis", //???
    children: [
      {
        path: "paperAnalysis",
        name: "课程试卷分析表",
        meta: {
          i18n: "paperAnalysis",
          title: "课程试卷分析表",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/paperAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/propositionReview",
    component: Layout,
    redirect: "/admin/propositionReview/subpoena", //???
    children: [
      {
        path: "subpoena",
        name: "试卷送印传票",
        meta: {
          i18n: "subpoena",
          title: "试卷送印传票",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/subpoena"
          ),
      },
    ],
  },
  {
    path: "/admin/propositionReview",
    component: Layout,
    redirect: "/admin/propositionReview/approval", //???
    children: [
      {
        path: "approval",
        name: "试卷命题和审批管理",
        meta: {
          i18n: "approval",
          title: "试卷命题和审批管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/approval"
          ),
      },
    ],
  },
  {
    path: "/admin/propositionReview",
    component: Layout,
    redirect: "/admin/propositionReview/propositionaddPlan", //???
    children: [
      {
        path: "propositionaddPlan",
        name: "考核命题计划",
        meta: {
          i18n: "propositionaddPlan",
          title: "考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/propositionaddPlan"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/paperPrint",
    children: [
      {
        path: "paperPrint",
        name: "试卷册材料打印",
        meta: {
          i18n: "info",
          title: "试卷册材料打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/paperPrint"
          ),
      },
    ],
  },
  {
    path: "/director/propositionReview",
    component: Layout,
    redirect: "/director/propositionReview/paperAnalysis", //???
    children: [
      {
        path: "paperAnalysis",
        name: "课程试卷分析表",
        meta: {
          i18n: "paperAnalysis",
          title: "课程试卷分析表",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/propositionReview/paperAnalysis"
          ),
      },
    ],
  },
  {
    path: "/director/propositionReview",
    component: Layout,
    redirect: "/director/propositionReview/subpoena", //???
    children: [
      {
        path: "subpoena",
        name: "试卷送印传票",
        meta: {
          i18n: "subpoena",
          title: "试卷送印传票",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/propositionReview/subpoena"
          ),
      },
    ],
  },
  {
    path: "/director/propositionReview",
    component: Layout,
    redirect: "/director/propositionReview/approval", //???
    children: [
      {
        path: "approval",
        name: "试卷命题和审批管理",
        meta: {
          i18n: "approval",
          title: "试卷命题和审批管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/approval"
          ),
      },
    ],
  },
  {
    path: "/director/propositionReview",
    component: Layout,
    redirect: "/director/propositionReview/propositionaddPlan", //???
    children: [
      {
        path: "propositionaddPlan",
        name: "考核命题计划",
        meta: {
          i18n: "propositionaddPlan",
          title: "考核命题计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/propositionReview/propositionaddPlan"
          ),
      },
    ],
  },
  {
    path: "/director/paperPrint",
    component: Layout,
    redirect: "/director/paperPrint/paperPrint",
    children: [
      {
        path: "paperPrint",
        name: "试卷册材料打印",
        meta: {
          i18n: "info",
          title: "试卷册材料打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/paperPrint/paperPrint"
          ),
      },
    ],
  },
  {
    path: "/director/paperPrint",
    component: Layout,
    redirect: "/paperPrint/paperPrint",
    children: [
      {
        path: "paperPrint",
        name: "试卷册材料打印",
        meta: {
          i18n: "info",
          title: "试卷册材料打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/paperPrint/paperPrint"
          ),
      },
    ],
  },
  {
    path: "/leadership/paperPrint",
    component: Layout,
    redirect: "/paperPrint/paperPrint",
    children: [
      {
        path: "paperPrint",
        name: "试卷册材料打印",
        meta: {
          i18n: "info",
          title: "试卷册材料打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/paperPrint/paperPrint"
          ),
      },
    ],
  },
  {
    path: "/admin/paperPrint",
    component: Layout,
    redirect: "/admin/paperPrint/addTestCourse",
    children: [
      {
        path: "addTestCourse",
        name: "学员成绩单打印",
        meta: {
          i18n: "info",
          title: "学员成绩单打印",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/paperPrint/addTestCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/adminCourseAsses",
    children: [
      {
        path: "adminCourseAsses",
        name: "未录入成绩课程成绩查询",
        meta: {
          i18n: "info",
          title: "未录入成绩课程成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adminCourseAsses"
          ),
      },
    ],
  },
  {
    path: "/student/approvalDelayedExam",
    component: Layout,
    redirect: "/student/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "申请缓考",
        meta: {
          i18n: "info",
          title: "申请缓考",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  // {
  //   path: "/student/approvalDelayedExam",
  //   component: Layout,
  //   redirect: '/student/approvalDelayedExam/approvalDelayedExam',
  //   children: [{
  //     path: 'approvalDelayedExam',
  //     name: '缓考申请',
  //     meta: {
  //       i18n: 'info',
  //       title: "缓考申请"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/student/approvalDelayedExam/approvalDelayedExam')
  //   }]
  // },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/checkScore",
    children: [
      {
        path: "checkScore",
        name: "课程考核成绩审核",
        meta: {
          i18n: "info",
          title: "课程考核成绩审核",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/checkScore"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/getScore",
    children: [
      {
        path: "getScore",
        name: "课程考核成绩未录人员",
        meta: {
          i18n: "info",
          title: "课程考核成绩未录人员",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/getScore"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/updatePrintEntry",
    children: [
      {
        path: "updatePrintEntry",
        name: "设置参评对象",
        meta: {
          i18n: "info",
          title: "设置参评对象",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/updatePrintEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setPrintEntry",
    children: [
      {
        path: "setPrintEntry",
        name: "创建学员评教活动",
        meta: {
          i18n: "info",
          title: "创建学员评教活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setPrintEntry"
          ),
      },
    ],
  },

  //新加路由
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/updateEvalutions",
    children: [
      {
        path: "updateEvalutions",
        name: "编辑期中评价活动",
        meta: {
          i18n: "info",
          title: "编辑期中评价活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/updateEvalutions"
          ),
      },
    ],
  },

  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/updateZhuanxiang",
    children: [
      {
        path: "updateZhuanxiang",
        name: "编辑专项教学活动",
        meta: {
          i18n: "info",
          title: "编辑专项教学活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/updateZhuanxiang"
          ),
      },
    ],
  },

  //管理员
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/giveRole",
    children: [
      {
        path: "giveRole",
        name: "授予角色权限",
        meta: {
          i18n: "info",
          title: "授予角色权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/giveRole"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/getPrintEntry",
    children: [
      {
        path: "getPrintEntry",
        name: "查看未评价学员",
        meta: {
          i18n: "info",
          title: "查看未评价学员",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/getPrintEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/undertake",
    children: [
      {
        path: "undertake",
        name: "承训专业任务",
        meta: {
          i18n: "info",
          title: "承训专业任务",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/undertake"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/details",
    children: [
      {
        path: "details",
        name: "专业详情",
        meta: {
          i18n: "info",
          title: "专业详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/details"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/major",
    children: [
      {
        path: "major",
        name: "年度开设专业",
        meta: {
          i18n: "info",
          title: "年度开设专业",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/major"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/setup",
    children: [
      {
        path: "setup",
        name: "设置年度开设专业",
        meta: {
          i18n: "info",
          title: "设置年度开设专业",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/setup"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/enrollment",
    children: [
      {
        path: "enrollment",
        name: "设置招生专业",
        meta: {
          i18n: "info",
          title: "设置招生专业",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/enrollment"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/information",
    children: [
      {
        path: "information",
        name: "设置学期信息",
        meta: {
          i18n: "info",
          title: "设置学期信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/information"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/inquire",
    children: [
      {
        path: "inquire",
        name: "年度开设专业查询",
        meta: {
          i18n: "info",
          title: "年度开设专业查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/inquire"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/specialty",
    children: [
      {
        path: "specialty",
        name: "编辑专业",
        meta: {
          i18n: "info",
          title: "编辑专业",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/specialty"
          ),
      },
    ],
  },
  {
    path: "/admin/professional",
    component: Layout,
    redirect: "/admin/professional/specialtysr",
    children: [
      {
        path: "specialtysr",
        name: "添加专业",
        meta: {
          i18n: "info",
          title: "添加专业",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/professional/specialtysr"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/trainings",
    children: [
      {
        path: "trainings",
        name: "培养方案管理",
        meta: {
          i18n: "info",
          title: "培养方案管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/trainings"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/programmanagement",
    children: [
      {
        path: "programmanagement",
        name: "培养方案管理详情",
        meta: {
          i18n: "info",
          title: "培养方案管理详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/programmanagement"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/newLookNoedit",
    children: [
      {
        path: "newLookNoedit",
        name: "培养方案管理详情",
        meta: {
          i18n: "info",
          title: "培养方案管理详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/newLookNoedit"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/newLookDetails",
    children: [
      {
        path: "newLookDetails",
        name: "培养方案管理详情",
        meta: {
          i18n: "info",
          title: "培养方案管理详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/newLookDetails"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/modify",
    children: [
      {
        path: "modify",
        name: "修改培养方案",
        meta: {
          i18n: "info",
          title: "修改培养方案",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/modify"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/talent",
    children: [
      {
        path: "talent",
        name: "创建人才培养方案",
        meta: {
          i18n: "info",
          title: "创建人才培养方案",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/talent"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/classifications",
    children: [
      {
        path: "classifications",
        name: "培养方案分类设置",
        meta: {
          i18n: "info",
          title: "培养方案分类设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/classifications"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/classification",
    children: [
      {
        path: "classification",
        name: "课程分类设置",
        meta: {
          i18n: "info",
          title: "课程分类设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/classification"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/setCourseType",
    children: [
      {
        path: "setCourseType",
        name: "课程设置",
        meta: {
          i18n: "info",
          title: "课程设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/setCourseType"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/courses",
    children: [
      {
        path: "courses",
        name: "新增课程",
        meta: {
          i18n: "info",
          title: "新增课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/courses"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/addTrainCourse",
    children: [
      {
        path: "addTrainCourse",
        name: "新增课程",
        meta: {
          i18n: "info",
          title: "新增课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/addTrainCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/implementation",
    children: [
      {
        path: "implementation",
        name: "培养方案执行",
        meta: {
          i18n: "info",
          title: "培养方案执行",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/implementation"
          ),
      },
    ],
  },
  {
    path: "/captain/training",
    component: Layout,
    redirect: "/captain/training/trainSearch",
    children: [
      {
        path: "trainSearch",
        name: "人才培养方案查询",
        meta: {
          i18n: "info",
          title: "人才培养方案查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/training/trainSearch"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/training",
    component: Layout,
    redirect: "/battalionChief/training/trainSearch",
    children: [
      {
        path: "trainSearch",
        name: "人才培养方案查询",
        meta: {
          i18n: "info",
          title: "人才培养方案查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/training/trainSearch"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/training",
    component: Layout,
    redirect: "/EducationLeader/training/trainSearch",
    children: [
      {
        path: "trainSearch",
        name: "人才培养方案查询",
        meta: {
          i18n: "info",
          title: "人才培养方案查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/training/trainSearch"
          ),
      },
    ],
  },
  {
    path: "/student/training",
    component: Layout,
    redirect: '/student/training/trainSearch',
    children: [{
      path: 'trainSearch',
      name: '培养方案管理详情',
      meta: {
        i18n: 'info',
        title: "培养方案管理详情"
      },
      component: () =>
        // import( /* webpackChunkName: "views" */ '@/views/captain/training/trainSearch')
        import( /* webpackChunkName: "views" */ '@/views/student/training/programmanagement')
    }]
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/batch",
    children: [
      {
        path: "batch",
        name: "批量引用培养方案",
        meta: {
          i18n: "info",
          title: "批量引用培养方案",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/batch"
          ),
      },
    ],
  },
  {
    path: "/admin/training",
    component: Layout,
    redirect: "/admin/training/details",
    children: [
      {
        path: "details",
        name: "培养方案详情",
        meta: {
          i18n: "info",
          title: "培养方案详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/training/details"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/courses",
    children: [
      {
        path: "courses",
        name: "课程库管理",
        meta: {
          i18n: "info",
          title: "课程库管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/courses"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/coursesDe",
    children: [
      {
        path: "coursesDe",
        name: "课程库管理详情",
        meta: {
          i18n: "info",
          title: "课程库管理详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/coursesDe"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/addCourse",
    children: [
      {
        path: "addCourse",
        name: "添加课程",
        meta: {
          i18n: "addCourse",
          title: "添加课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/addCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/addCoursesr",
    children: [
      {
        path: "addCoursesr",
        name: "修改课程",
        meta: {
          i18n: "addCoursesr",
          title: "修改课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/addCoursesr"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/classificationMaintenance",
    children: [
      {
        path: "classificationMaintenance",
        name: "课程类型维护",
        meta: {
          i18n: "classificationMaintenance",
          title: "课程类型维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/classificationMaintenance"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/awardCourse",
    children: [
      {
        path: "awardCourse",
        name: "获奖课程维护",
        meta: {
          i18n: "awardCourse",
          title: "获奖课程维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/awardCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/classificationSetting",
    children: [
      {
        path: "classificationSetting",
        name: "课程类型设置",
        meta: {
          i18n: "classificationSetting",
          title: "课程类型设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/classificationSetting"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/editCourse",
    children: [
      {
        path: "editCourse",
        name: "修改课程",
        meta: {
          i18n: "editCourse",
          title: "修改课程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/editCourse"
          ),
      },
    ],
  },
  {
    path: "/admin/textbookManagement",
    component: Layout,
    redirect: "/admin/textbookManagement/index",
    children: [
      {
        path: "index",
        name: "教材库管理",
        meta: {
          i18n: "index",
          title: "教材库管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/textbookManagement/index"
          ),
      },
    ],
  },
  {
    path: "/admin/calendarManagement",
    component: Layout,
    redirect: "/admin/calendarManagement/index",
    children: [
      {
        path: "index",
        name: "学期院历管理",
        meta: {
          i18n: "index",
          title: "学期院历管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/calendarManagement/index"
          ),
      },
    ],
  },
  {
    path: "/admin/calendarManagement",
    component: Layout,
    redirect: "/admin/calendarManagement/calendar",
    children: [
      {
        path: "calendar",
        name: "查看学期院历",
        meta: {
          i18n: "calendar",
          title: "查看学期院历",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/calendarManagement/calendar"
          ),
      },
    ],
  },
  {
    path: "/admin/course",
    component: Layout,
    redirect: "/admin/course/datails",
    children: [
      {
        path: "datails",
        name: "课程详情",
        meta: {
          i18n: "info",
          title: "课程详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/course/datails"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/getPrintEntrys",
    children: [
      {
        path: "getPrintEntrys",
        name: "查看已评价学员",
        meta: {
          i18n: "info",
          title: "查看已评价学员",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/getPrintEntrys"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/getPrintEntrysResult",
    children: [
      {
        path: "getPrintEntrysResult",
        name: "查看评价结果",
        meta: {
          i18n: "info",
          title: "查看评价结果",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/getPrintEntrys"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setPrintEntrya",
    children: [
      {
        path: "setPrintEntrya",
        name: "创建学员评教活动",
        meta: {
          i18n: "info",
          title: "创建学员评教活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setPrintEntrya"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setPrintEntryb",
    children: [
      {
        path: "setPrintEntryb",
        name: "创建学员评教活动",
        meta: {
          i18n: "info",
          title: "创建学员评教活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setPrintEntryb"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/upPrintEntry",
    children: [
      {
        path: "upPrintEntry",
        name: "查询个性化指标",
        meta: {
          i18n: "info",
          title: "查询个性化指标",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/upPrintEntry"
          ),
      },
    ],
  },
  {
    path: "/director/examination",
    component: Layout,
    redirect: "/director/examination/updatePrintEntry",
    children: [
      {
        path: "updatePrintEntry",
        name: "设置参评对象",
        meta: {
          i18n: "info",
          title: "设置参评对象",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/examination/updatePrintEntry"
          ),
      },
    ],
  },
  {
    path: "/director/examination",
    component: Layout,
    redirect: "/director/examination/upPrintEntry",
    children: [
      {
        path: "upPrintEntry",
        name: "评教信息核对",
        meta: {
          i18n: "info",
          title: "评教信息核对",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/examination/upPrintEntry"
          ),
      },
    ],
  },
  {
    path: "/director/examination",
    component: Layout,
    redirect: "/director/examination/updatePrintEntrys",
    children: [
      {
        path: "updatePrintEntrys",
        name: "权限委托查询",
        meta: {
          i18n: "info",
          title: "权限委托查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/examination/updatePrintEntrys"
          ),
      },
    ],
  },
  // {
  //   path: "/leadership/ManagementHours",
  //   component: Layout,
  //   redirect: '/leadership/ManagementHours/ClassTimeQuery',
  //   children: [{
  //     path: 'ClassTimeQuery',
  //     name: '课时量查询、统计',
  //     meta: {
  //       i18n: 'info',
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/ManagementHours/ClassTimeQuery')
  //   }]
  // },
  {
    path: "/leadership/ManagementHours",
    component: Layout,
    redirect: "/leadership/ManagementHours/ClassTimeQuery",
    children: [
      {
        path: "ClassTimeQuery",
        name: "课时量查询、统计",
        meta: {
          i18n: "info",
          title: "课时量查询",
        },

        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/ClassTimeQuery"
          ),
      },
    ],
  },
  //查看统计
  {
    path: "/leadership/ManagementHours",
    component: Layout,
    redirect: "/leadership/ManagementHours/viewStatistics",
    children: [
      {
        path: "viewStatistics",
        name: "查看统计",
        meta: {
          i18n: "info",
          title: "查看统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/ManagementHours/viewStatistics"
          ),
      },
    ],
  },
  //查看统计
  {
    path: "/leadership/ManagementHours",
    component: Layout,
    redirect: "/leadership/ManagementHours/viewStatistics",
    children: [
      {
        path: "viewStatistics",
        name: "查看统计",
        meta: {
          i18n: "info",
          title: "查看统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/ManagementHours/viewStatistics"
          ),
      },
    ],
  },

  {
    path: "/EducationLeader/ManagementHours",
    component: Layout,
    redirect: "/EducationLeader/ManagementHours/ClassTimeQuery",
    children: [
      {
        path: "ClassTimeQuery",
        name: "课时量查询、统计",
        meta: {
          i18n: "info",
          title: "课时量查询、统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/ClassTimeQuery"
          ),
      },
    ],
  },
  //查看统计
  {
    path: "/EducationLeader/ManagementHours",
    component: Layout,
    redirect: "/EducationLeader/ManagementHours/viewStatistics",
    children: [
      {
        path: "viewStatistics",
        name: "查看统计",
        meta: {
          i18n: "info",
          title: "查看统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/ManagementHours/viewStatistics"
          ),
      },
    ],
  },

  {
    path: "/admin/approvalDelayedExam",
    component: Layout,
    redirect: "/admin/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "缓考审批",
        meta: {
          i18n: "info",
          title: "缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  /* 查看缓考审批 */
  {
    path: "/admin/approvalDelayedExam",
    component: Layout,
    redirect: "/admin/approvalDelayedExam/detailPlan",
    children: [
      {
        path: "detailPlan",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/captain/approvalDelayedExam",
    component: Layout,
    redirect: "/captain/approvalDelayedExam/detailPlan",
    children: [
      {
        path: "detailPlan",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/approvalDelayedExam",
    component: Layout,
    redirect: "/EducationLeader/approvalDelayedExam/detailPlan",
    children: [
      {
        path: "detailPlan",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/approvalDelayedExam",
    component: Layout,
    redirect: "/battalionChief/approvalDelayedExam/detailPlan",
    children: [
      {
        path: "detailPlan",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/student/approvalDelayedExam",
    component: Layout,
    redirect: "/student/approvalDelayedExam/detailPlan",
    children: [
      {
        path: "detailPlan",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/admin/approvalDelayedExam",
    component: Layout,
    redirect: "/admin/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/admin/approvalDelayedExam",
    component: Layout,
    redirect: "/admin/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考审批",
        meta: {
          i18n: "info",
          title: "修改缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/captain/approvalDelayedExam",
    component: Layout,
    redirect: "/captain/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "缓考审批",
        meta: {
          i18n: "info",
          title: "缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/captain/approvalDelayedExam",
    component: Layout,
    redirect: "/captain/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/captain/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/captain/approvalDelayedExam",
    component: Layout,
    redirect: "/captain/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考审批",
        meta: {
          i18n: "info",
          title: "修改缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/leadership/approvalDelayedExam",
    component: Layout,
    redirect: "/leadership/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "缓考审批",
        meta: {
          i18n: "info",
          title: "缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/leadership/approvalDelayedExam",
    component: Layout,
    redirect: "/leadership/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/leadership/approvalDelayedExam",
    component: Layout,
    redirect: "/leadership/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考审批",
        meta: {
          i18n: "info",
          title: "修改缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/EducationLeader/approvalDelayedExam",
    component: Layout,
    redirect: "/EducationLeader/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "缓考审批",
        meta: {
          i18n: "info",
          title: "缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/EducationLeader/approvalDelayedExam",
    component: Layout,
    redirect: "/EducationLeader/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/EducationLeader/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/EducationLeader/approvalDelayedExam",
    component: Layout,
    redirect: "/EducationLeader/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考审批",
        meta: {
          i18n: "info",
          title: "修改缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/approvalDelayedExam",
    component: Layout,
    redirect: "/battalionChief/approvalDelayedExam/approvalDelayedExam",
    children: [
      {
        path: "approvalDelayedExam",
        name: "缓考审批",
        meta: {
          i18n: "info",
          title: "缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/approvalDelayedExam"
          ),
      },
    ],
  },
  /*增加缓考申请*/
  {
    path: "/battalionChief/approvalDelayedExam",
    component: Layout,
    redirect: "/battalionChief/approvalDelayedExam/addPlan",
    children: [
      {
        path: "addPlan",
        name: "增加缓考申请",
        meta: {
          i18n: "info",
          title: "增加缓考申请",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/battalionChief/approvalDelayedExam/addPlan"
          ),
      },
    ],
  },
  /*修改缓考申请*/
  {
    path: "/battalionChief/approvalDelayedExam",
    component: Layout,
    redirect: "/battalionChief/approvalDelayedExam/editPlan",
    children: [
      {
        path: "editPlan",
        name: "修改缓考审批",
        meta: {
          i18n: "info",
          title: "修改缓考审批",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/approvalDelayedExam/editPlan"
          ),
      },
    ],
  },
  {
    path: "/leadership/evaluation",
    component: Layout,
    redirect: "/leadership/evaluation/resultReportCard",
    children: [
      {
        path: "resultReportCard",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/evaluation/resultReportCard"
          ),
      },
    ],
  },
  {
    path: "/director/evaluation",
    component: Layout,
    redirect: "/director/evaluation/resultReportCard",
    children: [
      {
        path: "resultReportCard",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/evaluation/resultReportCard"
          ),
      },
    ],
  },
  {
    path: "/leadership/evaluation",
    component: Layout,
    redirect: "/leadership/evaluation/resultReportCard",
    children: [
      {
        path: "resultReportCard",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/evaluation/resultReportCard"
          ),
      },
    ],
  },
  {
    path: "/director/evaluation",
    component: Layout,
    redirect: "/director/evaluation/resultReportCard",
    children: [
      {
        path: "resultReportCard",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/evaluation/resultReportCard"
          ),
      },
    ],
  },
  {
    path: "/leadership/evaluation",
    component: Layout,
    redirect: "/leadership/evaluation/resultReportCards",
    children: [
      {
        path: "resultReportCards",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/evaluation/resultReportCards"
          ),
      },
    ],
  },
  {
    path: "/director/evaluation",
    component: Layout,
    redirect: "/director/evaluation/resultReportCards",
    children: [
      {
        path: "resultReportCards",
        name: "学员评教结果报告单",
        meta: {
          i18n: "info",
          title: "学员评教结果报告单",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/evaluation/resultReportCards"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/giveRolePermission",
    children: [
      {
        path: "giveRolePermission",
        name: "授予角色权限",
        meta: {
          i18n: "info",
          title: "授予角色权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/giveRolePermission"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/giveRolePermission",
    children: [
      {
        path: "giveRolePermission",
        name: "授予角色权限",
        meta: {
          i18n: "info",
          title: "授予角色权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/giveRolePermission"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/resultAnalysis",
    children: [
      {
        path: "resultAnalysis",
        name: "评教结果统计分析",
        meta: {
          i18n: "info",
          title: "评教结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/setScoreParam",
    children: [
      {
        path: "setScoreParam",
        name: "成绩参数设置",
        meta: {
          i18n: "info",
          title: "成绩参数设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/setScoreParam"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/arrangeTest",
    children: [
      {
        path: "arrangeTest",
        name: "安排考试",
        meta: {
          i18n: "info",
          title: "安排考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/arrangeTest"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/seeStandardManagement",
    children: [
      {
        path: "seeStandardManagement",
        name: "查看标准等级管理",
        meta: {
          i18n: "info",
          title: "查看标准等级管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/seeStandardManagement"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/setActivityEvaluation",
    children: [
      {
        path: "setActivityEvaluation",
        name: "创建专项活动评价",
        meta: {
          i18n: "info",
          title: "创建专项活动评价",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/setActivityEvaluation"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/setActivityEvaluations",
    children: [
      {
        path: "setActivityEvaluations",
        name: "创建期中教学评价",
        meta: {
          i18n: "info",
          title: "创建期中教学评价",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/setActivityEvaluations"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/setActivity",
    children: [
      {
        path: "setActivity",
        name: "创建教学评价",
        meta: {
          i18n: "info",
          title: "创建教学评价",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/setActivity"
          ),
      },
    ],
  },

  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/writeActivity",
    children: [
      {
        path: "writeActivity",
        name: "编辑教学评价",
        meta: {
          i18n: "info",
          title: "编辑教学评价",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/writeActivity"
          ),
      },
    ],
  },

  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/courseCheck",
    children: [
      {
        path: "courseCheck",
        name: "课程考核成绩",
        meta: {
          i18n: "info",
          title: "课程考核成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/courseCheck"
          ),
      },
    ],
  },
  {
    path: "/admin/examination",
    component: Layout,
    redirect: "/admin/examination/checkCourseScore",
    children: [
      {
        path: "checkCourseScore",
        name: "成绩课程审核",
        meta: {
          i18n: "info",
          title: "成绩课程审核",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/examination/checkCourseScore"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/upPrintEntry",
    children: [
      {
        path: "upPrintEntry",
        name: "审核专项督导情况",
        meta: {
          i18n: "info",
          title: "审核专项督导情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/upPrintEntry"
          ),
      },
    ],
  },

  //  添加路由
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/giveRolePermission?yesOrNotFlag=1",
    children: [
      {
        path: "giveRolePermission",
        name: "授予角色权限",
        meta: {
          i18n: "info",
          title: "授予角色权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/giveRolePermission"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect:
      "/admin/evaluation/resultAnalysis?radio4=整体分析(专业课通识课分布)",
    children: [
      {
        path: "resultAnalysis",
        name: "评教结果统计分析",
        meta: {
          i18n: "info",
          title: "评教结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/resultAnalysis?radio4=教员平均得分",
    children: [
      {
        path: "resultAnalysis",
        name: "评教结果统计分析",
        meta: {
          i18n: "info",
          title: "评教结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/resultAnalysis?radio4=通识课",
    children: [
      {
        path: "resultAnalysis",
        name: "评教结果统计分析",
        meta: {
          i18n: "info",
          title: "评教结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/admin/evaluation",
    component: Layout,
    redirect: "/admin/evaluation/resultAnalysis?radio4=专业课",
    children: [
      {
        path: "resultAnalysis",
        name: "评教结果统计分析",
        meta: {
          i18n: "info",
          title: "评教结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/evaluation/resultAnalysis"
          ),
      },
    ],
  },
  {
    path: "/teacher/eligible",
    component: Layout,
    redirect: "/teacher/eligible/setupMetrics",
    children: [
      {
        path: "setupMetrics",
        name: "设置个性化指标",
        meta: {
          i18n: "info",
          title: "设置个性化指标",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/eligible/setupMetrics"
          ),
      },
    ],
  },
  {
    path: "/teacher/eligible",
    component: Layout,
    redirect: "/teacher/eligible/setupMetrics?radio4=问答题",
    children: [
      {
        path: "setupMetrics",
        name: "设置个性化指标",
        meta: {
          i18n: "info",
          title: "设置个性化指标",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/eligible/setupMetrics"
          ),
      },
    ],
  },

  // {
  //   path: "/leadership/courseAchievement",
  //   component: Layout,
  //   redirect: '/leadership/courseAchievement/printEntry',
  //   children: [{
  //     path: 'printEntry',
  //     name: '录入、打印课程成绩',
  //     meta: {
  //       i18n: 'info',
  //       title: "录入、打印课程成绩"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/leadership/courseAchievement/printEntry')
  //   }]
  // },
  // {
  //   path: "/director/courseAchievement",
  //   component: Layout,
  //   redirect: '/director/courseAchievement/printEntry',
  //   children: [{
  //     path: 'printEntry',
  //     name: '录入、打印课程成绩',
  //     meta: {
  //       i18n: 'info',
  //       title: "录入、打印课程成绩"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/director/courseAchievement/printEntry')
  //   }]
  // },
  {
    path: "/leadership/courseAchievement",
    component: Layout,
    redirect: "/leadership/courseAchievement/manualEntry",
    children: [
      {
        path: "manualEntry",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/courseAchievement/manualEntry"
          ),
      },
    ],
  },
  {
    path: "/director/courseAchievement",
    component: Layout,
    redirect: "/director/courseAchievement/manualEntry",
    children: [
      {
        path: "manualEntry",
        name: "手动录入课程成绩",
        meta: {
          i18n: "info",
          title: "手动录入课程成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/courseAchievement/manualEntry"
          ),
      },
    ],
  },
  {
    path: "/leadership/courseAchievement",
    component: Layout,
    redirect: "/leadership/courseAchievement/entryResults",
    children: [
      {
        path: "entryResults",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/courseAchievement/entryResults"
          ),
      },
    ],
  },
  {
    path: "/director/courseAchievement",
    component: Layout,
    redirect: "/director/courseAchievement/entryResults",
    children: [
      {
        path: "entryResults",
        name: "录入补缓考成绩",
        meta: {
          i18n: "info",
          title: "录入补缓考成绩",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/courseAchievement/entryResults"
          ),
      },
    ],
  },
  {
    path: "/leadership/courseAchievement",
    component: Layout,
    redirect: "/leadership/courseAchievement/makeupScoreEntry",
    children: [
      {
        path: "makeupScoreEntry",
        name: "补考成绩录入",
        meta: {
          i18n: "info",
          title: "补考成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/courseAchievement/makeupScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/director/courseAchievement",
    component: Layout,
    redirect: "/director/courseAchievement/makeupScoreEntry",
    children: [
      {
        path: "makeupScoreEntry",
        name: "补考成绩录入",
        meta: {
          i18n: "info",
          title: "补考成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/courseAchievement/makeupScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/updateRudite",
    children: [
      {
        path: "updateRudite",
        name: "已审核成绩修改",
        meta: {
          i18n: "info",
          title: "已审核成绩修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/updateRudite"
          ),
      },
    ],
  },

  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/csTester",
    children: [
      {
        path: "csTester",
        name: "专业班次一览表",
        meta: {
          i18n: "info",
          title: "专业班次一览表",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/csTest/csTester"
          ),
      },
    ],
  },
  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/csTeat",
    children: [
      {
        path: "csTeat",
        name: "专业班次管理",
        meta: {
          i18n: "info",
          title: "专业班次管理",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/admin/csTest/csTeat"),
      },
    ],
  },
  {
    path: "/teacher/appraisal",
    component: Layout,
    redirect: "/teacher/appraisal/addSupervisionInformationsr",
    children: [
      {
        path: "addSupervisionInformationsr",
        name: "查看督导信息",
        meta: {
          i18n: "info",
          title: "查看督导信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/appraisal/addSupervisionInformationsr"
          ),
      },
    ],
  },
  {
    path: "/teacher/SupervisionTeam",
    component: Layout,
    redirect: "/teacher/SupervisionTeam/permissionsor",
    children: [
      {
        path: "permissionsor",
        name: "授予角色权限",
        meta: {
          i18n: "info",
          title: "授予角色权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/SupervisionTeam/permissionsor"
          ),
      },
    ],
  },

  {
    path: "/director/SupervisionTeam",
    component: Layout,
    redirect: "/director/SupervisionTeam/supervisonMains",
    children: [
      {
        path: "supervisonMains",
        name: "系督导组长查看",
        meta: {
          i18n: "info",
          title: "查看督导情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/SupervisionTeam/supervisonMains"
          ),
      },
    ],
  },
  {
    path: "/teacher/SupervisionTeam",
    component: Layout,
    redirect: "/teacher/SupervisionTeam/supervisingExperts",
    children: [
      {
        path: "supervisingExperts",
        name: "按督导专家分析",
        meta: {
          i18n: "info",
          title: "按督导专家分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/SupervisionTeam/supervisingExperts"
          ),
      },
    ],
  },
  {
    path: "/director/SupervisionTeam",
    component: Layout,
    redirect: "/director/SupervisionTeam/supervisingExperts",
    children: [
      {
        path: "supervisingExperts",
        name: "按督导专家分析",
        meta: {
          i18n: "info",
          title: "按督导专家分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/SupervisionTeam/supervisingExperts"
          ),
      },
    ],
  },
  {
    path: "/director/SupervisionTeam",
    component: Layout,
    redirect: "/director/SupervisionTeam/supervision",
    children: [
      {
        path: "supervision",
        name: "督导结果查看",
        meta: {
          i18n: "info",
          title: "督导结果查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/director/SupervisionTeam/supervision"
          ),
      },
    ],
  },
  {
    path: "/teacher/foundation",
    component: Layout,
    redirect: "/teacher/foundation/list",
    children: [
      {
        path: "list",
        name: "师资基础信息",
        meta: {
          i18n: "info",
          title: "师资基础信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/foundation/list"
          ),
      },
    ],
  },
  /*师资基础信息-批量上传学员图片*/
  {
    path: "/teacher/foundation",
    component: Layout,
    redirect: "/teacher/foundation/moreImg",
    children: [
      {
        path: "moreImg",
        name: "批量上传学员图片",
        meta: {
          i18n: "info",
          title: "批量上传学员图片",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/foundation/moreImg"
          ),
      },
    ],
  },
  /*师资基础信息-编辑*/
  {
    path: "/teacher/foundation",
    component: Layout,
    redirect: "/teacher/foundation/addOrEdit",
    children: [
      {
        path: "addOrEdit",
        name: "编辑",
        meta: {
          i18n: "info",
          title: "编辑",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/foundation/addOrEdit"
          ),
      },
    ],
  },
  /*师资基础信息-查看*/
  {
    path: "/teacher/foundation",
    component: Layout,
    redirect: "/teacher/foundation/addOrEdit",
    children: [
      {
        path: "addOrEdit",
        name: "查看",
        meta: {
          i18n: "info",
          title: "查看",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teacher/foundation/addOrEdit"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/importNum",
    children: [
      {
        path: "importNum&type=1",
        name: "导入学历证书号",
        meta: {
          i18n: "info",
          title: "导入学历证书号",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/importNum"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/importNum",
    children: [
      {
        path: "importNum&type=2",
        name: "导入学位证书号",
        meta: {
          i18n: "info",
          title: "导入学位证书号",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/importNum"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/awardList",
    children: [
      {
        path: "awardList",
        name: "授位学员名册",
        meta: {
          i18n: "info",
          title: "授位学员名册",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/awardList"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/awardDetail",
    children: [
      {
        path: "awardDetail",
        name: "授位学员名册详情",
        meta: {
          i18n: "info",
          title: "授位学员名册详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/awardDetail"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/list",
    children: [
      {
        path: "list",
        name: "毕业学员名册",
        meta: {
          i18n: "info",
          title: "毕业学员名册",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/list"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/detail",
    children: [
      {
        path: "detail",
        name: "毕业学员名册详情",
        meta: {
          i18n: "info",
          title: "毕业学员名册详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/detail"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/awardSetting",
    children: [
      {
        path: "awardSetting",
        name: "授位条件设置",
        meta: {
          i18n: "info",
          title: "授位条件设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/awardSetting"
          ),
      },
    ],
  },
  {
    path: "/student/graduationStudent",
    component: Layout,
    redirect: "/student/graduationStudent/setting",
    children: [
      {
        path: "setting",
        name: "毕业条件设置",
        meta: {
          i18n: "info",
          title: "毕业条件设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/graduationStudent/setting"
          ),
      },
    ],
  },
  {
    path: "/student/studentStatus",
    component: Layout,
    redirect: "/student/studentStatus/infoList",
    children: [
      {
        path: "infoList",
        name: "学籍信息维护",
        meta: {
          i18n: "info",
          title: "学籍信息维护",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/infoList"
          ),
      },
    ],
  },
  {
    path: "/student/historyStudent",
    component: Layout,
    redirect: "/student/historyStudent/list",
    children: [
      {
        path: "list",
        name: "历史学员名册",
        meta: {
          i18n: "info",
          title: "历史学员名册",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/historyStudent/list"
          ),
      },
    ],
  },
  {
    path: "/student/studentStatusDiff",
    component: Layout,
    redirect: "/student/studentStatusDiff/list",
    children: [
      {
        path: "list",
        name: "学籍异动",
        meta: {
          i18n: "info",
          title: "学籍异动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatusDiff/list"
          ),
      },
    ],
  },
  {
    path: "/student/studentStatusDiff",
    component: Layout,
    redirect: "/student/studentStatusDiff/edit",
    children: [
      {
        path: "edit",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatusDiff/edit"
          ),
      },
    ],
  },
  {
    path: "/student/studentStatusDiff",
    component: Layout,
    redirect: "/student/studentStatusDiff/add",
    children: [
      {
        path: "add",
        name: "添加异动处理",
        meta: {
          i18n: "info",
          title: "添加异动处理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatusDiff/add"
          ),
      },
    ],
  },
  {
    path: "/student/roster",
    component: Layout,
    redirect: "/student/roster/list",
    children: [
      {
        path: "list",
        name: "学员花名册",
        meta: {
          i18n: "info",
          title: "学员花名册",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/student/roster/list"),
      },
    ],
  },
  {
    path: "/student/disciplinaryInfo/reward",
    component: Layout,
    redirect: "/student/disciplinaryInfo/reward/list",
    children: [
      {
        path: "list",
        name: "学员奖励信息管理",
        meta: {
          i18n: "info",
          title: "学员奖励信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/reward/list"
          ),
      },
    ],
  },
  /* 学员管理 学员奖励信息管理 编辑行信息*/
  {
    path: "/student/disciplinaryInfo/reward",
    component: Layout,
    redirect: "/student/disciplinaryInfo/reward/editInfo",
    children: [
      {
        path: "editInfo",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/reward/editInfo"
          ),
      },
    ],
  },
  /* 学员管理 学员奖励信息管理 新增信息*/
  {
    path: "/student/disciplinaryInfo/reward",
    component: Layout,
    redirect: "/student/disciplinaryInfo/reward/add",
    children: [
      {
        path: "add",
        name: "新增",
        meta: {
          i18n: "info",
          title: "新增",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/reward/add"
          ),
      },
    ],
  },
  /* 学员管理 学员处分信息管理*/
  {
    path: "/student/disciplinaryInfo/punishment",
    component: Layout,
    redirect: "/student/disciplinaryInfo/punishment/list",
    children: [
      {
        path: "list",
        name: "学员处分信息管理",
        meta: {
          i18n: "info",
          title: "学员处分信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/punishment/list"
          ),
      },
    ],
  },
  /* 学员管理 学员处分信息管理 编辑行信息*/
  {
    path: "/student/disciplinaryInfo/punishment",
    component: Layout,
    redirect: "/student/disciplinaryInfo/punishment/editInfo",
    children: [
      {
        path: "editInfo",
        name: "修改",
        meta: {
          i18n: "info",
          title: "修改",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/punishment/editInfo"
          ),
      },
    ],
  },
  /* 学员管理 学员处分信息管理 新增信息*/
  {
    path: "/student/disciplinaryInfo/punishment",
    component: Layout,
    redirect: "/student/disciplinaryInfo/punishment/add",
    children: [
      {
        path: "add",
        name: "新增",
        meta: {
          i18n: "info",
          title: "新增",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/disciplinaryInfo/punishment/add"
          ),
      },
    ],
  },
  /*学籍管理*/
  {
    path: "/student/studentStatus/registerList",
    component: Layout,
    redirect: "/student/studentStatus/registerList/registerList",
    children: [
      {
        path: "registerList",
        name: "学籍注册",
        meta: {
          i18n: "info",
          title: "学籍注册",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/registerList/registerList"
          ),
      },
    ],
  },
  /* 学员管理 学籍管理 学籍注册 编辑*/
  {
    path: "/student/studentStatus/registerList",
    component: Layout,
    redirect: "/student/studentStatus/registerList/edit",
    children: [
      {
        path: "edit",
        name: "编辑",
        meta: {
          i18n: "info",
          title: "编辑",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/registerList/edit"
          ),
      },
    ],
  },
  /* 学员管理 学籍管理 学籍注册 批量上传学员图片*/
  {
    path: "/student/studentStatus/registerList",
    component: Layout,
    redirect: "/student/studentStatus/registerList/picture",
    children: [
      {
        path: "picture",
        name: "批量上传学员图片",
        meta: {
          i18n: "info",
          title: "批量上传学员图片",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/registerList/picture"
          ),
      },
    ],
  },
  /* 学员管理 学籍管理 学籍导入*/
  {
    path: "/student/studentStatus/registerList",
    component: Layout,
    redirect: "/student/studentStatus/registerList/import",
    children: [
      {
        path: "import",
        name: "学籍导入",
        meta: {
          i18n: "info",
          title: "学籍导入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/registerList/import"
          ),
      },
    ],
  },
  /* 学员管理 学籍注册 学员个人信息查询*/
  {
    path: "/student/studentStatus",
    component: Layout,
    redirect: "/student/studentStatus/infoSearch",
    children: [
      {
        path: "infoSearch",
        name: "个人信息查询",
        meta: {
          i18n: "info",
          title: "个人信息查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/studentStatus/infoSearch"
          ),
      },
    ],
  },
  /* 学员管理 学籍注册 添加信息*/
  {
    path: "/student/studentStatus",
    component: Layout,
    redirect: "/student/studentStatus/addInfo",
    children: [
      {
        path: "addInfo",
        name: "添加信息",
        meta: {
          i18n: "info",
          title: "添加信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views//student/studentStatus/addInfo"
          ),
      },
    ],
  },
  /* 教学运行数据管理 基础数据*/
  {
    path: "/teaching",
    component: Layout,
    redirect: "/teaching/basicData",
    children: [
      {
        path: "basicData",
        name: "教学基础数据统计",
        meta: {
          i18n: "info",
          title: "教学基础数据统计",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/teaching/basicData"),
      },
    ],
  },
  /* 教学运行数据管理 基础数据*/
  {
    path: "/teaching",
    component: Layout,
    redirect: "/teaching/basicData2",
    children: [
      {
        path: "basicData2",
        name: "教学基础数据统计2",
        meta: {
          i18n: "info",
          title: "教学基础数据统计2",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/teaching/basicData2"),
      },
    ],
  },
  /* 教学运行数据管理 调停课情况查询*/
  {
    path: "/teaching/frontlineData",
    component: Layout,
    redirect: "/teaching/frontlineData/adjustment",
    children: [
      {
        path: "adjustment",
        name: "一线数据统计分析-调停课情况查询",
        meta: {
          i18n: "info",
          title: "一线数据统计分析-调停课情况查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/adjustment"
          ),
      },
    ],
  },
  /* 教学运行数据管理 听课情况查询*/
  {
    path: "/teaching/frontlineData",
    component: Layout,
    redirect: "/teaching/frontlineData/attendClass",
    children: [
      {
        path: "attendClass",
        name: "一线数据分类统计分析-听课情况查询",
        meta: {
          i18n: "info",
          title: "一线数据分类统计分析-听课情况查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/attendClass"
          ),
      },
    ],
  },
  /* 教学运行数据管理 跟课情况查询*/
  {
    path: "/teaching/frontlineData",
    component: Layout,
    redirect: "/teaching/frontlineData/followUp",
    children: [
      {
        path: "followUp",
        name: "一线数据分类统计分析-跟课情况查询",
        meta: {
          i18n: "info",
          title: "一线数据分类统计分析-跟课情况查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/followUp"
          ),
      },
    ],
  },
  /* 教学运行数据管理 一线数据查询*/
  {
    path: "/teaching/frontlineData",
    component: Layout,
    redirect: "/teaching/frontlineData/search",
    children: [
      {
        path: "search",
        name: "一线数据查询",
        meta: {
          i18n: "info",
          title: "一线数据查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/search"
          ),
      },
    ],
  },
  /* 教学运行数据管理 一线数据填报*/
  {
    path: "/teaching/frontlineData/filling",
    component: Layout,
    redirect: "/teaching/frontlineData/filling/filling",
    children: [
      {
        path: "filling",
        name: "一线数据填报",
        meta: {
          i18n: "info",
          title: "一线数据填报",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/filling/filling"
          ),
      },
    ],
  },
  /* 教学运行数据管理 一线数据指标管理*/
  {
    path: "/teaching/frontlineData/dataIndex",
    component: Layout,
    redirect: "/teaching/frontlineData/dataIndex/list",
    children: [
      {
        path: "list",
        name: "一线数据指标管理",
        meta: {
          i18n: "info",
          title: "一线数据指标管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/frontlineData/dataIndex/list"
          ),
      },
    ],
  },
  /* 优质课 候选对象数据*/
  {
    path: "/qualityCourse",
    component: Layout,
    redirect: "/qualityCourse/list",
    children: [
      {
        path: "list",
        name: "候选对象数据",
        meta: {
          i18n: "info",
          title: "候选对象数据",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/qualityCourse/list"),
      },
    ],
  },
  /* 优质课 评选结果数据*/
  {
    path: "/qualityCourse",
    component: Layout,
    redirect: "/qualityCourse/list2",
    children: [
      {
        path: "list2",
        name: "评选结果数据",
        meta: {
          i18n: "info",
          title: "评选结果数据",
        },
        component: () =>
          import(/* webpackChunkName: "views" */ "@/views/qualityCourse/list2"),
      },
    ],
  },
  /*  教学绩效信息管理 教学研究情况*/
  {
    path: "/teaching/performance",
    component: Layout,
    redirect: "/teaching/performance/research",
    children: [
      {
        path: "research",
        name: "教学研究情况",
        meta: {
          i18n: "info",
          title: "教学研究情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/performance/research"
          ),
      },
    ],
  },
  /*  教学绩效信息管理 授课情况*/
  {
    path: "/teaching/performance",
    component: Layout,
    redirect: "/teaching/performance/skList",
    children: [
      {
        path: "skList",
        name: "授课情况",
        meta: {
          i18n: "info",
          title: "授课情况",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/performance/skList"
          ),
      },
    ],
  },
  /*  教学绩效信息管理 授予权限*/
  {
    path: "/teaching/performance",
    component: Layout,
    redirect: "/teaching/performance/givenQX",
    children: [
      {
        path: "givenQX",
        name: "授予权限",
        meta: {
          i18n: "info",
          title: "授予权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/performance/givenQX"
          ),
      },
    ],
  },
  /*  教学绩效信息管理 授予权限*/
  {
    path: "/teaching/performance",
    component: Layout,
    redirect: "/teaching/performance/qxSee",
    children: [
      {
        path: "qxSee",
        name: "授予权限",
        meta: {
          i18n: "info",
          title: "授予权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/performance/qxSee"
          ),
      },
    ],
  },
  /*  教学绩效信息管理 授予权限*/
  {
    path: "/teaching/performance",
    component: Layout,
    redirect: "/teaching/performance/qxSee1",
    children: [
      {
        path: "qxSee1",
        name: "授予权限",
        meta: {
          i18n: "info",
          title: "授予权限",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teaching/performance/qxSee1"
          ),
      },
    ],
  },
  /*  公共考试信息管理 公共考试成绩查询*/
  {
    path: "/publicExams",
    component: Layout,
    redirect: "/publicExams/scoreQuery",
    children: [
      {
        path: "scoreQuery",
        name: "公共考试成绩查询",
        meta: {
          i18n: "info",
          title: "公共考试成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/publicExams/scoreQuery"
          ),
      },
    ],
  },
  
  /*  公共考试信息管理 公共考试成绩查询*/
  {
    path: "/publicExams",
    component: Layout,
    redirect: "/publicExams/commonRecord",
    children: [
      {
        path: "commonRecord",
        name: "公共考试成绩查询",
        meta: {
          i18n: "info",
          title: "公共考试成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/publicExams/commonRecord"
          ),
      },
    ],
  },
  /*  公共考试信息管理 公共考试类型管理*/
  {
    path: "/publicExams/typeList",
    component: Layout,
    redirect: "/publicExams/typeList/list",
    children: [
      {
        path: "list",
        name: "公共考试类型管理",
        meta: {
          i18n: "info",
          title: "公共考试类型管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/publicExams/typeList/list"
          ),
      },
    ],
  },
  /*  公共考试信息管理 公共考试信息管理*/
  {
    path: "/publicExams/infoList",
    component: Layout,
    redirect: "/publicExams/infoList/list",
    children: [
      {
        path: "list",
        name: "公共考试信息管理",
        meta: {
          i18n: "list",
          title: "公共考试信息管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/publicExams/infoList/list"
          ),
      },
    ],
  },
    /*  公共考试信息管理 公共考试信息管理*/
    {
      path: "/publicExams/infoList",
      component: Layout,
      redirect: "/publicExams/infoList/viewTranscript",
      children: [
        {
          path: "viewTranscript",
          name: "查看成绩",
          meta: {
            i18n: "list",
            title: "查看成绩",
          },
          component: () =>
            import(
              /* webpackChunkName: "views" */ "@/views/publicExams/infoList/viewTranscript"
            ),
        },
      ],
    },
  /* 指向同一个画面 */
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/studentScoreEntry",
    children: [
      {
        path: "studentScoreEntry",
        name: "学员成绩录入",
        meta: {
          i18n: "info",
          title: "学员成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/leadership/courseAchievement",
    component: Layout,
    redirect: "/leadership/courseAchievement/printEntry",
    children: [
      {
        path: "printEntry",
        name: "学员成绩录入",
        meta: {
          i18n: "info",
          title: "学员成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/director/courseAchievement",
    component: Layout,
    redirect: "/director/courseAchievement/printEntry",
    children: [
      {
        path: "printEntry",
        name: "学员成绩录入",
        meta: {
          i18n: "info",
          title: "学员成绩录入",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/teacher/courseAchievement",
    component: Layout,
    redirect: "/teacher/courseAchievement/printEntry",
    children: [
      {
        path: "printEntry",
        name: "录入、打印课程教程",
        meta: {
          i18n: "info",
          title: "录入、打印课程教程",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/studentScoreEntry"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/adming",
    children: [
      {
        path: "adming",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/director/scoreManagement",
    component: Layout,
    redirect: "/director/scoreManagement/adminCourseAssessment",
    children: [
      {
        path: "adminCourseAssessment",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/battalionChief/scoreManagement",
    component: Layout,
    redirect: "/battalionChief/scoreManagement/adminCourseAssessment",
    children: [
      {
        path: "adminCourseAssessment",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/adminCourseAssessment",
    children: [
      {
        path: "adminCourseAssessment",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/adminC",
    children: [
      {
        path: "adminC",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/admin/scoreManagement",
    component: Layout,
    redirect: "/admin/scoreManagement/adminj",
    children: [
      {
        path: "adminj",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/scoreManagement/adming"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/examinationPlan/makeupPlan",
    component: Layout,
    redirect: "/admin/courseAssessment/examinationPlan/makeupPlan/index",
    children: [
      {
        path: "index",
        name: "补缓考安排",
        meta: {
          i18n: "info",
          title: "补缓考安排",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/examinationPlan/makeupPlan/index"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/scoreManagement/fail",
    component: Layout,
    redirect: "/admin/courseAssessment/scoreManagement/fail/index",
    children: [
      {
        path: "index",
        name: "不及格人员查询",
        meta: {
          i18n: "info",
          title: "不及格人员查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/scoreManagement/fail/index"
          ),
      },
      {
        path: "review",
        name: "查看历年成绩查询",
        meta: {
          i18n: "info",
          title: "查看历年成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/scoreManagement/fail/review"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/scoreManagement/inetElectives",
    component: Layout,
    redirect: "/admin/courseAssessment/scoreManagement/inetElectives/index",
    children: [
      {
        path: "index",
        name: "互联网选修课成绩审核",
        meta: {
          i18n: "info",
          title: "互联网选修课成绩审核",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/scoreManagement/inetElectives/index"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/questionExamination/questions",
    component: Layout,
    redirect: "/admin/courseAssessment/questionExamination/questions/index",
    children: [
      {
        path: "index",
        name: "试题库管理",
        meta: {
          i18n: "info",
          title: "试题库管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/questions/index"
          ),
      },
      {
        path: "clazzQuestions",
        name: "试题库详情",
        meta: {
          i18n: "info",
          title: "试题库详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/questions/clazzQuestions"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/questionExamination/examinations",
    component: Layout,
    redirect: "/admin/courseAssessment/questionExamination/examinations/index",
    children: [
      {
        path: "index",
        name: "试卷库管理",
        meta: {
          i18n: "info",
          title: "试卷库管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/examinations/index"
          ),
      },
      {
        path: "createNew",
        name: "新增试卷",
        meta: {
          i18n: "info",
          title: "新增试卷",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/examinations/createNew"
          ),
      },
      {
        path: "detail",
        name: "试卷详情",
        meta: {
          i18n: "info",
          title: "试卷详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/examinations/detail"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/performanceAnalysis/distribution",
    component: Layout,
    redirect: "/admin/courseAssessment/performanceAnalysis/distribution/index",
    children: [
      {
        path: "index",
        name: "课程考核成绩分布",
        meta: {
          i18n: "info",
          title: "课程考核成绩分布",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/performanceAnalysis/distribution/index"
          ),
      },
      {
        path: "statistics",
        name: "课程考核成绩分布详情",
        meta: {
          i18n: "info",
          title: "详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/performanceAnalysis/distribution/statistics"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/performanceAnalysis/leagueTables",
    component: Layout,
    redirect: "/admin/courseAssessment/performanceAnalysis/leagueTables/index",
    children: [
      {
        path: "index",
        name: "学员成绩排名",
        meta: {
          i18n: "info",
          title: "学员成绩排名",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/performanceAnalysis/leagueTables/index"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/questionExamination/onlineTest",
    component: Layout,
    redirect: "/admin/courseAssessment/questionExamination/onlineTest/index",
    children: [
      {
        path: "index",
        name: "在线考试",
        meta: {
          i18n: "info",
          title: "在线考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/onlineTest/index"
          ),
      },
      {
        path: "review",
        name: "查看考试",
        meta: {
          i18n: "info",
          title: "查看考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/onlineTest/review"
          ),
      },
      {
        path: "Submit",
        name: "提交考试",
        meta: {
          i18n: "info",
          title: "提交考试",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/student/onlinetest/Submit"
          ),
      },
      {
        path: "marking",
        name: "开始阅卷",
        meta: {
          i18n: "info",
          title: "开始阅卷",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/questionExamination/onlineTest/marking"
          ),
      },
    ],
  },
  {
    path: "/student/onlinetest",
    component: Layout,
    redirect: "/views/student/onlinetest/index",
    children: [
      {
        path: "index",
        name: "在线考试(学员)",
        meta: {
          i18n: "info",
          title: "在线考试(学员)",
        },
        component: () =>
          import(
            "@/views/student/onlinetest/index.vue"
          ) /* @/views/student/inetElectives/index*/,
        /* webpackChunkName: "views" */
      },
    ],
  },
  {
    path: "/views/student/onlinetest/marking",
    component: Layout,
    redirect: "/views/student/onlinetest/marking",
    children: [
      {
        path: "index",
        name: "开始考试",
        meta: {
          i18n: "info",
          title: "开始考试",
        },
        component: () =>
          import(
            "@/views/student/onlinetest/marking.vue"
          ) /* @/views/student/inetElectives/index*/,
        /* webpackChunkName: "views" */
      },
    ],
  },

  {
    path: "/leadership/scoreManagement",
    component: Layout,
    redirect: "/leadership/scoreManagement/adminCourseAssessment",
    children: [
      {
        path: "adminCourseAssessment",
        name: "课程考核成绩查询",
        meta: {
          i18n: "info",
          title: "课程考核成绩查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/leadership/scoreManagement/adminCourseAssessment"
          ),
      },
    ],
  },
  {
    path: "/admin/planManagement",
    component: Layout,
    redirect: "/admin/planManagement/editPlanManages",
    children: [
      {
        path: "editPlanManages",
        name: "查看课程考核实施计划",
        meta: {
          i18n: "info",
          title: "查看课程考核实施计划",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/planManagement/editPlanManages"
          ),
      },
    ],
  },
  {
    path: "/admin/propositionReview",
    component: Layout,
    redirect: "/admin/propositionReview/subpoenaStaue",
    children: [
      {
        path: "subpoenaStaue",
        name: "制作试卷封皮袋",
        meta: {
          i18n: "info",
          title: "制作试卷封皮袋",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/propositionReview/subpoenaStaue"
          ),
      },
    ],
  },

  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/csReport",
    children: [
      {
        path: "csReport",
        name: "查看专业班次学员信息统计",
        meta: {
          i18n: "info",
          title: "查看专业班次学员信息统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/csTest/csReport"
          ),
      },
    ],
  },
  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/editClass",
    children: [
      {
        path: "editClass",
        name: "专业班次信息",
        meta: {
          i18n: "info",
          title: "专业班次信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/csTest/editClass"
          ),
      },
    ],
  },
  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/batchCreate",
    children: [
      {
        path: "batchCreate",
        name: "批量创建专业班次",
        meta: {
          i18n: "info",
          title: "批量创建专业班次",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/csTest/batchCreate"
          ),
      },
    ],
  },
  {
    path: "/admin/csTest",
    component: Layout,
    redirect: "/admin/csTest/viewClass",
    children: [
      {
        path: "viewClass",
        name: "专业班次详情",
        meta: {
          i18n: "info",
          title: "专业班次详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/csTest/viewClass"
          ),
      },
    ],
  },
  // {
  //   path: "/admin/csTest",
  //   component: Layout,
  //   redirect: '/admin/csTest/classRoom',
  //   children: [{
  //     path: 'classRoom',
  //     name: '选择教室场地',
  //     meta: {
  //       i18n: 'info',
  //       title: "选择教室场地"
  //     },
  //     component: () =>
  //       import( /* webpackChunkName: "views" */ '@/views/admin/csTest/classRoom')
  //   }]
  // },
  {
    path: "/admin/ManagementHours",
    component: Layout,
    redirect: "/admin/ManagementHours/classHourStandard",
    children: [
      {
        path: "classHourStandard",
        name: "课时标准设置",
        meta: {
          i18n: "info",
          title: "课时标准设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/ManagementHours/classHourStandard"
          ),
      },
    ],
  },
  {
    path: "/admin/courseAssessment/scoreManagement/inetElectives",
    component: Layout,
    redirect: "/admin/courseAssessment/scoreManagement/inetElectives/filling",
    children: [
      {
        path: "filling",
        name: "互联网选修课成绩填报",
        meta: {
          i18n: "info",
          title: "互联网选修课成绩填报",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/courseAssessment/scoreManagement/inetElectives/filling"
          ),
      },
    ],
  },
  {
    path: "/admin/teachingEvaluation/summarySet",
    component: Layout,
    redirect: "/admin/teachingEvaluation/summarySet/index",
    children: [
      {
        path: "index",
        name: "汇总设置",
        meta: {
          i18n: "info",
          title: "汇总设置",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/teachingEvaluation/summarySet/index"
          ),
      },
    ],
  },
  {
    path: "/admin/teachingEvaluation/evaluationResult",
    component: Layout,
    redirect: "/admin/teachingEvaluation/evaluationResult/index",
    children: [
      {
        path: "index",
        name: "综合评教结果查询",
        meta: {
          i18n: "info",
          title: "综合评教结果查询",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/teachingEvaluation/evaluationResult/index"
          ),
      },
    ],
  },
  {
    path: "/admin/teachingEvaluation/dataSummary",
    component: Layout,
    redirect: "/admin/teachingEvaluation/dataSummary/index",
    children: [
      {
        path: "index",
        name: "数据汇总",
        meta: {
          i18n: "info",
          title: "数据汇总",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/teachingEvaluation/dataSummary/index"
          ),
      },
    ],
  },
  {
    path: "/admin/teachingEvaluation/peerEvaluation",
    component: Layout,
    redirect: "/admin/teachingEvaluation/peerEvaluation/index",
    children: [
      {
        path: "index",
        name: "同行评教结果提交",
        meta: {
          i18n: "info",
          title: "同行评教结果提交",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/admin/teachingEvaluation/peerEvaluation/index"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/topicType",
    component: Layout,
    redirect: "/teachingTopics/topicType/list",
    children: [
      {
        path: "list",
        name: "课题类别管理",
        meta: {
          i18n: "info",
          title: "课题类别管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/topicType/list"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/topicType",
    component: Layout,
    redirect: "/teachingTopics/topicType/stageList",
    children: [
      {
        path: "stageList",
        name: "课题阶段管理",
        meta: {
          i18n: "info",
          title: "课题阶段管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/topicType/stageList"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/activity",
    component: Layout,
    redirect: "/teachingTopics/activity/declare",
    children: [
      {
        path: "declare",
        name: "课题申报",
        meta: {
          i18n: "info",
          title: "课题申报",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/activity/declare"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/activity",
    component: Layout,
    redirect: "/teachingTopics/activity/declareForm",
    children: [
      {
        path: "declareForm",
        name: "申报",
        meta: {
          i18n: "info",
          title: "申报",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/activity/declareForm"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/activity",
    component: Layout,
    redirect: "/teachingTopics/activity/newList",
    children: [
      {
        path: "newList",
        name: "创建新的课题活动",
        meta: {
          i18n: "info",
          title: "创建新的课题活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/activity/newList"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/activity",
    component: Layout,
    redirect: "/teachingTopics/activity/infoList",
    children: [
      {
        path: "infoList",
        name: "课题信息统计",
        meta: {
          i18n: "info",
          title: "课题信息统计",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/activity/infoList"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics",
    component: Layout,
    redirect: "/teachingTopics/allTopic",
    children: [
      {
        path: "allTopic",
        name: "查询所有课题信息",
        meta: {
          i18n: "info",
          title: "查询所有课题信息",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/allTopic"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/standard",
    component: Layout,
    redirect: "/teachingTopics/review/standard/list",
    children: [
      {
        path: "list",
        name: "评审标准管理",
        meta: {
          i18n: "info",
          title: "评审标准管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/standard/list"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/standard",
    component: Layout,
    redirect: "/teachingTopics/review/standard/seeStandard",
    children: [
      {
        path: "seeStandard",
        name: "查看评审标准",
        meta: {
          i18n: "info",
          title: "查看评审标准",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/standard/seeStandard"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/newActivity",
    component: Layout,
    redirect: "/teachingTopics/review/newActivity/list",
    children: [
      {
        path: "list",
        name: "创建新的评审活动",
        meta: {
          i18n: "info",
          title: "创建新的评审活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/newActivity/list"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/newActivity",
    component: Layout,
    redirect: "/teachingTopics/review/newActivity/add",
    children: [
      {
        path: "add",
        name: "新增评审活动",
        meta: {
          i18n: "info",
          title: "新增评审活动",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/newActivity/add"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/expert",
    component: Layout,
    redirect: "/teachingTopics/review/expert/statistics",
    children: [
      {
        path: "statistics",
        name: "评审结果统计分析",
        meta: {
          i18n: "info",
          title: "评审结果统计分析",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/expert/statistics"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/expert",
    component: Layout,
    redirect: "/teachingTopics/review/expert/starting",
    children: [
      {
        path: "starting",
        name: "开始评审",
        meta: {
          i18n: "info",
          title: "开始评审",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/expert/starting"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/expert",
    component: Layout,
    redirect: "/teachingTopics/review/expert/ps",
    children: [
      {
        path: "ps",
        name: "评审",
        meta: {
          i18n: "info",
          title: "评审",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/expert/ps"
          ),
      },
    ],
  },
  {
    path: "/teachingTopics/review/expert",
    component: Layout,
    redirect: "/teachingTopics/review/expert/list",
    children: [
      {
        path: "list",
        name: "专家评审",
        meta: {
          i18n: "info",
          title: "专家评审",
        },
        component: () =>
          import(
            /* webpackChunkName: "views" */ "@/views/teachingTopics/review/expert/list"
          ),
      },
    ],
  },
  {
    path: "/admin/teachingEvaluation/superviseEvaluation",
    component: Layout,
    redirect: '/admin/teachingEvaluation/superviseEvaluation/index',
    children: [{
      path: 'index',
      name: '督导评教结果提交',
      meta: {
        i18n: 'info',
        title: "督导评教结果提交"
      },
      component: () =>
        import( /* webpackChunkName: "views" */ '@/views/admin/teachingEvaluation/superviseEvaluation/index')
    }]
  }
];
