let handlerFun; // 有消息返回 要执行的代码
var lockReconnect = false;//避免重复连接
var wsUrl = "";
var ws;
var tt;
import { getUserInfo } from "../api/system/user";
import { getStore } from "@/util/store.js";
// 获取用户基础信息
var userInfo =  getStore({ // 在localStorage 中获取
  name: "userInfo",
  debug: true,
}) || {};
// 如果没有数据 就请求接口获取
if (!userInfo.content) {
  getUserInfo().then(res => {
    userInfo = res.data.data;
    userInfo.user_id = userInfo.id
  })
}
userInfo = userInfo.content
// console.log(userInfo)

/**
 * 建立websocket连接
 * @param {string} url ws地址
 */
export const createSocket = (url, handlerMsg) => {
  wsUrl = url
  handlerFun = handlerMsg;
  try {
    // 建立websocket连接
    if (typeof (WebSocket) === "undefined") {
      alert("您的浏览器不支持socket")
    } else {
      ws = new WebSocket(url);
    }
    // 初始化
    init();
  } catch (e) {
    // console.log('catch');
    reconnect(url);
  }
}
const init = function () {
  // console.log('init')
  ws.onclose = function () {
    // console.log('链接关闭');
    reconnect(wsUrl);
  };
  ws.onopen = function () {
    console.log("心跳检测重置")
    let user = userInfo.user_id;
    ws.send(',join,' + user);
    heartCheck.start();
  };
  ws.onerror = function () {
    // console.log('发生异常了');
    reconnect(wsUrl);
  };
  ws.onmessage = function (msg) {
    //拿到任何消息都说明当前连接是正常的
    // console.log(1213)
    var message = JSON.parse(msg.data);
    // var message = msg.data;
    // heartCheck.start();

    // 顶部消息弹出框 弹出方法 \src\page\index\top\top-notice.vue
    handlerFun(message);
  }
}

const reconnect = (url) => {
  if (lockReconnect) {
    return;
  }
  lockReconnect = true;
  //没连接上会一直重连，设置延迟避免请求过多
  tt && clearTimeout(tt);
  tt = setTimeout(function () {
    createSocket(url, handlerFun);
    lockReconnect = false;
  }, 4000);
}

var heartCheck = {
  timeout: 5000,
  timeoutObj: null,
  serverTimeoutObj: null,
  start: function () {
    this.timeoutObj && clearTimeout(this.timeoutObj);
    this.timeoutObj = setTimeout(function () {
      //这里发送一个心跳，后端收到后，返回一个心跳消息，
      var user = userInfo.user_id;
      ws.send(',heart,' + user);
      // console.log("发送一个心跳")
    }, this.timeout)
  }
}






// let socket = ''
// let handlerFun;


// import { getUserInfo } from "../api/system/user";
// /**
//  * 建立websocket连接
//  * @param {string} url ws地址
//  */
// export const createSocket = (url, handlerMsg) => {
//   if (typeof (WebSocket) === "undefined") {
//     alert("您的浏览器不支持socket")
//   } else {
//     // 实例化socket
//     socket = new WebSocket(url)
//     handlerFun = handlerMsg;
//     // 监听socket连接
//     socket.onopen = open
//     // 监听socket错误信息
//     socket.onerror = error
//     // 监听socket消息
//     socket.onmessage = getMessage
//   }
// }


// const open = () => {
//   getUserInfo().then(res => {
//     const users = res.data.data;
//     var user = users.id;
//     socket.send(',join,' + user);
//   })
//   console.log("socket连接成功")
// }

// /**连接失败 */
// const error = () => {
//   console.log("连接错误")
// }

// // 创建接收消息函数
// const getMessage = msg => {
//   console.log(msg, "msg")
//   var message = JSON.parse(msg.data);
//   handlerFun(message);
// }



// export const aaaaa = params => {
//   socket.send(params)
// }

// const send = params => {
// }

// const close = () => {
//   console.log("socket已经关闭")
// }
// export const destroyed = () => {
//   // 销毁监听
//   this.socket.onclose = this.close
// }
