/**
 * 单点登录
 */
import { getToken } from '@/util/auth';
import store from '../store';
import { resetRouter } from "@/router/router.js";
const init = (callback) => {
    let token = getToken();
    let message = getUrlParam("sso_session");
    if (token) {
        loginSuccess(callback);
    } else {
        if (message) {
            store.dispatch("LoginBySSO", message).then((data) => {
                if (data.length !== 0) {
                    resetRouter();
                }
                loginSuccess(callback);
            }).catch(() => {});
        } else {
            let sevice = "http://" + window.location.host;// + "/systemPages/chiefSpace";
            let serviceUrl = encodeURIComponent(sevice);
            window.location.href = window._CONFIG.ssoPrefixUrl + "/login?redirect_url=" + serviceUrl;
        }
    }
};
const SSO = {
    init: init
};

function getUrlParam(paraName) {
    let url = document.location.toString();
    let arrObj = url.split("?");

    if (arrObj.length > 1) {
        let arrPara = arrObj[1].split("&");
        let arr;

        for (let i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");

            if (arr != null && arr[0] == paraName) {
                return arr[1];
            }
        }
        return "";
    } else {
        return "";
    }
}

function loginSuccess(callback) {
    callback();
}
export default SSO;
