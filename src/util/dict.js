export default 
  //////////////////////// 课程节次 ////////////////////////
  function initSection() {
    return [
      {dictValue: '1', dictLabel: '第一节'},
      {dictValue: '2', dictLabel: '第二节'},
      {dictValue: '3', dictLabel: '第三节'},
      {dictValue: '4', dictLabel: '第四节'},
      {dictValue: '5', dictLabel: '第五节'},
      {dictValue: '6', dictLabel: '第六节'},
      {dictValue: '7', dictLabel: '第七节'},
      {dictValue: '8', dictLabel: '第八节'},
      {dictValue: '9', dictLabel: '第九节'},
      {dictValue: '10', dictLabel: '第十节'}
    ]
  }