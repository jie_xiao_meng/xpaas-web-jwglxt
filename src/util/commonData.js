import {rewardTypeList,rewardGradeList,getPunishType,getFirstLevel,getStudentDui,getZYBC,getXQList,getTeacherXiList,getTeacherJYSList} from "@/api/student"
import {getCurrentSysTerm,getTermWeek} from "@/api/lineData"
import {getallTypeList,allActivityList} from "@/api/teachingTopic"

//奖励类别
export const getRewardTypeList = async() => {
    const res = rewardTypeList()   
    return res 
}
//奖励级别
export const getGradeList = async() => {
    const res = rewardGradeList()   
    return res 
}
//处分类型
export const getPunishList = async(params) => {
    const res = getPunishType(params)   
    return res 
}
//业务自典
export const getCommonData = async(params) => {
    const res = getFirstLevel(params)   
    return res 
}
//学员队
export const getStudentDuiData = async(params) => {
    const res = getStudentDui(params)   
    return res 
}
//专业班次
export const getZybc = async(params) => {
    const res = getZYBC(params)   
    return res 
}
//获取学期
export const getXQOption = async(params) => {
    const res = getXQList(params)   
    return res 
}
//获取系
export const getXIOption = async() => {
    const res = getTeacherXiList({deptName:''})   
    return res 
}
//获取教研室
export const getJYSOption = async() => {
    const res = getTeacherJYSList({deptName:''})   
    return res 
}
//获取当前学期信息
export const getCurrentXQ = async() => {
    const res = getCurrentSysTerm()   
    return res 
}
//获取当前学期信息
export const getWeeks = async(params) => {
    const res = getTermWeek(params)   
    return res 
}
//获得所有活动所属类别
export const getAllType = async(params) => {
    const res = getallTypeList(params)   
    return res 
}
//获得所有课题活动
export const getAllActivity = async(params) => {
    const res = allActivityList(params)   
    return res 
}