import axios from 'axios'
export function downLoadZip(ids) {
  axios({
    method: 'post',
    url: '/api/xpaas-develop/code/gen-code-down',
    data: {
      ids,
      system: 'xpaasui'
    },
    responseType: 'blob'
  }).then(data => {
    if (!data.data) {
      return
    }
    const blob = new Blob([data.data], { type: 'application/zip' })
    const url = window.URL.createObjectURL(blob)
    const link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    link.setAttribute('download', 'xpaas_code.zip')
    document.body.appendChild(link)
    link.click()
  }).catch((error) => {

  })
}
import {Message} from 'element-ui'

export function downloadAction(url, method, parameter, filename) {
  return axios({
      url: url,
      method: method,
      params: parameter,
      responseType: 'blob',
  }).then(response => {
      const type = response.data.type || ''
      if (type.includes('application/json')) {
          let reader = new FileReader()
          reader.onload = e => {
              if (e.target.readyState === 2) {
                  let res = {}
                  res = JSON.parse(e.target.result)
                  Message.error(res.msg)
              }
          }
          reader.readAsText(response.data)
      } else {
          filename = decodeURI(filename);
          if (typeof window.navigator.msSaveBlob !== 'undefined') {
              window.navigator.msSaveBlob(response.data, filename)
          } else {
              var blobURL = window.URL.createObjectURL(response.data)// 将blob对象转为一个URL
              var tempLink = document.createElement('a')// 创建一个a标签
              tempLink.style.display = 'none'
              tempLink.href = blobURL
              tempLink.setAttribute('download', filename)// 给a标签添加下载属性
              if (typeof tempLink.download === 'undefined') {
                  tempLink.setAttribute('target', '_blank')
              }
              document.body.appendChild(tempLink)// 将a标签添加到body当中
              tempLink.click()// 启动下载
              document.body.removeChild(tempLink)// 下载完毕删除a标签
              window.URL.revokeObjectURL(blobURL)
          }
      }
  }).catch((error) => {
      console.log(error)
  })
}