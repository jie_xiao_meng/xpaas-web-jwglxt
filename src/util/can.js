export default {
    a: {},
    canDo(e = {}) {
        if (!this.a[e.key]) {
            this.lockKey(e.key);
            e.success && e.success();
            setTimeout(() => {
                this.releaseKey(e.key)
            }, e.miao ? e.miao : 2000)
        } else {
            e.fail && e.fail()
        }
    },
    releaseKey(key) {
        delete this.a[key]
    },
    lockKey(key) {
        this.a[key] = true
    }
}