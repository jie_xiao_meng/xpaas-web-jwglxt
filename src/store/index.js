import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import common from './modules/common'
import tags from './modules/tags'
import logs from './modules/logs'
import dict from './modules/dict'
import addtestpaper from './modules/addtestpaper'
import getters from './getters'
import tagsView from "@/store/modules/tagsView";
import studentUrl from "@/store/modules/baseUrl";

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    user,
    common,
    logs,
    tags,
    dict,
    tagsView,
    studentUrl,
    addtestpaper
  },
  getters,
})

export default store
