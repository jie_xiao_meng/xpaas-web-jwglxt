export const optionParent = {
    height: 'auto',
    calcHeight: 95,
    tip: false,
    searchShow: true,
    searchMenuSpan: 6,
    searchMenuPosition: "left",
    menuWidth: 150,
    tree: true,
    selection: true,
    border: false,
    index: true,
    indexLabel: '序号',
    viewBtn: true,
    refreshBtn: false,
    columnBtn: false,
    dialogClickModal: false,
    column: [{
            label: "字典编号",
            width: 120,
            overHidden: true,
            labelPosition: "top",
            maxlength: 30,
            showWordLimit: false,
            search: true,
            searchSpan: 8,
            prop: "code",
            rules: [{
                required: true,
                message: "请输入字典编号",
                trigger: "blur"
            }]
        },
        {
            label: "字典名称",
            labelPosition: "top",
            search: true,
            searchSpan: 8,
            prop: "dictValue",
            maxlength: 30,
            showWordLimit: false,
            rules: [{
                required: true,
                message: "请输入字典名称",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "字典排序",
            prop: "sort",
            labelPosition: "top",
            type: "number",
            rules: [{
                required: true,
                message: "请输入字典排序",
                trigger: "blur"
            }]
        },
        {
            label: "封存",
            labelPosition: "top",
            prop: "isSealed",
            type: "switch",
            value: 0,
            slot: true,
            dicData: [{
                    label: "否",
                    value: 0
                },
                {
                    label: "是",
                    value: 1
                }
            ],
            rules: [{
                required: true,
                message: "请选择封存",
                trigger: "blur"
            }]
        },
        {
            label: "字典备注",
            maxlength: 30,
            showWordLimit: false,
            prop: "remark",
            labelPosition: "top",
            hide: true
        }
    ]
};

export const optionChild = {
    height: 'auto',
    calcHeight: 95,
    searchShow: true,
    searchMenuSpan: 6,
    searchMenuPosition: "left",
    menuWidth: 150,
    tip: false,
    tree: true,
    border: false,
    index: true,
    indexLabel: '序号',
    selection: true,
    viewBtn: true,
    refreshBtn: false,
    columnBtn: false,
    dialogClickModal: false,
    column: [{
            label: "字典编号",
            maxlength: 30,
            showWordLimit: false,
            labelPosition: "top",
            search: true,
            searchSpan: 8,
            prop: "code",
            width: 120,
            overHidden: true,
            addDisabled: true,
            editDisabled: true,
            rules: [{
                required: true,
                message: "请输入字典编号",
                trigger: "blur"
            }]
        },
        {
            label: "字典名称",
            maxlength: 30,
            showWordLimit: false,
            labelPosition: "top",
            prop: "dictValue",
            search: true,
            searchSpan: 8,
            rules: [{
                required: true,
                message: "请输入字典名称",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            addDisabled: true,
            editDisabled: true,
            label: "上级字典",
            labelPosition: "top",
            prop: "parentId",
            type: "tree",
            dicData: [],
            props: {
                label: "title"
            },
            rules: [{
                required: false,
                message: "请选择上级字典",
                trigger: "click"
            }]
        },
        {
            label: "字典键值",
            maxlength: 30,
            showWordLimit: false,
            labelPosition: "top",
            prop: "dictKey",
            rules: [{
                required: true,
                message: "请输入字典键值",
                trigger: "blur"
            }]
        },
        {
            label: "字典排序",
            labelPosition: "top",
            prop: "sort",
            type: "number",
            rules: [{
                required: true,
                message: "请输入字典排序",
                trigger: "blur"
            }]
        },
        {
            label: "封存",
            labelPosition: "top",
            prop: "isSealed",
            type: "switch",
            value: 0,
            slot: true,
            dicData: [{
                    label: "否",
                    value: 0
                },
                {
                    label: "是",
                    value: 1
                }
            ],
            rules: [{
                required: true,
                message: "请选择封存",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "字典备注",
            prop: "remark",
            labelPosition: "top",
        }
    ]
};