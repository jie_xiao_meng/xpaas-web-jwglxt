export const optionParent = {
    height: 'auto',
    calcHeight: 95,
    tip: false,
    searchShow: true,
    searchMenuSpan: 6,
    searchLabelWidth: 80,
    searchMenuPosition: "left",
    menuWidth: 150,
    tree: true,
    border: false,
    selection: true,
    index: true,
    indexLabel: '序号',
    viewBtn: true,
    refreshBtn: false,
    columnBtn: false,
    dialogClickModal: false,
    column: [{
            label: "字典编号",
            labelPosition: "top",
            maxlength: 30,
            showWordLimit: false,
            prop: "code",
            search: true,
            searchSpan: 8,
            width: 120,
            overHidden: true,
            rules: [{
                required: true,
                message: "请输入字典编号",
                trigger: "blur"
            }]
        },
        {
            label: "字典名称",
            labelPosition: "top",
            maxlength: 30,
            showWordLimit: false,
            prop: "dictValue",
            searchSpan: 8,
            search: true,
            rules: [{
                required: true,
                message: "请输入字典名称",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "字典排序",
            labelPosition: "top",
            prop: "sort",
            type: "number",
            rules: [{
                required: true,
                message: "请输入字典排序",
                trigger: "blur"
            }]
        },
        {
            label: "封存",
            labelPosition: "top",
            prop: "isSealed",
            type: "switch",
            value: 0,
            slot: true,
            dicData: [{
                    label: "否",
                    value: 0
                },
                {
                    label: "是",
                    value: 1
                }
            ],
            rules: [{
                required: true,
                message: "请选择封存",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "字典备注",
            labelPosition: "top",
            prop: "remark",
        }
    ]
};

export const optionChild = {
    height: 'auto',
    calcHeight: 95,
    tip: false,
    searchShow: true,
    searchMenuSpan: 6,
    searchLabelWidth: 80,
    searchMenuPosition: "left",
    tree: true,
    border: false,
    index: true,
    indexLabel: '序号',
    selection: true,
    viewBtn: true,
    menuWidth: 150,
    dialogClickModal: false,
    refreshBtn: false,
    columnBtn: false,
    column: [{
            label: "字典编号",
            maxlength: 20,
            prop: "code",
            labelPosition: "top",
            addDisabled: true,
            editDisabled: true,
            search: true,
            maxlength: 30,
            showWordLimit: false,
            width: 120,
            overHidden: true,
            rules: [{
                required: true,
                message: "请输入字典编号",
                trigger: "blur"
            }]
        },
        {
            label: "字典名称",
            maxlength: 30,
            showWordLimit: false,
            prop: "dictValue",
            labelPosition: "top",
            search: true,
            searchSpan: 8,
            align: "center",
            rules: [{
                required: true,
                message: "请输入字典名称",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "上级字典",
            prop: "parentId",
            labelPosition: "top",
            type: "tree",
            dicData: [],
            props: {
                label: "title"
            },
            addDisabled: true,
            editDisabled: true,
            rules: [{
                required: false,
                message: "请选择上级字典",
                trigger: "click"
            }]
        },
        {
            label: "字典键值",
            maxlength: 30,
            showWordLimit: false,
            prop: "dictKey",
            labelPosition: "top",
            rules: [{
                required: true,
                message: "请输入字典键值",
                trigger: "blur"
            }]
        },
        {
            label: "字典排序",
            labelPosition: "top",
            prop: "sort",
            type: "number",
            rules: [{
                required: true,
                message: "请输入字典排序",
                trigger: "blur"
            }]
        },
        {
            label: "封存",
            labelPosition: "top",
            prop: "isSealed",
            type: "switch",
            value: 0,
            slot: true,
            dicData: [{
                    label: "否",
                    value: 0
                },
                {
                    label: "是",
                    value: 1
                }
            ],
            rules: [{
                required: true,
                message: "请选择封存",
                trigger: "blur"
            }]
        },
        {
            hide: true,
            label: "字典备注",
            labelPosition: "top",
            prop: "remark",
        }
    ]
};