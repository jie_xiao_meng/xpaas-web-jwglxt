import request from '@/router/axios'

// 评教管理：课堂教学质量评价汇总：数据汇总查询
export const querySummaryList = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/queryevluateshujuhuizong',
    method: 'post',
    data: data,
    params: { current: data.current, size: data.size }
  })
}
// 评教管理：课堂教学质量评价汇总：发布
export const batchRelease = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/plupdate',
    method: 'post',
    data: data
  })
}
// 评教管理：课堂教学质量评价汇总：综合评教结果查询
export const queryResultList = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/queryevluatedudaozhpjselect',
    method: 'post',
    data: data,
    params: { current: data.current, size: data.size }
  })
}
// 评教管理：专项督导信息：专项督导信息列表
export const querySpecialList = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/queryzxddxinfo',
    method: 'post',
    data: data,
    params: { current: data.current, size: data.size }
  })
}
// 评教管理：专项督导信息：获取所有督导专家
export const queryAllExpert = () => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/queryxidudaozhuanjia',
    method: 'post'
  })
}
// 评教管理：专项督导信息：详情
export const querySpecialDetail = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/detail',
    method: 'post',
    data: data
  })
}
// 评教管理：专项督导信息：新增专项督导信息
export const insertSpecialInfo = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/save',
    method: 'post',
    data: data
  })
}
// 评教管理：专项督导信息：修改专项督导信息
export const updateSpecialInfo = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/update',
    method: 'post',
    data: data
  })
}
// 评教管理：专项督导信息：批量通过
export const batchPass = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/plupdate',
    method: 'post',
    data: data
  })
}
// 生成评教汇总数据
export const createSummaryData = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/starszhzshuju',
    method: 'post',
    data: data
  })
}
// 专家评教结果提交
export const updateEvaluationResult = (data) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/queryevluatedudaoendup',
    method: 'post',
    data: data
  })
}
