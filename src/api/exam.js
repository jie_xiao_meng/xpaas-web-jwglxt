import request from '@/router/axios';
let requestType = { headers: { 'content-type': 'application/x-www-form-urlencoded' } };

import store from '../store'
let baseUrl = '/api/xpaas-student'

// 公共考试信息管理列表
export const examBatchList = (params) => {
    return request({
      url: baseUrl+'/examBatch/list',
      method: 'get',
      params:params
    }).then(res=>res.data)
  }
  // 公共考试成绩查询
export const commonList = (params) => {
  return request({
    url: baseUrl+'/examBatch/manageList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 考试批次详情
export const examBatchDetail = (params) => {
    return request({
      url: baseUrl+'/examBatch/detail',
      method: 'get',
      params:params
    }).then(res=>res.data)
  }
//新增/修改考试批次
export const addBatch = (params) => {
    return request({
      url: baseUrl+'/examBatch/save',
      method: 'post',
      data:params
    }).then(res=>res.data)
  }
//删除考试批次
export const deleteExamBatch = (params) => {
    return request({
      url: baseUrl+'/examBatch/delete',
      method: 'delete',
      params:params,
      requestType
    }).then(res=>res.data)
  }
// 考试类型列表
export const examTypeList = (params) => {
  return request({
    url: baseUrl+'/examType/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//新增/修改考试类型
export const addExamType = (params) => {
    return request({
      url: baseUrl+'/examType/save',
      method: 'post',
      data:params
    }).then(res=>res.data)
  }
//删除考试类型
export const deleteExamType = (params) => {
  return request({
    url: baseUrl+'/examType/delete',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
// 考试类型详情
export const examTypeDetail = (params) => {
  return request({
    url: baseUrl+'/examType/detail',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 根据成绩ID删除成绩
export const delExamScoreById = (params) => {
  return request({
    url: baseUrl+'/examScore/delExamScoreById',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 根据成绩ID查询四六级成绩
export const CETList = (params) => {
  return request({
    url: baseUrl+'/examScore/getCETExamScoreById',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 根据成绩ID查询自定义专业考试成绩
export const cusList = (params) => {
  return request({
    url: baseUrl+'/examScore/getCusExamScoreById',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 查询成绩单
export const reportList = (params) => {
  return request({
    url: baseUrl+'/examScore/getExamScoreByBatchId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//手动录入自定义专业考试成绩
export const saveCusExam = (params) => {
  return request({
    url: baseUrl+'/examScore/saveCusExam',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 修改时根据成绩ID查询自定义专业考试成绩
export const cusDetail = (params) => {
  return request({
    url: baseUrl+'/examScore/getCusExamScoreById',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//修改自定义专业考试成绩
export const saveEditCusExam = (params) => {
  return request({
    url: baseUrl+'/examScore/updateCusExam',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//手动录入四六级考试成绩
export const saveCETExam = (params) => {
  return request({
    url: baseUrl+'/examScore/saveCETExam',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 修改时根据成绩ID查询四六级成绩
export const cetDetail = (params) => {
  return request({
    url: baseUrl+'/examScore/getCETExamScoreById',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//修改四六级考试成绩
export const saveEditCETExam = (params) => {
  return request({
    url: baseUrl+'/examScore/updateCETExam',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 学生成绩查询
export const studentList = (params) => {
  return request({
    url: baseUrl+'/examScore/getStudentScore',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 点击授权按钮查看当前可授权人
export const allowSQList = (params) => {
  return request({
    url: baseUrl+'/examBatch/getEmpowerList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//授权后的保存 考试批次授权
export const saveSQ = (params) => {
  return request({
    url: baseUrl+'/examBatch/empower',
    method: 'post',
    data:params
  }).then(res=>res.data)
}




