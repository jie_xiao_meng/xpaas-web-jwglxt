import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/msg-center-service/message/page',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/msg-center-service/message/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/msg-center-service/message/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/msg-center-service/message/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/msg-center-service/message/submit',
    method: 'post',
    data: row
  })
}

export const myNewsNum = () => {
  return request({
    url: '/api/msg-center-service/msg/myNewsNum',
    method: 'get',
  })
}
export const sendIsDone = (id) => {
  return request({
    url: '/api/msg-center-service/message/sendIsDone',
    method: 'get',
    params: {
      id
    }
  })
}
export const getMyMsg = (sort) => {
  return request({
    url: '/api/msg-center-service/message/getMyMsg',
    method: 'get',
    params: {
      sort
    }
  })
}

