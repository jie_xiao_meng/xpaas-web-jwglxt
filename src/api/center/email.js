import request from '@/router/axios';

export const getList = (current, size, params,data) => {
  return request({
    url: '/api/msg-center-service/email/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
export const getInboxList = (current, size, params) => {
  return request({
    url: '/api/msg-center-service/email/inbox',
    method: 'get',
    params: {

    }
  })
}
export const getDraftsList = (current, size, params) => {
  return request({
    url: '/api/msg-center-service/email/drafts',
    method: 'get',
    params: {

    }
  })
}
export const getTrashList = (current, size, params) => {
  return request({
    url: '/api/msg-center-service/email/trash',
    method: 'get',
    params: {

    }
  })
}
/*export const getDraftsList = (current, size, params) => {
  return request({
    url: '/api/msg-center-service/email/drafts',
    method: 'get',
    params: {

    }
  })
}*/

export const getDetail = (id) => {
  return request({
    url: '/api/msg-center-service/email/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/msg-center-service/email/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/msg-center-service/email/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/msg-center-service/email/submit',
    method: 'post',
    data: row
  })
}

