import request from '@/router/axios';
import row from "element-ui/packages/row/src/row";

//字典表所有数据
export const queryDictByCode = (codeType) => {
  return request({
    url: "/api/xpaas-plan-service/dictbiz/selectdgplace",
    method: "post",
    params: {
      code: codeType
    }
  })
}

// 获取当前年份前五年和后五年
export const getYearIndex = () => {
  return request({
    url: '/api/xpaas-plan-service/systerm/getYear',
    method: 'get'
  })
}

//课程考核成绩查询
// export const queryAllscore = () => {
//   return request({
//     url: '/api/xpaas-course-service/courseimplementplan/queryAllKcState2',
//     method: 'post',
//   })
// }

//按条件查询
export const queryscore = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/tjQueryAllKcState2',
    method: 'post',
    data: row
  })
}

export const reqQuerycjplanid = planid => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/tjQueryyshcjxgbyid",
    method: "get",
    params: { planid }
  });
};
//查询全部待审核学生成绩
export const querystudentcj2 = (planid) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/querystudentcj2',
    method: 'post',
    params: { planid }
  })
}

//查询全部待审核学生成绩(管理员已审核成绩修改)
export const queryallscore = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/Tjkcqueryallscoreysh',
    method: 'post',
    data: obj
  })
}
//课程考核成绩数据(教研室主任)
export const queryallscorej = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/TjkcqueryallscoreJYSZR',
    method: 'post',
    data: obj
  })
}
// 根据planid查询学生成绩
export const querycjstudnetid = (stuid) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/querycjstudnetid',
    method: 'get',
    params: { stuid }
  })
}

//已审核成绩修改跳转页面的查询
export const querycjplanid = (id) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/querycjplanid',
    method: 'get',
    params: { id }
  })
}
//已审核成绩修改跳转页面的修改保存
export const updatewinster = (row) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/updateStudnetnewinster',
    method: 'post',
    data: row,
  })
}

//课程考核成绩查询
export const kcqueryallscore = () => {
  return request({
    url: '/api/xpaas-course-service/scorequery/kcqueryallscore',
    method: 'get',
  })
}

//按条件课程考核成绩查询(管理员成绩管理)
export const Tjkcqueryallscore = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/Tjkcqueryallscore',
    method: 'post',
    data: obj
  })
}

//下边数据
export const scorecj2 = (planid) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/querystudentcj2',
    method: 'get',
    params: { planid }
  })
}

//上边数据
export const scorecj1 = (planid) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryKcState2Iet',
    method: 'get',
    params: { planid }
  })
}


// 审核通过与不通过
export const updateCjSh = (row) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/updateStudentCjSh',
    method: 'post',
    data: row
  })
}
//查询全部已修改数据
export const queryallstudnetlog = () => {
  return request({
    url: '/api/xpaas-course-service/updatescorelog/queryallstudnetlog',
    method: 'get',
  })
}
//恢复初始值
export const RecoverByname = (paramcode) => {
  return request({
    url: '/api/xpaas-course-service/electiveconfig/RecoverByname',
    method: 'get',
    params: { paramcode }
  })
}
//恢复初始值
export const updateele = (row) => {
  return request({
    url: '/api/xpaas-course-service/electiveconfig/updateele',
    method: 'post',
    data: row
  })
}
//查询成绩参数设置
export const elequeryall = () => {
  return request({
    url: '/api/xpaas-course-service/electiveconfig/queryall',
    method: 'get',
  })
}
//按时间查询
export const alquerytime = (pageNo,pageSize) => {
  return request({
    url: '/api/xpaas-course-service/updatescorelog/queryallstudnetlogByTime',
    method: 'get',
    params:{ pageNo,pageSize }
  })
}
//按学号查询
export const blqueryid = (pageNo,pageSize) => {
  return request({
    url: '/api/xpaas-course-service/updatescorelog/queryallstudnetlogByStudentid',
    method: 'get',
    params:{ pageNo,pageSize }
  })
}
//按课程编号查询
export const clqueryscorenum = (pageNo,pageSize) => {
  return request({
    url: '/api/xpaas-course-service//updatescorelog/QueryallByscorenum',
    method: 'get',
    params:{ pageNo,pageSize }
  })
}
//按起始截止查询
export const dlqueryscorenum = (qizhitime, jiezhitime,pageNo,pageSize) => {
  return request({
    url: '/api/xpaas-course-service/updatescorelog/queryallstudnetlogTime',
    method: 'get',
    params: { qizhitime, jiezhitime,pageNo,pageSize }

  })
}
//课程考核成绩(系领导)
export const queryallscorex = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/TjkcqueryallscoreXLD',
    method: 'post',
    data: obj
  })
}
//课程考核成绩查询(系领导)
export const Tjkcqueryallscorex = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/TjkcqueryallscoreXLD',
    method: 'post',
    data: obj
  })
}
//课程考核成绩(教科处领导)
export const queryallscorejk = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/Tjkcqueryallscore',
    method: 'post',
    data: obj
  })
}
//课程考核成绩查询(教科处领导)
export const Tjkcqueryallscorejk = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/Tjkcquerystudentcjd',
    method: 'post',
    data: obj
  })
}
export const down = (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/updatestudneitnew',
    method: 'post',
    data: obj
  })
}

//学期院历管理----查询所有学期院历  /systerm/queryAll
export const reqCheckAllScore = (current, size) => {
  return request({
    url: "/api/xpaas-plan-service/systerm/queryAll",
    method: "get",
    params: {
      current,
      size
    }
  });
};
// 学期院历管理----新增新学期 /systerm/save
export const reqAddNewXq = data => {
  return request({
    url: "/api/xpaas-plan-service/systerm/save",
    method: "post",
    data
  });
};
// 学期院历管理----编辑 /systerm/selectByIdOne
export const reqEditXq = id => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectByIdOne",
    method: "get",
    params: { id }
  });
};
// 学期院历管理----修改 /systerm/update
export const reqUpdateXq = data => {
  return request({
    url: "/api/xpaas-plan-service/systerm/update",
    method: "post",
    data
  });
};
// 学期院历管理----删除 /systerm/remove
export const reqDeleteXq = ids => {
  return request({
    url: "/api/xpaas-plan-service/systerm/remove",
    method: "post",
    params: {
      ids
    }
  });
};
// 学期院历管理----查看学期院历  /systerm/selectById
export const reqGetCalendarList = id => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectById",
    method: "get",
    params: {
      id
    }
  });
};
// 学期院历管理----查看学期院历----学期院历时间列表  xpaas-home-service/term/getCurrentTermAll
export const reqGetXqList = () => {
  return request({
    url: "/api/xpaas-home-service/term/getCurrentTermAll",
    method: "get"
  });
};
// 学期院历管理----查看学期院历----查询/term/getCurrentTermStudyNian
export const reqcheckCalendarList = term => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectByTerm",
    method: "get",
    params: { term }
  });
};
// 学期院历管理-----查看学期院历----保存事件说明   /systermremarks/submit
export const reqSaveRemarks = data => {
  return request({
    url: "/api/xpaas-plan-service/systermremarks/submit",
    method: "post",
    data
  });
};
// 通过学期名称查 是否有重复学期名称
export const selectByXqmc = term => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectByXqmc",
    method: "get",
    params: { term }
  });
};

// 根据学期查询可选月份
export const toSelectDateByTerm = term => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectDateByTerm",
    method: "get",
    params: { term }
  });
};

// 培养方案执行 ----年级接口  /xpaas-system/dict-biz/list
export const reqCheckYearList = () => {
  return request({
    url: "/api/xpaas-home-service/term/getCurrentTermStudyNian",
    method: "get"
  });
};
// 培养方案执行 ----层次接口  /xpaas-system/dict-biz/list
export const reqCheckLevelList = code => {
  return request({
    url: "/api/xpaas-system/dict-biz/list?code=pylevel",
    method: "get",
    /* params: {
      code
    } */
  });
};
// 培养方案执行 ----查询  /yearspecialtyinfo/pyfazx
export const reqCheckPlanList = (cc, current,nj, size) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/pyfazx",
    method: "get",
    params: {
      cc,
      current,
      nj,
      size
    }
  });
};
// 培养方案执行 ----点击详情----初始化数据   /testtrainingprograminfo/setCourse
export const reqDetailPlanList = id => {
  return request({
    url: "/api/xpaas-plan-service/testtrainingprograminfo/setCourse",
    method: "get",
    params: {
      id
    }
  });
};

//查询指时间是否存在学期
export const selectTermByTime = (statrtime,endtime,id) => {
  return request({
    url: "/api/xpaas-plan-service/systerm/selectTermByTime",
    method: "get",
    params: {
      statrtime,
      endtime,
      id
    }
  });
};

// 培养方案执行 ----点击引用  /yearspecialtyinfo/selectById
export const reqUsePlan = id => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/selectById",
    method: "get",
    params: {
      id
    }
  });
};
// 培养方案执行 ----培养方案（版本)  /yearspecialtyinfo/selectByIdTrainings
export const reqPlanVersions = id => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/selectByIdTrainings",
    method: "get",
    params: {
      id
    }
  });
};
// 培养方案执行 ----单个/批量引用培养方案  /yearspecialtyinfo/updateList
export const reqUpdatePlan = data => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/updateList",
    method: "post",
    data
  });
};
//培养方案管理 ---修改页面内浏览文件
export const lineWord = (id,filePaths) => {
  return request({
    url: "/api/xpaas-plan-service/testtrainingprograminfo/lineWord",
    method: "get",
    params: {
      id,
      filePaths
    },
    responseType: "blob",
  });
};

//人才培养方案查询 ---- 队长
export const trainSearch = (current,size,row) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/selectByTeacherIdTrainings",
    method: "post",
    params:{current,size},
    data: row
  });
};

//人才培养方案查询 ---- 大队长
export const trainDSearch = (current,size,row) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/selectByDTeacherIdTrainings",
    method: "post",
    params:{current,size},
    data: row
  });
};
//人才培养方案查询 ---- 学员
export const trainStudentSearch = () => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/selectByStudentIdTrainings",
    method: "get"
  });
};
//人才培养方案查询 ---- 教科处领导
export const getConditionList = (pageNo, pageSize, year, pyLevel) => {
  return request({
      url: "/api/xpaas-plan-service/testtrainingprograminfo/getConditionList",
      method: "get",
      params: { pageNo, pageSize, year, pyLevel }
  });
};

// 学员队
export const reqXydList = () => {
  return request({
    url: "/api/xpaas-home-service/dept/queryxyd",
    method: "get"
  });
};
// 年级
export const reqNjList = () => {
  return request({
    url: "/api/xpaas-course-service/scorequery/querynj",
    method: "post"
  });
};
// 专业班次
export const reqZybcList = () => {
  return request({
    url: "/api/xpaas-plan-service/specialtyclassinfo/queryspecticy",
    method: "post"
  });
};
//一进页面显示所有的专业班次
export const reqZybcListAll = (current,size) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyclassinfo/queryzybc",
    method: "post",
    // data: data,
    params: { current, size }
  });
};

export const hpsetchock = ( pageNo,pageSize,termid,designcontent,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,state ) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/queryCoursesPlan",
    method: "get",
    params: {
      pageNo,pageSize,termid,designcontent,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,state
    }
  });
};

export const hpsetchockNew = ( pageNo,pageSize,termid,designcontent,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,detalestatus ) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/queryCoursesPlan",
    method: "get",
    params: {
      pageNo,pageSize,termid,designcontent,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,detalestatus
    }
  });
};

export const hpsetchockAdmin = ( pageNo,pageSize,termid,department,teachingandresearchsection,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,detalestatus ) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/queryCoursesPlan",
    method: "get",
    params: {
      pageNo,pageSize,termid,department,teachingandresearchsection,traininglevel,grade,professionaldivisions,studytype,coursename,teachingfaculty,detalestatus
    }
  });
};

export const hpStatus = data => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/auditStatus",
    method: "post",
    data
  });
};


export const querySemesterByYear = (year) => {
  return request({
    url: "/api/xpaas-home-service/term/geteambygrade",
    method: "get",
    params: {
      year: year
    }
  })
}
export const queryExamResultList = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/cjfxkckhcjfb",
    method: "post",
    data: data
  })
}
export const queryStatisticsList = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/querystudentcjtjsz",
    method: "post",
    data: data
  })
}
export const queryCourseRate = (planid) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/cjfxscorecoursefbxq",
    method: "get",
    params: {
      planid: planid
    }
  })
}
export const queryResultDetail = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/cjfxscorecoursefbxqlb",
    method: "post",
    data: data
  })
}
export const queryScoreRank = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/cjfxstudentorder",
    method: "post",
    data: data
  })
}
export const queryFailList = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/bjgpeoplequerystudent",
    method: "post",
    data: data
  })
}
export const queryElectiveList = (data, pageNo, pageSize) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/queryhlwxxcourselb",
    method: "post",
    data: data,
    params: { current: pageNo, size: pageSize }
  })
}
export const updateElectiveState = (data) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/updatetganddh",
    method: "post",
    data: data
  })
}
export const queryElectiveDetail = (id) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/queryxuanxiucoursebyid?id=" + id,
    method: "get"
  })
}
export const updateElectiveInfo = (data) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/update",
    method: "post",
    data: data
  })
}
export const uploadImage = (data) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/uploadFileCoursejpg",
    method: "post",
    data: data
  })
}
export const uploadFile = (data) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/uploadFileCourse",
    method: "post",
    data: data
  })
}
export const getFileStream = (id) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/linejpg?id=" + id,
    method: "get"
  })
}

export const downloadFile = (path) => {
  return request({
    url: "/api/xpaas-course-service/kchlwxxcourinfo/dowload",
    method: "get",
    params: { file: path },
    responseType: "blob",
  })
}
export const querySummarySetList = (data) => {
  return request({
    url: "/api/xpaas-evaluate-service/summarizesetup/select",
    method: "post",
    data: data
  })
}
export const updateSummaryInfo = (data) => {
  return request({
    url: "/api/xpaas-evaluate-service/summarizesetup/update",
    method: "post",
    data: data
  })
}
export const querryPlanDetail = (planid) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/queryCoursesPlanXq",
    method: "get",
    params: { planid: planid }
  })
}
