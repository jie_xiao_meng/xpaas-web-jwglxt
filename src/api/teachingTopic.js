import request from '@/router/axios';
let requestType = { headers: { 'content-type': 'application/x-www-form-urlencoded' } };

let baseUrl = '/api/xpaas-student'

// 课题类别
export const topicList = (params) => {
  return request({
    url: baseUrl+'/subjectCategory/getSubjectCategoryList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题类别删除
export const deleteTopic = (params) => {
  return request({
    url: baseUrl+'/subjectCategory/delNewSubjectCategory',
    method: 'get',
    params:params,
  }).then(res=>res.data)
}
//课题类别 新增
export const addType = (params) => {
  return request({
    url: baseUrl+'/subjectCategory/addNewSubjectCategory',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 课题流程管理-查询
export const lcList = (params) => {
    return request({
      url: baseUrl+'/subjectCategoryStageFlow/queryFlowByStageId',
      method: 'get',
      params:params
    }).then(res=>res.data)
  }
//课题流程管理-保存
export const lcSave = (params) => {
  return request({
    url: baseUrl+'/subjectCategoryStageFlow/saveOrUpdateFlow',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 课题材料管理-查询
export const clList = (params) => {
  return request({
    url: '/api/xpaas-student/subjectCategoryStageResource/queryResourceByStageId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题材料管理-查询new
export const queryclList = (params) => {
  return request({
    url: '/api/xpaas-student/subjectCategoryStageResource/querycailiaoxianshi',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题材料管理-保存
export const clSave = (params) => {
  return request({
    url: baseUrl+'/subjectCategoryStageResource/saveOrUpdateResource',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//课题类别-阶段
export const jdList = (params) => {
  return request({
    url: baseUrl+'/subjectCategoryStage/queryStageBySubCategoryId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题类别-阶段 保存
export const jdSave = (params) => {
  return request({
    url: baseUrl+'/subjectCategoryStage/saveOrUpdateStage',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//课题申报-列表页查询 （蓝湖第一张图）
export const sbList = (params) => {
  return request({
    url: baseUrl+'/fillSubject/getSubjectFillList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题申报-列表页查询的提交 （蓝湖第一张图）
export const sbSubmit = (params) => {
  return request({
    url: baseUrl+'/fillSubject/commitSubjectFill',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//获得所有申报课题活动
export const allActivityList = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/getAllSubjectActivity',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//选择申报课题活动后查阶段材料
export const sbJDList = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/getAcStageBySubActivityId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题申报修改-查询
export const sbDetail = (params) => {
  return request({
    url: baseUrl+'/fillSubject/getSingleSubjectFill',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//课题申报-申报里的保存
export const sbSave = (params) => {
  return request({
    url: baseUrl+'/fillSubject/subjectFill',
    method: 'post',
    data:params
  }).then(res=>res.data)
}

// 创建新的课题活动列表
export const getActivityList = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/getSubjectActivity',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//获得所有活动所属类别
export const getallTypeList = (params) => {
  return request({
    url: baseUrl+'/subjectCategory/getSubjectCategoryListNoPage',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 根据类别查询所有相关课题活动
export const getTypeActivityList = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/getSubjectActivityByCate',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 根据课题活动阶段查询流程
export const getTypeActivityLC = (params) => {
  return request({
    url: baseUrl+'/subjectAcStageFlow/getAcFlowBySubActivityId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题活动列表 --新增
export const addActivity = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/addNewSubjectActivity',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 创建新的课题活动 下发课题活动/关闭课题活动
export const xfORClose = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/switchSubjectActivity',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//课题活动列表 --设置
export const setting = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/querySubjectActivityResource',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题活动列表 --设置保存
export const settingSave = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/setSubjectActivityResource',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//查询所有课题信息
export const allKTInfo = (params) => {
  return request({
    url: baseUrl+'/fillSubject/getAllSubjectFill',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题信息统计-查询
export const ktxxtjCX = (params) => {
  return request({
    url: baseUrl+'/fillSubject/getSpecialSubjectFill',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题信息统计-编辑课题编号
export const updateNum = (params) => {
  return request({
    url: baseUrl+'/fillSubject/updateSubjectId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题信息统计-变更-查询填报信息
export const changeSearch = (params) => {
  return request({
    url: baseUrl+'/fillSubject/getSingleFillSubject',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 课题信息统计-变更-所有基本信息都要回传回来
export const changeSave = (params) => {
  return request({
    url: baseUrl+'/fillSubject/updateSingleFillSubject',
    method: 'post',
    data:params
  }).then(res=>res.data)
}


//课题信息统计-不通过
export const ktNoPass = (params) => {
  return request({
    url: baseUrl+'/fillSubject/rejectSubjectId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题信息统计-打回
export const repulse = (params) => {
  return request({
    url: baseUrl+'/fillSubject/reUpdateSubjectId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//课题信息统计-确认并进入下一步
export const confirmNextStep = (params) => {
  return request({
    url: baseUrl+'/fillSubject/nextFlow',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

// 评审指标体系
export const pstxList = (params) => {
  return request({
    url: baseUrl+'/reviewSystem/page',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//新增评审指标体系
export const pszbtxSave = (params) => {
  return request({
    url: baseUrl+'/reviewSystem/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//评审指标体系 重命名
export const resizeSave = (params) => {
  return request({
    url: baseUrl+'/reviewSystem/update',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//新增指标项
export const zbxSave = (params) => {
  return request({
    url: baseUrl+'/reviewItem/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//编辑指标项
export const updateZbSave = (params) => {
  return request({
    url: baseUrl+'/reviewItem/update',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//删除 指标项
export const deleteZBX = (params) => {
  return request({
    url: baseUrl+'/reviewItem/delete',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
//评审等级权重管理 根据指标项id获取等级说明
export const psDJList = (params) => {
  return request({
    url: baseUrl+'/reviewGradeRemarks/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//编辑 评审指标项等级说明
export const updatePsDJ = (params) => {
  return request({
    url: baseUrl+'/reviewGradeRemarks/update',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//查询评审活动列表
export const psActivityList = (params) => {
  return request({
    url: baseUrl+'/reviewActivity/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//新增评审活动
export const addPSActivity = (params) => {
  return request({
    url: baseUrl+'/reviewActivity/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
  //编辑评审活动
export const updatePSActivity = (params) => {
  return request({
    url: baseUrl+'/reviewActivity/update',
    method: 'post',
    data:params
  }).then(res=>res.data)
  
}
//已有的课题活动中选择
export const choseList = (params) => {
  return request({
    url: baseUrl+'/subjectActivity/getSubjectActivityByPage',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//专家端根据课题活动查询评审活动列表
export const pshdList = (params) => {
  return request({
    url: baseUrl+'/reviewActivity/getListByExpert',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//专家端根据课题活动查询评审活动列表 
export const pshdZJList = (params) => {
  return request({
    url: baseUrl+'/reviewActivity/getFillSubjectListByActivity',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 开始评审
export const startPS = (params) => {
  return request({
    url: baseUrl+'/reviewResult/getReviewSystemDiy',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//保存评审分数
export const savePSscore = (params) => {
  return request({
    url: baseUrl+'/reviewResult/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//提交评审分数和意见
export const submitPSscore = (params) => {
  return request({
    url: baseUrl+'/reviewResult/submit',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//评审结果统计分析
export const resultPS = (params) => {
  return request({
    url: baseUrl+'/reviewResult/getReviewResultGather',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//专家评审详情
export const resultPSDetail = (params) => {
  return request({
    url: baseUrl+'/reviewResult/getReviewResultInfo',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//发布
export const release = (params) => {
  return request({
    url: baseUrl+'/reviewResult/publish',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 创建评审活动----点击编辑单个评审活动
export const getOneActivity = (activityId) => {
  return request({
    url: baseUrl+'/reviewActivity/getOneActivity',
    method: 'get',
    params:{
      activityId:activityId
    }
  }).then(res=>res.data)
}









