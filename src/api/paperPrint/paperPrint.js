import request from '@/router/axios';


export const getSelectConditionPrint = (row) => {
  return request({
    url: '/api/xpaas-course-service/testpaperandprint/selectConditionPrint',
    method: 'post',
    data: row
  })
}
export const getLine = (id,position) => {
  return request({
    url: '/api/xpaas-course-service/materialDownload/line',
    method: 'get',
    params: {
      id,
      position
    }
  })
}
export const getDownload = (row) => {
  return request({
    url: '/api/xpaas-course-service/materialDownload/download',
    method: 'post',
    data: row
  })
}
