import request from "@/router/axios";

let requestType = {
  headers: { "content-type": "application/x-www-form-urlencoded" },
};
let baseUrl = "/api/xpaas-sx";

// ======================= 公共接口 =====================

// 获取排课状态
export const queryCommonGetPkzt = () => {
  return request({
    url: baseUrl + "/common/getPkzt",
    method: "get",
  });
};

//获取审核状态
export const queryCommonShzt = () => {
  return request({
    url: baseUrl + "/common/shzt",
    method: "get",
  });
};

//获取开课状态
export const queryCommonkkzt = () => {
  return request({
    url: baseUrl + "/common/kkzt",
    method: "get",
  });
};

//获取课程列表
export const queryCommonCoursList = () => {
  return request({
    url: baseUrl + "/common/course",
    method: "get",
  });
};

//获取课程类型
export const queryCommonCoursetypee = () => {
  return request({
    url: baseUrl + "/coursetype/querycoursetype",
    method: "get",
  });
};

//获取修习类型
export const queryCommonXxType = () => {
  return request({
    url: baseUrl + "/common/xxType",
    method: "get",
  });
};

//获取学科分类
export const queryCommonSubjectType = () => {
  return request({
    url: baseUrl + "/common/subjectType",
    method: "get",
  });
};

//获取部门
export const queryCommonNjNdzy = () => {
  return request({
    url: baseUrl + "/common/njNdzy",
    method: "get",
  });
};

//获取部门
export const queryCommonDept = () => {
  return request({
    url: baseUrl + "/common/dept",
    method: "get",
  });
};

//获取部门
export const queryCommonDeptByCategory = (pCategory) => {
  return request({
    url: baseUrl + "/common/deptByCategory",
    method: "get",
    params: { category: pCategory },
  });
};

// 获取培养层次
export const queryCommonPycc = () => {
  return request({
    url: baseUrl + "/common/pyccType",
    method: "get",
  });
};

// 获取培养层次
export const queryCommonPyccId = () => {
  return request({
    url: baseUrl + "/common/pyccIdType",
    method: "get",
  });
};

// 获取学期
export const queryCommonXueqi = () => {
  return request({
    url: baseUrl + "/common/xueqi",
    method: "get",
  });
};

// 获取队别
export const queryCommonDB = (xueqiId) => {
  return request({
    url: baseUrl + "/common/db",
    method: "get",
  });
};

// 获取年级
export const queryCommonNj = (xueqiId) => {
  return request({
    url: baseUrl + "/common/nj?xueqiId=" + xueqiId,
    method: "get",
  });
};

// 获取教研室
export const queryCommonJys = () => {
  return request({
    url: baseUrl + "/common/getJys",
    method: "get",
  });
};

// 获取校区
export const queryCommonXq = () => {
  return request({
    url: baseUrl + "/common/getXq",
    method: "get",
  });
};

// 获取学期计划执行状态
export const queryCommonXueqiJhStatus = () => {
  return request({
    url: baseUrl + "/common/xueqiJhStatus",
    method: "get",
  });
};

// 获取场地场所
export const queryCommonCdcsType = () => {
  return request({
    url: baseUrl + "/common/cdcsType",
    method: "get",
  });
};

// 获取分制
export const queryCommonFzType = () => {
  return request({
    url: baseUrl + "/common/fzType",
    method: "get",
  });
};

// 考核方式
export const queryCommonKhfsType = () => {
  return request({
    url: baseUrl + "/common/khfsType",
    method: "get",
  });
};

// 授课方式
export const queryCommonSkfsType = () => {
  return request({
    url: baseUrl + "/common/skfsType",
    method: "get",
  });
};

// 根据部门获取教师
export const queryCommonTeacherInfo = (GZBM) => {
  return request({
    url: baseUrl + "/teacherInfo/list?GZBM=" + GZBM,
    method: "get",
  });
};

//教员课表查询
export const queryTimeTable = (name, type) => {
  return request({
    url: baseUrl + "/kcb/js?name=" + name + "&type=" + type,
    method: "get",
  });
};
// 教员课表导出
export const queryTimeTable_Export = (name, type) => {
  return request({
    url: baseUrl + "/kcb/exportJS?name=" + name + "&type=" + type,
    method: "get",
    responseType: "blob",
  });
};

//专业班次课表查询
export const queryProfessionalTimeTable = (name) => {
  return request({
    url: baseUrl + "/class/zy?name=" + name,
    method: "get",
  });
};
// 场地课表查询 一室一表
export const queryPlaceTimeTable = (name, type) => {
  return request({
    url: baseUrl + "/kcb/cd?name=" + name + "&type=" + type,
    method: "get",
  });
};

// 场地课表查询 一室一表 导出
export const queryPlaceTimeTable_ExportCD = (name, type) => {
  return request({
    url: baseUrl + "/kcb/exportCD?name=" + name + "&type=" + type,
    method: "get",
    responseType: "blob",
  });
};

// 场地课表查询 多室一表
export const queryPlaceTimeTable_More = (data) => {
  return request({
    url: baseUrl + "/kcb/cdDsyb",
    method: "post",
    data,
  });
};

// 场地课表查询 多室一表
export const queryPlaceTimeTable_More_Export = (data) => {
  return request({
    url: baseUrl + "/kcb/exportCdDsyb",
    method: "post",
    data,
    responseType: "blob",
  });
};

// 场地选择 type=1按建筑物，type:2 部门, type: 3 类型
export const queryPlaceByTypeAndName = (type, name) => {
  return request({
    url: baseUrl + "/placeinfo/placeInfoTree?type=" + type + "&name=" + name,
    method: "get",
  });
};

// 根据建筑物及名称查询场地列表
export const queryPlaceListByName = (
  cdmc,
  cdlx,
  ssxq,
  deptName,
  jzwName,
  floorName
) => {
  return request({
    url:
      baseUrl +
      "/placeinfo/list?cdmc=" +
      cdmc +
      "&cdlx=" +
      cdlx +
      "&ssxq=" +
      ssxq +
      "&deptName=" +
      deptName +
      "&jzwName=" +
      jzwName +
      "&floorName=" +
      floorName,
    method: "get",
  });
};

// ================================= 1. 制定专业班次学期执行计划  ===================================================

export const queryZdzybcxqzxjhList = (xq, nj, pycc, zt, current, size) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/list?xq=" +
      xq +
      "&nj=" +
      nj +
      "&pycc=" +
      pycc +
      "&zt=" +
      zt +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 确认
export const zdzybc_submit_Course = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/queren",
    method: "post",
    data,
  });
};

// 撤销
export const zdzybc_chexiao_Course = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/chexiao",
    method: "post",
    data,
  });
};

// 同步
export const zdzybc_tongbu_Course = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/tongbu",
    method: "post",
    data,
  });
};

//主页导出
export const queryZdzybcxqzxjhList_export = (xq, nj, pycc, zt) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/exportIndex?xq=" +
      xq +
      "&zt=" +
      zt +
      "&nj=" +
      nj +
      "&pycc=" +
      pycc,
    method: "get",
    responseType: "blob",
  });
};
// 导出任务书
export const queryZdzybcxqzxjhRws_export = (xq, nj, pycc, zt, ids) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/exportRws?xq=" +
      xq +
      "&zt=" +
      zt +
      "&nj=" +
      nj +
      "&pycc=" +
      pycc +
      "&ids=" +
      ids,
    method: "get",
    responseType: "blob",
  });
};

// 专业班次同步
export const zdzybc_tongbu_Course_zybc = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/tongbuFromZybc",
    method: "post",
    data,
  });
};

// 批量从培养方案导入-查询培养计划
export const querypyfaByNj = (data, current, size) => {
  return request({
    url:
      baseUrl + "/zdzybcxqzxjh/pyfaByNj?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};

// 批量从培养方案导入-查询培养课程
export const queryPykcByFaId = (data, current, size) => {
  return request({
    url:
      baseUrl + "/zdzybcxqzxjh/pykcByFaId?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};

//批量从培养方案导入-导入
export const addFromPykcByNdzyId = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/addFromPykcByNdzyId",
    method: "post",
    data,
  });
};

// 批量从培养方案导入 - 通过专业班次id获取专业班次课程
export const queryZybcCourseByZybcId = (zybcId, xueqi, current, size) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/zybcCourseByZybcId?zybcId=" +
      zybcId +
      "&xueqi=" +
      xueqi +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 查询详细信息
export const specialtyclassinfo_detail = (id) => {
  return request({
    url: baseUrl + "/specialtyclassinfo/detail?id=" + id,
    method: "get",
  });
};

// 批量从培养方案导入 - 更新课程
export const updateZxjhCourse = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/updateZxjhCourse",
    method: "post",
    data,
  });
};

// 批量从培养方案导入 - 更新课程
export const deleteZxjhCourse = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/deleteZxjhCourse",
    method: "post",
    data,
  });
};

// 获取部系校验所
export const getXbJys = () => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/getXbJys",
    method: "get",
  });
};

// 批量从培养方案导入 - 获取课程
export const getCourse = (
  teachingClassName,
  bx,
  courseName,
  courseCode,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/getCourse?teachingClassName=" +
      teachingClassName +
      "&bx=" +
      bx +
      "&courseName=" +
      courseName +
      "&courseCode=" +
      courseCode +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 添加课程
export const addZxjhCourse = (data) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/addZxjhCourse",
    method: "post",
    data,
  });
};

//批量从培养方案导入-导入
export const addFromPykcByZybcId = (pykcId, zybcId) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/addFromPykcByZybcId?pykcId=" +
      pykcId +
      "&zybcId=" +
      zybcId,
    method: "post",
  });
};

export const getYwbczxjh = (zybcId) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/getYwbczxjh?zybcId=" + zybcId,
    method: "get",
  });
};

//导出以往培养计划
export const exportPyfa = (zybcId) => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/exportPyfa?zybcId=" + zybcId,
    method: "get",
    responseType: "blob",
  });
};

// ================================= 2. 教学班管理  ===================================================

// 教学班管理-主页
export const jxbgl_query_class_list = (xueqi, current, size) => {
  return request({
    url:
      baseUrl +
      "/class/index?xueqi=" +
      xueqi +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};
// 查询表头数据
export const jxbgl_query_class_top = (xueqi) => {
  return request({
    url: baseUrl + "/class/indexTop?xueqi=" + xueqi,
    method: "get",
  });
};

// 教学班管理-点击教研室
export const jxbgl_query_class_jyskc = (
  jysId,
  xiId,
  xueqi,
  pycc,
  nj,
  fzdw,
  state,
  kcbh,
  kcmc
) => {
  return request({
    url:
      baseUrl +
      "/class/jyskc?jysId=" +
      jysId +
      "&xueqi=" +
      xueqi +
      "&xiId=" +
      xiId +
      "&pycc=" +
      pycc +
      "&fzdw=" +
      fzdw +
      "&nj=" +
      nj +
      "&kcbh=" +
      kcbh +
      "&kcmc=" +
      kcmc +
      "&state=" +
      state,
    // url: baseUrl+"/class/jyskc?xueqi" + xueqi + "&pycc=" + pycc + "&nj=" + nj + "&fzdw=" + fzdw + "&state=" + state + "&kcbh=" + kcbh + "&kcmc=" + kcmc,
    method: "get",
  });
};

// 按照课程生成教学班
export const jxbgl_submit_jxbByKc = (data) => {
  return request({
    url: baseUrl + "/class/jxbByKc",
    method: "post",
    data,
  });
};

// 按照专业生成教学班
export const jxbgl_submit_jxbByZybc = (data) => {
  return request({
    url: baseUrl + "/class/jxbByZybc",
    method: "post",
    data,
  });
};

// 获取课程详情
export const jxbgl_courseinfo_detail = (courseno) => {
  return request({
    url: baseUrl + "/courseinfo/detail?courseno=" + courseno,
    method: "post",
  });
};

// 教学班管理-设置-上半部分
export const jxbgl_query_szIndex = (kcid, nj, xueqi, kcbh) => {
  return request({
    url:
      baseUrl +
      "/class/szIndex?kcid=" +
      kcid +
      "&nj=" +
      nj +
      "&xueqi=" +
      xueqi +
      "&kcbh=" +
      kcbh,
    method: "get",
  });
};

// 教学班管理-设置-详情
export const jxbgl_query_teaching_class_detail = (id) => {
  return request({
    url: baseUrl + "/class/detail?id=" + id,
    method: "get",
  });
};

// 教学班管理-专业班次人员选择
export const jxbgl_query_student_list = (courseNo, nj, zybcId, xueqi) => {
  return request({
    url:
      baseUrl +
      "/class/xyByZybcIdNotInJxb?courseNo=" +
      courseNo +
      "&nj=" +
      nj +
      "&zybcId=" +
      zybcId +
      "&xueqi=" +
      xueqi,
    method: "get",
  });
};

// 教学班管理-合班后专业人次查看
export const jxbgl_query_xyByJxbId = (jxbId, zybcId) => {
  return request({
    url:
      baseUrl +
      "/class/xyByJxbIdAndZybcId?jxbId=" +
      jxbId +
      "&zybcId=" +
      zybcId,
    method: "get",
  });
};

// 教学班管理-删除教学班
export const jxbgl_query_remove = (data) => {
  return request({
    url: baseUrl + "/class/delete",
    method: "post",
    data,
  });
};

// 教学班管理-设置-新增
export const jxbgl_teaching_save = (data) => {
  return request({
    url: baseUrl + "/class/save",
    method: "post",
    data,
  });
};
// 教学班管理-设置-修改
export const jxbgl_teaching_update = (data) => {
  return request({
    url: baseUrl + "/class/update",
    method: "post",
    data,
  });
};

// 教学班管理-设置-选择教师
export const jxbgl_query_teacher_list = (GZBM, xm) => {
  return request({
    url: baseUrl + "/teacherInfo/list?GZBM=" + GZBM + "&xm=" + xm,
    method: "get",
  });
};

// 合班
export const jxbgl_calss_bh = (data) => {
  return request({
    url: baseUrl + "/class/hb",
    method: "post",
    data,
  });
};

// 拆班
export const jxbgl_calss_cb = (data) => {
  return request({
    url: baseUrl + "/class/cb",
    method: "post",
    data,
  });
};

// 删除教学班里面的某专业班次
export const jxbgl_calss_deleteZybcFromJxb = (zybcId, jxbId) => {
  return request({
    url:
      baseUrl + "/class/deleteZybcFromJxb?zybcId=" + zybcId + "&jxbId=" + jxbId,
    method: "post",
  });
};
// ================================= 3. 审核教学任务  ===================================================

// 教材任务和教材信息核对
export const shjx_query_list = (
  term,
  xb,
  courseName,
  pyLevel,
  nj,
  status,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/sh/list?xueqi=" +
      term +
      "&fzdw=" +
      xb +
      "&kcmc=" +
      courseName +
      "&pycc=" +
      pyLevel +
      "&nj=" +
      nj +
      "&state=" +
      status +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 教材任务和教材信息核对 导出
export const shjx_query_exportSh = (
  term,
  xb,
  courseName,
  pyLevel,
  nj,
  status
) => {
  return request({
    url:
      baseUrl +
      "/sh/exportSh?xueqi=" +
      term +
      "&xb=" +
      xb +
      "&courseName=" +
      courseName +
      "&pyLevel=" +
      pyLevel +
      "&nj=" +
      nj +
      "&status=" +
      status,
    method: "get",
  });
};

// 添加教材
export const jxbgl_data_addBook = (data) => {
  return request({
    url: baseUrl + "/sh/addBook",
    method: "post",
    data,
  });
};

// 获取教材详情
export const jxbgl_data_book_detail = (data) => {
  return request({
    url: baseUrl + "/sh/detail",
    method: "post",
    data
  });
};

// 添加备注
export const jxbgl_data_remark = (data) => {
  return request({
    url: baseUrl + "/sh/remark",
    method: "post",
    data,
  });
};

// 打回
export const jxbgl_data_dh = (data) => {
  return request({
    url: baseUrl + "/sh/dh",
    method: "post",
    data,
  });
};

// 发布
export const jxbgl_data_fb = (data) => {
  return request({
    url: baseUrl + "/sh/fb",
    method: "post",
    data,
  });
};

// 获取最后修改人基本信息
export const bpksj_query_teacher_info = (id) => {
  return request({
    url: baseUrl + "/teacherInfo/detail?id=" + id,
    method: "get",
  });
};

// 获取最后修改人教育信息
export const bpksj_query_edu_info = (id) => {
  return request({
    url: baseUrl + "/teacherInfo/edu?id=" + id,
    method: "get",
  });
};

// 下发核对
export const bpksj_query_edu_sh = (data) => {
  return request({
    url: baseUrl + "/sh/xf",
    method: "post",
    data,
  });
};
// 通过
export const bpksj_query_edu_tg = (data,classIds) => {
  return request({
    url: baseUrl + "/sh/tg",
    method: "post",
    data,
    params:{classIds}
  });
};

// ================================= 4. 教学任务查询  ===================================================

// 教学任务查询
export const jxrw_query_teaching_list2 = (data) => {
  return request({
    url: baseUrl + "/class/jxrwcx",
    method: "post",
    data,
  });
};

export const jxrw_query_teaching_list = (
  term,
  xb,
  NJ,
  eduLevel,
  specialtyClassId
) => {
  return request({
    url:
      baseUrl +
      "/teachingclassinfo/list?term=" +
      term +
      "&xb=" +
      xb +
      "&NJ=" +
      NJ +
      "&eduLevel=" +
      eduLevel +
      "&specialtyClassId=" +
      specialtyClassId,
    method: "get",
  });
};

// ================================= 5. 不排课时间  ===================================================

// 不排课时间-不排课时间列表接口
export const bpksj_query_invalid_time = (xueqi, current, size) => {
  return request({
    url:
      baseUrl +
      "/invalidTime/list?xueqi=" +
      xueqi +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

//  删除不排课时间表
export const jxbgl_data_remove = (id) => {
  return request({
    url: baseUrl + "/invalidTime/remove?id=" + id,
    method: "post",
  });
};

// 不排课时间-专业班次选择
export const bpksj_query_specialty = (
  trainingLevel,
  year,
  yearSpecialtyId,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/specialtyclassinfo/list?trainingLevel=" +
      trainingLevel +
      "&year=" +
      year +
      "&yearSpecialtyId=" +
      yearSpecialtyId +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 新增不排课时间表
export const jxbgl_data_save = (data) => {
  return request({
    url: baseUrl + "/invalidTime/save",
    method: "post",
    data,
  });
};
// 修改
export const jxbgl_data_update = (data) => {
  return request({
    url: baseUrl + "/invalidTime/update",
    method: "post",
    data,
  });
};

// ================================= 6. 不检测冲突教学场地  ===================================================

//不检测冲突教学场地管理    /placeinfo/listNoConflictPlace
export const reqGetPlaceInfo = (current, size) => {
  return request({
    url:
      baseUrl +
      "/placeinfo/listNoConflictPlace?current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

//批次移除不检测冲突的场地  placeinfo/updateBatchNoConflictRemove
export const updateBatchNoConflictRemove = (ids) => {
  return request({
    url: baseUrl + "/placeinfo/updateBatchNoConflictRemove?ids=" + ids,
    method: "post",
  });
};

//批次移除不检测冲突的场地  placeinfo/updateBatchNoConflictRemove
export const updateBatchNoConflictAdd = (ids) => {
  return request({
    url: baseUrl + "/placeinfo/updateBatchNoConflictAdd?ids=" + ids,
    method: "post",
  });
};

// ================================= 7. 排课  ===================================================

// 获取课程列表信息
export const paike_courseList = (data) => {
  return request({
    url: baseUrl + "/paike/courseList",
    method: "post",
    data,
  });
};

// 修改简称
export const paike_update_short = (data) => {
  return request({
    url: baseUrl + "/paikeInfo/update",
    method: "post",
    data,
  });
};

// 批量修改
export const paike_update_paike = (data) => {
  return request({
    url: baseUrl + "/paike/update",
    method: "post",
    data,
  });
};
// 批量删除
export const paike_delete_paike = (data) => {
  return request({
    url: baseUrl + "/paike/delete",
    method: "post",
    data,
  });
};

// 获取排课 课表
export const paike_index = (data) => {
  return request({
    url: baseUrl + "/paike/index",
    method: "post",
    data,
  });
};

// 查询推荐场地
export const paike_listPlaceInfo = (data) => {
  return request({
    url: baseUrl + "/paike/listPlaceInfo",
    method: "post",
    data,
  });
};

// 获取助教或主讲
export const paike_teacher_list = (GZBM, xm) => {
  return request({
    url: baseUrl + "/teacherInfo/list?gzbm=" + GZBM + "&xm=" + xm,
    method: "get",
  });
};

// 获取冲突周次
export const paike_get_ctzc = (data) => {
  return request({
    url: baseUrl + "/paike/ctzc",
    method: "post",
    data,
  });
};

export const paike_saveAll = (data) => {
  return request({
    url: baseUrl + "/paike/saveAll",
    method: "post",
    data,
  });
};

// ================================= 8. 调课  ===================================================

export const tk_change_course = (xueqi, fzdw, current, size) => {
  return request({
    url:
      baseUrl +
      "/paike/changeCourseList?xueqi=" +
      xueqi +
      "&fzdw=" +
      fzdw +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 调课登记
export const tk_addCourseList = (data) => {
  return request({
    url: baseUrl + "/paike/addCourseList",
    method: "post",
    data,
  });
};

// 删除调课记录
export const tk_removeCourseList = (id) => {
  return request({
    url: baseUrl + "/paike/removeCourseList?id=" + id,
    method: "post",
  });
};

// 调课登记 获取教学班内容
export const tk_teaching_class_info = (id) => {
  return request({
    url: baseUrl + "/teachingclassinfo/detail?id=" + id,
    method: "get",
  });
};

// 调课登记 获取教学班内容
export const tk_get_class_list = (
  fzdw,
  courseProperty,
  term,
  courseName,
  STAFF_ROOM
) => {
  return request({
    url:
      baseUrl +
      "/class/list?fzdw=" +
      fzdw +
      "&courseProperty=" +
      courseProperty +
      "&term=" +
      term +
      "&courseName=" +
      courseName +
      "&STAFF_ROOM=" +
      STAFF_ROOM,
    method: "get",
  });
};

// ================================= 9. 教研室排课情况统计  ===================================================

// 教研室排课情况统计
export const static_getJysPkqktj = (xueqi, current, size) => {
  return request({
    url:
      baseUrl +
      "/paikeInfo/getJysPkqktj?xueqi=" +
      xueqi +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 教研室排课情况统计 导出
export const static_exportJysPkqktj = (xueqi) => {
  return request({
    url: baseUrl + "/paikeInfo/exportJysPkqktj?xueqi=" + xueqi,
    method: "get",
    responseType: "blob",
  });
};

// 教学任务详情
export const static_getJxrw = (coursename, jys, status, xueqi) => {
  return request({
    url:
      baseUrl +
      "/paikeInfo/getJxrw?coursename=" +
      coursename +
      "&jys=" +
      jys +
      "&status=" +
      status +
      "&xueqi=" +
      xueqi,
    method: "get",
  });
};

// ================================= 10. 课表安排学时统计  ===================================================

// 教学任务详情
export const static_getKbapxstj = (data, current, size) => {
  return request({
    url:
      baseUrl + "/paikeInfo/getKbapxstj?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};

// 课表教师统计
export const static_getKbapxstj_teacher = (data, current, size) => {
  return request({
    url:
      baseUrl +
      "/paikeInfo/getKbapxstj/teacher?current=" +
      current +
      "&size=" +
      size,
    method: "post",
    data,
  });
};

// 课表课程统计
export const static_getKbapxstj_kc = (data, current, size) => {
  return request({
    url:
      baseUrl +
      "/paikeInfo/getKbapxstj/kc?current=" +
      current +
      "&size=" +
      size,
    method: "post",
    data,
  });
};

// ================================= 11. 教员工作量统计  ===================================================

// 教师工作量统计
export const static_getGzl = (name, xueqi, gzdw) => {
  return request({
    url:
      baseUrl +
      "/teacherInfo/getGzl?name=" +
      name +
      "&xueqi=" +
      xueqi +
      "&gzdw=" +
      gzdw,
    method: "get",
  });
};

// 教师工作量统计
export const static_exportGzl = (name, xueqi, gzdw) => {
  return request({
    url:
      baseUrl +
      "/teacherInfo/exportGzl?name=" +
      name +
      "&xueqi=" +
      xueqi +
      "&gzdw=" +
      gzdw,
    method: "get",
    responseType: "blob",
  });
};

// ================================= 12. 学时出入情况统计  ===================================================

export const static_paike_xscrqktj = (xueqi, zybc, nj, current, size) => {
  return request({
    url:
      baseUrl +
      "/paike/xscrqktj?xueqi=" +
      xueqi +
      "&zybc=" +
      zybc +
      "&nj=" +
      nj +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

export const static_paike_exportXscrqktj = (xueqi, zybc, nj) => {
  return request({
    url:
      baseUrl +
      "/paike/exportXscrqktj?xueqi=" +
      xueqi +
      "&zybc=" +
      zybc +
      "&nj=" +
      nj,
    method: "get",
  });
};

// ================================= 13. 空闲场地查询  ===================================================

// 空闲场地查询
export const static_aplace_info_freed = (data, current, size) => {
  return request({
    url:
      baseUrl + "/placeinfo/placeInfoFree?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};

// 空闲场地查询
export const static_aplace_info_exportPlaceInfoFree = (data) => {
  return request({
    url: baseUrl + "/placeinfo/exportPlaceInfoFree",
    method: "post",
    data,
  });
};

// ================================= 14. 场地使用登记  ===================================================

// 场地使用登记列表
export const cdsydj_place_list = (current, size) => {
  return request({
    url: baseUrl + "/placeRegist/list?current=" + current + "&size=" + size,
    method: "get",
  });
};

// 获取使用登记详情信息
export const cdsydj_place_list_details = (registid) => {
  return request({
    url: baseUrl + "/placeRegist/detail?registid=" + registid,
    method: "get",
  });
};

// 修改
export const cdsydj_place_list_update = (data) => {
  return request({
    url: baseUrl + "/placeRegist/update",
    method: "post",
    data,
  });
};

// 新增
export const cdsydj_place_list_add = (data) => {
  return request({
    url: baseUrl + "/placeRegist/add",
    method: "post",
    data,
  });
};

// ================================= 15. 选修课开课管理  ===================================================

// 从培养方案导入 查询列表
export const xxkkkgl_Pyfa_list = () => {
  return request({
    url: baseUrl + "/xxkkkgl/listFromPyfa",
    method: "get",
  });
};

// 从培养方案导入-跳转
export const xxkkkgl_Pyfa_kcFromPyfa = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/kcFromPyfa",
    method: "post",
    data,
  });
};

// // 从培养方案导入 查询列表
// export const xxkkkgl_Pyfa_kc = (courseno) => {
//   return request({
//     url: baseUrl+"/xxkkkgl/kc?courseno=" + courseno,
//     method: "get",
//   });
// };

// 从培养方案导入-提交接口
export const xxkkkgl_Pyfa_add = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/addFromPyfa",
    method: "post",
    data,
  });
};

// 添加选修课 查询列表
export const xxkkkgl_course_list = () => {
  return request({
    url: baseUrl + "/xxkkkgl/listFromJHCourse",
    method: "get",
  });
};

// 添加选修课 查询列表 跳转第二页
export const xxkkkgl_JH_course = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/kcFromJHCourse",
    method: "post",
    data,
  });
};

// 添加选修课-提交接口
export const xxkkkgl_JH_Course_add = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/addFromJHCourse",
    method: "post",
    data,
  });
};

// 设置选课要求  获取课程名称
export const xxkkkgl_JH_getClassNameByClassNo = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/getClassNameByClassNo",
    method: "post",
    data,
  });
};

// 设置选课要求 提交
export const xxkkkgl_JH_setClassXz = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/setClassXz",
    method: "post",
    data,
  });
};

// 点击课程编号
export const xxkkkgl_getCourse = (id) => {
  return request({
    url: baseUrl + "/xxkkkgl/getCourse?id=" + id,
    method: "get",
  });
};

// 点击课程编号 保存
export const xxkkkgl_setCourse = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/setCourse",
    method: "post",
    data,
  });
};

// 排课 保存
export const xxkkkgl_setPaike = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/setPaike",
    method: "post",
    data,
  });
};

// 获取排课
export const xxkkkgl_getPaike = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/getPaike?classNo=" + classNo,
    method: "get",
  });
};

// 获取教学班详细信息
export const xxkkkgl_getClassInfo = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/getClassInfo?classNo=" + classNo,
    method: "get",
  });
};

// 设置教学班详细信息
export const xxkkkgl_setClassInfo = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/setClassInfo",
    method: "post",
    data,
  });
};

// 开课
export const xxkkkgl_open = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/open?classNo=" + classNo,
    method: "post",
  });
};

// 停课
export const xxkkkgl_close = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/close?classNo=" + classNo,
    method: "post",
  });
};

// 删除
export const xxkkkgl_delete = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/delete",
    method: "post",
    data,
  });
};
// 获取学员信息
export const xxkkkgl_getClassStudent = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/getClassStudent?classNo=" + classNo,
    method: "get",
  });
};
// 获取学员队树
export const xxkkkgl_getPyccNjXyd = () => {
  return request({
    url: baseUrl + "/xxkkkgl/getPyccNjXyd",
    method: "get",
  });
};
// 获取学员队年级树
export const xxkkkgl_getPyccXydNj = () => {
  return request({
    url: baseUrl + "/xxkkkgl/getPyccXydNj",
    method: "get",
  });
};
// 获取学员队年级树
export const xxkkkgl_getStudent = (pycc, nj, xyd, zy, studentNo, name) => {
  return request({
    url:
      baseUrl +
      "/xxkkkgl/getStudent?pycc=" +
      pycc +
      "&nj=" +
      nj +
      "&xyd=" +
      xyd +
      "&zy=" +
      zy +
      "&studentNo=" +
      studentNo +
      "&name=" +
      name,
    method: "get",
  });
};

// 添加学员
export const xxkkkgl_addStudent = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/addStudent",
    method: "post",
    data,
  });
};
// 转正选
export const xxkkkgl_zZx = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/zZx",
    method: "post",
    data,
  });
};
// 转预选
export const xxkkkgl_zYx = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/zYx",
    method: "post",
    data,
  });
};
// 删除学时
export const xxkkkgl_deleteStudent = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/deleteStudent",
    method: "post",
    data,
  });
};
// 转教学班
export const xxkkkgl_zJxb = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/zJxb",
    method: "post",
    data,
  });
};
// 全转正选
export const xxkkkgl_zZxByClassNo = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/zZxByClassNo?classNo=" + classNo,
    method: "post",
  });
};
// 全转预选
export const xxkkkgl_zYxByClassNo = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/zYxByClassNo?classNo=" + classNo,
    method: "post",
  });
};
// 清楚非正选
export const xxkkkgl_cleanYx = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/cleanYx?classNo=" + classNo,
    method: "post",
    data,
  });
};
// 生成正选
export const xxkkkgl_addZx = (classNo) => {
  return request({
    url: baseUrl + "/xxkkkgl/addZx?classNo=" + classNo + "&type=" + type,
    method: "post",
    data,
  });
};

// 选修课开课管理列表
export const xxkkkgl_query_list = (
  xueqi,
  classname,
  pcmc,
  status,
  xi,
  classRoom,
  kcbh,
  courseName,
  mainEmployeeIds,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/xxkkkgl/list?xueqi=" +
      xueqi +
      "&classname=" +
      classname +
      "&pcmc=" +
      pcmc +
      "&status=" +
      status +
      "&xi=" +
      xi +
      "&classRoom=" +
      classRoom +
      "&kcbh=" +
      kcbh +
      "&courseName=" +
      courseName +
      "&mainEmployeeIds=" +
      mainEmployeeIds +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// ================================= 17. 选修课少于N门的学时  ===================================================

// 选修课少于N门的学生
export const xxk_getXkXyN = (xueqi, pycc, ms) => {
  return request({
    url:
      baseUrl + "/xkpc/getXkXyN?xueqi=" + xueqi + "&pycc=" + pycc + "&ms=" + ms,
    method: "get",
  });
};
// 选修课少于N门的学生 导出
export const xxk_exportXkXyN = (xueqi, pycc, ms) => {
  return request({
    url:
      baseUrl +
      "/xkpc/exportXkXyN?xueqi=" +
      xueqi +
      "&pycc=" +
      pycc +
      "&ms=" +
      ms,
    method: "get",
    responseType: "blob",
  });
};

// ================================= 18. 互联网选修课登记  ===================================================

// 选修课登记列表
export const hlwxk_list = (xueqi, current, size) => {
  return request({
    url:
      baseUrl +
      "/hlwxk/list?xueqi=" +
      xueqi +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 选修课登记详情
export const hlwxk_detail = (id) => {
  return request({
    url: baseUrl + "/hlwxk/detail?id=" + id,
    method: "get",
  });
};

// 登记
export const hlwxk_add = (xueqi, data) => {
  return request({
    url: baseUrl + "/hlwxk/add?xueqi=" + xueqi,
    method: "post",
    data,
  });
};
// 提交
export const hlwxk_submit = (data) => {
  return request({
    url: baseUrl + "/hlwxk/submit",
    method: "post",
    data,
  });
};
// 更新
export const hlwxk_update = (data) => {
  return request({
    url: baseUrl + "/hlwxk/update",
    method: "post",
    data,
  });
};
// 登记并提交
export const hlwxk_addAndSubmit = (xueqi, data) => {
  return request({
    url: baseUrl + "/hlwxk/addAndSubmit?xueqi=" + xueqi,
    method: "post",
    data,
  });
};

// ================================= 19. 互联网选修课登记审核  ===================================================

// 选修课登记审核详情
export const hlwxkSh_list = (
  xueqi,
  db,
  nj,
  zybc,
  xm,
  xh,
  kcmc,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/hlwxkSh/list?xueqi=" +
      xueqi +
      "&db=" +
      db +
      "&nj=" +
      nj +
      "&zybc=" +
      zybc +
      "&xm=" +
      xm +
      "&xh=" +
      xh +
      "&kcmc=" +
      kcmc +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 批量通过
export const hlwxkSh_tgBatch = (data) => {
  return request({
    url: baseUrl + "/hlwxkSh/tgBatch",
    method: "post",
    data,
  });
};

// 批量打回
export const hlwxkSh_dhBatch = (data) => {
  return request({
    url: baseUrl + "/hlwxkSh/dhBatch",
    method: "post",
    data,
  });
};

export const hlwxkSh_delete = (data) => {
  return request({
    url: baseUrl + "/hlwxkSh/delete",
    method: "post",
    data,
  });
};

// 队长提交
export const hlwxkSh_dz_submit = (data, xueqi, studentNo) => {
  return request({
    url:
      baseUrl + "/hlwxkSh/submitDj?xueqi=" + xueqi + "&studentNo=" + studentNo,
    method: "post",
    data,
  });
};

// ================================= 20. 选修课开课情况统计  ===================================================

export const xkpc_kkqktj = (xueqi) => {
  return request({
    url: baseUrl + "/xkpc/kkqktj?xueqi=" + xueqi,
    method: "get",
  });
};
// 导出
export const xkpc_exportKkqktj = (xueqi) => {
  return request({
    url: baseUrl + "/xkpc/exportKkqktj?xueqi=" + xueqi,
    method: "get",
    responseType: "blob",
  });
};

// ================================= 21. 网上选课  ===================================================

// 网上选课 列表查询
export const wsxk_wsxkList = (xueqi) => {
  return request({
    url: baseUrl + "/xkpc/wsxkList?xueqi=" + xueqi,
    method: "get",
  });
};

// 选课列表
export const wsxk_wsxkJxbList = (batchId) => {
  return request({
    url: baseUrl + "/xkpc/wsxkJxbList?batchId=" + batchId,
    method: "get",
  });
};

// 选课列表
export const wsxk_wsxkList_list = (xueqi) => {
  return request({
    url: baseUrl + "/xkpc/getWsxk?xueqi=" + xueqi,
    method: "get",
  });
};

// 选课
export const wsxk_submit = (data) => {
  return request({
    url: baseUrl + "/xkpc/wsxk",
    method: "post",
    data,
  });
};

// ================================= 22. 选修课选课情况查询  ===================================================

// 课程视角
export const hlwxk_xxkcx_kc = (data, current, size) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/kc?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};
// 课程视角 导出
export const hlwxk_xxkcx_exportKc = (data) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/exportKc",
    method: "post",
    data,
  });
};
// 人员视角
export const hlwxk_xxkcx_ry = (data, current, size) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/ry?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};
// 人员视角 导出
export const hlwxk_xxkcx_exportRy = (data) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/exportRy",
    method: "post",
    data,
  });
};

// 人员列表
export const hlwxk_xxkcx_ry_cx = (data) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/ry/cx",
    method: "post",
    data,
  });
};

// 人员列表 导出
export const hlwxk_xxkcx_ry_exportCx = (data) => {
  return request({
    url: baseUrl + "/hlwxk/xxkcx/ry/exportCx",
    method: "post",
    data,
  });
};

// ================================= 23. 课时查询  ===================================================

// 队长查询
export const kscx_kcb_dz = (type) => {
  return request({
    url: baseUrl + "/kcb/dz?type=" + type,
    method: "get",
  });
};

// 队长查询
export const kscx_kcb_exportDz = (type) => {
  return request({
    url: baseUrl + "/kcb/exportDz?type=" + type,
    method: "get",
    responseType: "blob",
  });
};

// 学员查询
export const kscx_kcb_dy = (type) => {
  return request({
    url: baseUrl + "/kcb/dy?type=" + type,
    method: "get",
  });
};

// 学员查询 导出
export const kscx_kcb_exportDy = (type) => {
  return request({
    url: baseUrl + "/kcb/exportDy?type=" + type,
    method: "get",
    responseType: "blob",
  });
};

// ================================= 24. 时间段课时查询  ===================================================

// 时间段课表查询
export const kscx_kcb_sjd = (data, current, size) => {
  return request({
    url: baseUrl + "/kcb/sjd?current=" + current + "&size=" + size,
    method: "post",
    data,
  });
};

// 时间段课表查询
export const kscx_kcb_exportSjd = (data) => {
  return request({
    url: baseUrl + "/kcb/exportSjd",
    method: "post",
    data,
    responseType: "blob",
  });
};

// ================================= 24. 时间段课时查询  ===================================================

// 专业班次课表查询 一班一表
export const kscx_kcb_zyYbyb_one = (name, xueqi) => {
  return request({
    url: baseUrl + "/kcb/zyYbyb?name=" + name + "&xueqi=" + xueqi,
    method: "get",
  });
};

// 专业班次课表查询 一班一表 daochu
export const kscx_kcb_zyYbyb_one_exportZyYbyb = (name, xueqi) => {
  return request({
    url: baseUrl + "/kcb/exportZyYbyb?name=" + name + "&xueqi=" + xueqi,
    method: "get",
    responseType: "blob",
  });
};

// 专业班次课表查询 多班一表
export const kscx_kcb_zyDbyb_more = (data) => {
  return request({
    url: baseUrl + "/kcb/zyDbyb",
    method: "post",
    data,
  });
};

// 专业班次课表查询 多班一表 导出
export const kscx_kcb_zyDbyb_more_exportZyDbyb = (data) => {
  return request({
    url: baseUrl + "/kcb/exportZyDbyb",
    method: "post",
    data,
    responseType: "blob",
  });
};
// ================================= 27. 部门课表查询  ===================================================

// 部门课表查询
export const kscx_kcb_bm = (xq, cddw, jys, zcSart, zcEnd) => {
  return request({
    url:
      baseUrl +
      "/kcb/bm?xq=" +
      xq +
      "&cddw=" +
      cddw +
      "&jys=" +
      jys +
      "&zcSart=" +
      zcSart +
      "&zcEnd=" +
      zcEnd,
    method: "get",
  });
};

// 部门课表查询
export const kscx_kcb_bm_export = (xq, cddw, jys, zcSart, zcEnd) => {
  return request({
    url:
      baseUrl +
      "/kcb/exportBm?xq=" +
      xq +
      "&cddw=" +
      cddw +
      "&jys=" +
      jys +
      "&zcSart=" +
      zcSart +
      "&zcEnd=" +
      zcEnd,
    method: "get",
    responseType: "blob",
  });
};

// 部门课表查询
export const kscx_kcb_bm_details = (data) => {
  return request({
    url: baseUrl + "/kcb/bm/xq",
    method: "post",
    data,
  });
};

// 默认学期信息
export const queryDefaultXueQi = () => {
  return request({
    url: baseUrl + "/zdzybcxqzxjh/defaultXueQi",
    method: "get",
  });
};

// 教材库
export const text_book_list = (xq, nj, pycc, zt, cbrq, current, size) => {
  return request({
    url:
      baseUrl +
      "/textbookinfo/list?bh=" +
      xq +
      "&jcmc=" +
      nj +
      "&zz=" +
      pycc +
      "&cbs=" +
      zt +
      "&cbrq=" +
      cbrq +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

// 添加课程批次
export const addKcpi = (data) => {
  return request({
    url: baseUrl + "/xkpc/addInfo",
    method: "post",
    data,
  });
};

export const teacherTree = (name) => {
  return request({
    url: baseUrl + "/teacherInfo/teacherInfoTree?name=" + name,
    method: "get",
  });
};

export const allTeacherList = (data) => {
  return request({
    url: baseUrl + "/teacherInfo/allList",
    method: "post",
    data,
  });
};

// 选课分页查询
export const xkPageList = (xueqi) => {
  return request({
    url: baseUrl + "/xkpc/pageList?xueqi=" + xueqi,
    method: "get",
  });
};

// 选课详情
export const xkInfo = (id) => {
  return request({
    url: baseUrl + "/xkpc/getInfo?id=" + id,
    method: "get",
  });
};

// 修改选课批次
export const updateXkpc = (data) => {
  return request({
    url: baseUrl + "/xkpc/setInfo",
    method: "post",
    data,
  });
};
// 选课 添加学员范围 培养方案列表
export const getAddXyXkfw = (year, pycc) => {
  return request({
    url: baseUrl + "/xkpc/getAddXyXkfw?year=" + year + "&pycc=" + pycc,
    method: "get",
  });
};

// 添加选课学员范围信息
export const addXyXkfwInfo = (data) => {
  return request({
    url: baseUrl + "/xkpc/addXyXkfw",
    method: "post",
    data,
  });
};

// 获取已选学员范围信息
export const getXyXkfwInfo = (id) => {
  return request({
    url: baseUrl + "/xkpc/getXyXkfw?id=" + id,
    method: "get",
  });
};

// 删除已选学员范围
export const deleteXyXkfw = (id) => {
  return request({
    url: baseUrl + "/xkpc/deleteByXkRange/" + id,
    method: "delete",
  });
};

// 获取已选教学班信息
export const getJxbInfo = (id) => {
  return request({
    url: baseUrl + "/xkpc/getJxb?id=" + id,
    method: "get",
  });
};

// 设置教学班信息
export const setJxbInfo = (data) => {
  return request({
    url: baseUrl + "/xkpc/setJxb",
    method: "post",
    data,
  });
};
// 删除批次班级信息
export const deleteBatchClass = (pcId, classId) => {
  return request({
    url: baseUrl + "/xkpc/deleteByJxb/" + pcId + "/" + classId,
    method: "delete",
  });
};

// 主键修改
export const updateByPrimaryKey = (data) => {
  return request({
    url: baseUrl + "/xxkkkgl/UpdateRy",
    method: "post",
    data: data,
  });
};

// 查询课程详细信息
export const queryCourseDetail = (id) => {
  return request({
    url: baseUrl + "/xxcourse/querysxcoursebyid?id=" + id,
    method: "get",
  });
};
// 修改教学班及课程信息
export const updateClassCourseInfo = (data) => {
  return request({
    url: baseUrl + "/xxteachingclass/updatejiaoxueban",
    method: "post",
    data: data,
  });
};
// 获取教学班详细信息
export const queryClassDetail = (classNo) => {
  return request({
    url: baseUrl + "/xxteachingclass/detail?classno=" + classNo,
    method: "get",
  });
};
// 查询选修课学员信息
export const queryStudentList = (data) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/querystudentlist",
    method: "post",
    data: data,
  });
};
// 批量删除选修课学员
export const removeStudents = (ids) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/remove?ids=" + ids,
    method: "post",
  });
};
// 批量修改选修课学员信息
export const updateStudents = (data) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/updatestudentxuanxiuke",
    method: "post",
    data: data,
  });
};
// 根据教学班编号批量查询教学班信息
export const queryClassList = (data) => {
  return request({
    url: baseUrl + "/xxteachingclass/plsetzxandyxstudent",
    method: "post",
    data: data,
  });
};
// 批量转正选或转预选
export const batchUpdateByClassNo = (data) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/plzzxhyx",
    method: "post",
    data: data,
  });
};
// 批量删除非正选人员
export const batchDeleteByClassNo = (data) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/removestudent",
    method: "post",
    data: data,
  });
};
// 保存排课信息
export const saveCoursePlans = (data) => {
  return request({
    url: baseUrl + "/xxtecahingclasscourse/setxxpaike",
    method: "post",
    data: data,
  });
};
// 排课时间详情
export const queryCoursePlanDetail = (data) => {
  return request({
    url: baseUrl + "/xxtecahingclasscourse/paiketimeanpai",
    method: "post",
    data: data,
  });
};
// 查找学员
export const queryStudentsList = (data, pageNo, pageSize) => {
  return request({
    url: "/api/xpaas-home-service/studentinfo/selectByinfo",
    method: "post",
    data: data,
    params: { current: pageNo, size: pageSize },
  });
};
// 批量添加学员
export const batchAddstudents = (data) => {
  return request({
    url: baseUrl + "/xxteachingclassstudent/saveofpl",
    method: "post",
    data: data,
  });
};

export const updateCourseList = (data) => {
  return request({
    url: baseUrl + "/paike/updateCourseList",
    method: "post",
    data: data,
  });
};

export const deletePlaceInfo = (id) => {
  return request({
    url: baseUrl + "/placeRegist/delete/" + id,
    method: "delete",
  });
};

//获取部门
export const queryCommonJysDept = () => {
  return request({
    url: baseUrl + "/common/jysdept",
    method: "get",
  });
};

export const importAddPykcByZybcId = (pykcId, zybcId, xueqi) => {
  return request({
    url:
      baseUrl +
      "/zdzybcxqzxjh/importAddPykcByZybcId?pykcId=" +
      pykcId +
      "&zybcId=" +
      zybcId +
      "&xueqi=" +
      xueqi,
    method: "post",
  });
};

// selectedBatchCourseList
export const selectedBatchCourseList = (
  xueqi,
  classname,
  xkpc,
  current,
  size
) => {
  return request({
    url:
      baseUrl +
      "/xxkkkgl/selectedBatchCourseList?xueqi=" +
      xueqi +
      "&classname=" +
      classname +
      "&xkpc=" +
      xkpc +
      "&current=" +
      current +
      "&size=" +
      size,
    method: "get",
  });
};

export const deleteXkpcInfo = (id) => {
  return request({
    url: baseUrl+"/xkpc/delete?id=" + id,
    method: "post",
  });
};

export const termDetails = (id) => {
  return request({
    url: baseUrl+"/systerm/detail?id=" + id,
    method: "get",
  });
};

// 批量提醒
export const course_time_tx = (data) => {
  return request({
    url: baseUrl + "/msg/save",
    method: "post",
    data: data
  });
};

// 查询详细信息
export const updateCourseCharger = (id, name) => {
  return request({
    url: baseUrl + "/specialClassInfo/updateCourseCharger?jhCourseId=" + id + "&courseCharger=" + name,
    method: "get",
  });
};

