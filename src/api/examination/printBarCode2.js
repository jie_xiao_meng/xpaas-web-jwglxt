import request from '@/router/axios';
// 课程考核与实施计划


export const getSelectCondition = (row) => {
  return request({
    url: '/api/xpaas-course-service/specialtyclassinfo/selectCondition',
    method: 'post',
    data: row
  })
}


//学员条形码打印---按照专业班次
export const getmajor = (row) => {
  return request({
    url: '/api/xpaas-course-service/kcbarcodeprint/major',
    method: 'post',
    data: row,
    params: { current: row.current, size: row.size }
  })
}
//学员条形码打印---按照专业班次
export const getstudent = (row) => {
  return request({
    url: '/api/xpaas-course-service/kcbarcodeprint/major',
    method: 'post',
    data: row
  })
}
//学员条形码打印---按照课程信息
export const getcourse = (row) => {
  return request({
    url: '/api/xpaas-course-service/kcbarcodeprint/course',
    method: 'post',
    data: row,
    params: { current: row.current, size: row.size }
  })
}
export const selectByTeam = (row) => {
  return request({
    url: '/api/xpaas-home-service/studentinfo/selectByTeam',
    method: 'post',
    data: row
  })
}
export const studentList = (row) => {
  return request({
    url: '/api/xpaas-home-service/studentinfo/studentList',
    method: 'post',
    data: row
  })
}
export const queryCourseList = (data) => {
  return request({
    url: '/api/xpaas-home-service/studentinfo/selectByplanid?planid=' + data,
    method: 'post'
  })
}
