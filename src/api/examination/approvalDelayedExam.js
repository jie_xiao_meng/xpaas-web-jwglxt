import request from '@/router/axios';
// 课程考核与实施计划


export const getSelectCondition = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/selectCondition',
    method: 'post',
    params: { current, size},
    data: row
  })
}
export const studentSelectCondition = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/studentSelectCondition',
    method: 'post',
    params: { current, size},
    data: row
  })
}

export const capSelectCondition = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/capSelectCondition',
    method: 'post',
    params: { current, size},
    data: row
  })
}
export const addRemark = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseassesscheck/addRemark',
    method: 'post',
    data: row
  })
}

export const addMessage = (row) => {
  return request({
    url: '/api/xpaas-course-service/shspeed/addMessage',
    method: 'post',
    data: row
  })
}
export const updateStatus = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/updateStatus',
    method: 'post',
    data: row
  })
}
export const updateMessage = (row) => {
  return request({
    url: '/api/xpaas-course-service/shspeed/updateMessage',
    method: 'post',
    data: row
  })
}
export const detail = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/detail',
    method: 'post',
    data: row
  })
}
export const submit = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/submit',
    method: 'post',
    data: row
  })
}
export const getUpload = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/upload',
    method: 'post',
    data: row
  })
}
export const createDId = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/createDId',
    method: 'post',
    data: row
  })
}
export const getsave = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/save',
    method: 'post',
    data: row
  })
}
export const getCurrentOne = () => {
  return request({
    url: '/api/xpaas-home-service/teacherinfo/getCurrentOne',
    method: 'get',

  })
}
export const hkkcList = (termid) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/hkkcList',
    method: 'get',
    params:{ termid }
  })
}
export const reject = (row) => {
  return request({
    url: '/api/xpaas-course-service/shspeed/reject',
    method: 'post',
    data: row
  })
}


//考试计划 ---- 开始排考
export const alreadyIssue = ( kpId ) => {
  return request({
    url: '/api/xpaas-course-service/examplan/alreadyIssue',
    method: 'get',
    params:{ kpId  }
  })
}
export const stayIssue = ( kpId ) => {
  return request({
    url: '/api/xpaas-course-service/examplan/stayIssue',
    method: 'get',
    params:{ kpId }
  })
}
//补缓考安排发布与取消发布
export const updateBatchStatus = (ids,status) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/updateBatchStatus',
    method: 'get',
    params: { ids,status }
  })
}