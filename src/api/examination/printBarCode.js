import request from '@/router/axios';
// 课程考核与实施计划


export const getSelectConditionList = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/selectConditionList',
    method: 'post',
    data: row,
    params:{ current,size }
  })
}

export const gettwoPassCondition = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/twoPassCondition',
    method: 'post',
    params:{current,size},
    data: row
  })
}
export const regisPass = (row) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/regisPass',
    method: 'post',
    data:row
    // params: {
    //   ids,
    // }
  })
}

export const outPass = (row) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/outPass',
    method: 'post',
    data:row
  })
}
