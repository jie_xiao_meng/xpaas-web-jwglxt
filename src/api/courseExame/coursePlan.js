import request from '@/router/axios';
// 考试安排
export const getList = (current, size, params) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
export const coursePlanGetList = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/queryAllIdeExaDetail',
    method: 'post',
    data:row,
    params: {
      current,
      size,
    }
  })
}
export const courseTeaPlanGetList = (query) => {
  return request({
    url: '/api/xpaas-course-service/examplan/queryTeaExaDetail',
    method: 'get',
    params: query
  })
}


export const examsPlanBuList = (current,size,row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/queryAllBuHuanExa',
    method: 'post',
    data: row,
    params:{current,size}
  })
}
export const exaTeacherCheck = (query) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/exaTeacherCheck',
    method: 'get',
    params: query
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/x/examtechercheck/submit',
    method: 'post',
    data: row
  })
}
export const checkUpdate = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplancheck/update',
    method: 'post',
    data: row
  })
}

export const selectDetail = (checkid) => {
  return request({
    url: '/api/xpaas-course-service/examplancheck/detail',
    method: 'get',
    params: {checkid}
  })
}
export const addApply = (row) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/save',
    method: 'post',
    data: row
  })
}
export const editRemark = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplancalss/update',
    method: 'post',
    data: row
  })
}

export const selectClassDetail = (query) => {
  return request({
    url: '/api/xpaas-course-service/examplancalss/detail',
    method: 'get',
    params: query
  })
}

//学员考试安排查询页面批次下拉框
export const selectPc = () => {
  return request({
    url: '/api/xpaas-course-service/examplan/selectPc',
    method: 'get'
  })
}
