import request from '@/router/axios';
// 课程考核与实施计划
export const getList = (current, size, params) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
// 学员
export const courseGetList = (query) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryAllStudentCoursesPlan',
    method: 'get',
    params: query
  })
}

export const getDetail = (planid) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/detail',
    method: 'get',
    params: {
      planid
    }
  })
}
//导出Excel
export const coursePlanPrintExcel = (ids) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/coursePlanPrintExcel'  ,
    method: 'post',
    params: {
      ids
    }
  })
}

export const queryTeaCheck = (query) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTeaCheck'  ,
    method: 'get',
    params: query
  })
}

export const update = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/update',
    method: 'post',
    data: row
  })
} 

export const updateSubmit = (cpId,roId) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/updateSubmit',
    method: 'post',
    params: {
      cpId, roId
    }
  })
}
export const selectCondition = (row) => {
  return request({
    url: '/api/xpaas-course-service/shijuanprint/selectCondition',
    method: 'post',
    data: row
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/x/examtechercheck/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplanmodify/submit',
    method: 'post',
    data: row
  })
}

export const queryTeacherCourseCheck = (query) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplanmodify/queryTeacherCourseCheckLog',
    method: 'get',
    params: query
  })
}


export const insertCourseModify = (chId,teaId) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplanmodify/insertCourseModify',
    method: 'post',
    params: {
      chId,teaId
    }
  })
}

export const getCheckDetail = (cheplanid) => {
  return request({
    url: '/api/xpaas-course-service/courseassesscheck/detail',
    method: 'get',
    params: {
      cheplanid
    }
  })
}

export const checkUpdate = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseassesscheck/update',
    method: 'post',
    data: row
  })
}
export const addRemark = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseassesscheck/addRemark',
    method: 'post',
    data: row
  })
}

// 删除
export function checkDelete(ids) {
  return request({
    url: '/api/xpaas-course-service/courseassesscheck/remove',
    method: 'delete',
    params: ids
  })
}

export const getAssDetail = (ids) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplanmodify/teadetail',
    method: 'get',
    params: {ids}
  })
}

export const docPrint = (ids) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/coursePlanPrint',
    method: 'get',
    params: {ids}
  })
}

export const hpinPort = (cpId) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryAllCheck',
    method: 'get',
    params: {cpId}
  })
}
export const dhRemark = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/examTeasCheck',
    method: 'post',
    data: row
  })
}
