import request from '@/router/axios';
// 课程考核与实施计划
export const getList = (current, size, params) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/x/examtechercheck/submit',
    method: 'post',
    data: row
  })
}

