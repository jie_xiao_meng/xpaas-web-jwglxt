import request from '@/router/axios';
//教材库管理
export const jckgl = (obj) => {
    return request({
        url: '/api/xpaas-plan-service/textbookinfo/Tjquerytextbook',
        method: 'post',
        data: obj
    })
}

//教材库管理    /textbookinfo/Tjquerytextbook
export const reqGetInitList = (
  current,
  size,
  bh,
  bookname,
  name,
  chubanshe,
  chubanriqi,
  course
) => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/Tjquerytextbook",
    method: "get",
    params: {
      current,
      size,
      bh,
      bookname,
      name,
      chubanshe,
      chubanriqi,
      course
    }
  });
};
export const tjxjc = obj => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/save",
    method: "post",
    data: obj
  });
};
export const xiugai = id => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/selectById",
    method: "get",
    params: { id }
  });
};
export const xiugaisave = obj => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/update",
    method: "post",
    data: obj
  });
};
export const sc = ids => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/remove",
    method: "get",
    params: { ids }
  });
};
export const yykc = id => {
  return request({
    url: "/api/xpaas-plan-service/textbookinfo/selectbookCourse",
    method: "get",
    params: { id }
  });
};
