import request from '@/router/axios';

//学员网上评教查询到所有数据
export const studentlist = (studentid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/selectPJ',
    method: 'post',
    params: {
      studentid,
    }
  })
}

//网上评教
export const supstudentlist = (studentid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/select',
    method: 'post',
    params: {
      studentid,
    }
  })
}

//网上评教
export const studentResultid = (resultid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/updatePjResult',
    method: 'post',
    params: {
      resultid,
    }
  })
}


export const selectPjMany = (studentid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/selectPjMany',
    method: 'post',
    params: {
      studentid,
    }
  })
}


//修改保存网上评教内容
export const pjupdate= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/update ',
    method: 'post',
    data:row,
  })
}

//课程考核成绩查询(学生)
export const kckhdate= (type) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/Tjkcqueryallscorestu',
    method: 'get',
    params :{ type }
  })
}
//课程考核成绩数据(队长)

export const kckhdated= (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/TjkcqueryallscoreDz ',
    method: 'post',
    data:obj
  })
}
//课程考核成绩查询(队长)

export const kckhdatec= (obj) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/TjkcqueryallscoreDz ',
    method: 'post',
    data:obj
  })
}



