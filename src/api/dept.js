import request from '@/router/axios';


/** 按类别查询组织机构 */
export const loadDeptListByCategory = (deptCategory) => {
  return request({
    url: "/api/xpaas-system/dept/list",
    method: "get",
    params: { deptCategory: deptCategory }
  }).then(res=>res.data)
}
