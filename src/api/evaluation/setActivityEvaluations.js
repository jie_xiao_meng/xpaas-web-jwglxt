import request from "@/router/axios";

export const ByAllRankAndGuide = () => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/ByAllRankAndGuide",
    method: "get"
  });
};
export const insert = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/insert",
    method: "post",
    data: row
  });
};
export const insert2 = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/update",
    method: "post",
    data: row
  });
};
export const teachingclassSelectAll = () => {
  return request({
    url: "/api/xpaas-home-service/teachingclassinfo/selectAll",
    method: "get"
  });
};
export const teacherinfoSelectAll = () => {
  return request({
    url: "/api/xpaas-home-service/teacherinfo/selectAll",
    method: "get"
  });
};
