import request from '@/router/axios';
let requestType = { headers: { 'content-type': 'application/x-www-form-urlencoded' } };
let baseUrl = '/api/xpaas-student'
// 教学研究情况列表
export const researchList = (params) => {
  return request({
    url: baseUrl+'/kpiResearch/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 教学研究情况详情
export const researchInfo = (params) => {
    return request({
      url: baseUrl+'/kpiResearch/detail',
      method: 'get',
      params:params
    }).then(res=>res.data)
  }
//修改教学研究情况
export const editResearch = (params) => {
    return request({
      url: baseUrl+'/kpiResearch/update',
      method: 'put',
      data:params
    }).then(res=>res.data)
  }
//删除教学研究情况信息
export const deleteResearch = (params) => {
    return request({
      url: baseUrl+'/kpiResearch/delete',
      method: 'delete',
      params:params,
      requestType
    }).then(res=>res.data)
  }
// 查询教学研究情况授权列表
export const givenList = (params) => {
  return request({
    url: baseUrl+'/kpiResearchAuth/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//编辑教学研究情况授权
export const dataExtendListSave = (params) => {
  return request({
    url: baseUrl+'/kpiResearchAuth/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 优质课
//获取候选对象
export const hxList = (params) => {
  return request({
    url: baseUrl+'/hightQualityClass/getHqcImportData',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//删除候选对象
export const delHX = (params) => {
  return request({
    url: baseUrl+'/hightQualityClass/removeHqc',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
// 获取评选结果
export const pxList = (params) => {
  return request({
    url: baseUrl+'/hqcResult/getHqcResultData',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//删除评选结果
export const delHPX = (params) => {
  return request({
    url: baseUrl+'/hqcResult/removeHqcResult',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
export const teacherList = (params) => {
  return request({
    url: baseUrl+'/sysTeacherInfo/teacherByUserId',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
