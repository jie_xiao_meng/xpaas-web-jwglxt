import request from '@/router/axios';
let baseUrl = '/api/xpaas-student'
// 一线数据指标管理--查询右边表格
export const getCurrentSysTerm = (params) => {
  return request({
    url: baseUrl+'/sysTerm/getCurrentSysTerm',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 一线数据指标管理--查询右边表格
export const dataConfigList = (params) => {
    return request({
      url: baseUrl+'/frontlineConfig/list',
      method: 'get',
      params:params
    }).then(res=>res.data)
  }
//一线数据指标管理--保存右边表格
export const dataConfigSave = (params) => {
  return request({
    url: baseUrl+'/frontlineConfig/edit',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 复制一线数据配置
export const copyConfig = (params) => {
  return request({
    url: baseUrl+'/frontlineConfig/copyConfig',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 一获取填报人信息
export const getDataInfo = (params) => {
  return request({
    url: baseUrl+'/frontlineData/getUserFillData',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 获取所有标准体系
export const bzList = (params) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalguide/ByAllRankAndGuide',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询一线数据配置扩展字段列表 添加字段
export const dataExtendList = (params) => {
  return request({
    url: baseUrl+'/frontlineConfig/extendConfigList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//一线数据指标管理--保存右边表格
export const dataExtendListSave = (params) => {
  return request({
    url: baseUrl+'/frontlineConfig/extendEdit',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 查询所有用户
export const roleList = (params) => {
  return request({
    url: '/api/xpaas-system/role/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

// 查询一线数据列表
export const dataList = (params) => {
  return request({
    url: baseUrl+'/frontlineData/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//批量打回一线数据填报
export const rejectLineData = (params) => {
  return request({
    url: baseUrl+'/frontlineData/reject',
    method: 'put',
    data:params
  }).then(res=>res.data)
}
//一线数据填报
export const lineDataSave = (params) => {
  return request({
    url: baseUrl+'/frontlineData/edit',
    method: 'post',
    data:params
  }).then(res=>res.data)
}

// 一线数据调停课列表
export const dataTTList = (params) => {
  return request({
    url: baseUrl+'/frontlineMediation/gather',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 一线数据调停课详情
export const dataTTDetail = (params) => {
  return request({
    url: baseUrl+'/frontlineMediation/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 保存一线数据调停课信息列表保存
export const dataTTSave = (params) => {
  return request({
    url: baseUrl+'/frontlineMediation/edit',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 一线数据统计跟课
export const dataFollowList = (params) => {
  return request({
    url: baseUrl+'/frontlineData/gatherFollow',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 一线数据统计听课
export const dataAttendList = (params) => {
  return request({
    url: baseUrl+'/frontlineData/gatherListen',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 获取标准体系详情
export const bzLDetail = (params) => {
  return request({
    url: baseUrl+'/api/pjguideitem/paixu',
    method: 'post',
    data:params
  }).then(res=>res.data)
}

//教学基础数据统计 
// 教学基础数据统计列表
export const basicTJList = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/query',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 教学基础数据统计列表2
export const basicKCList1 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/queryTeacherStatics',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 教学基础数据统计列表合计2
export const basicKCListSum1 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/teacherStaticsCount',
    method: 'get',
    params:params
  }).then(res=>res)
}
export const basicKCList2 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/queryStaffStatics',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
export const basicKCListSum2 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/queryStaffStaticsCount',
    method: 'get',
    params:params
  }).then(res=>res)
}
export const basicKCList3 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/queryMajorStaticsCourse',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
export const basicKCListSum3 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/majorStaticsCount',
    method: 'get',
    params:params
  }).then(res=>res)
}
export const basicKCList4 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/queryMajorStatics',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
export const basicKCListSum4 = (params) => {
  return request({
    url: baseUrl+'/teachBasicDataStatics/majorStaticsCourseCount',
    method: 'get',
    params:params
  }).then(res=>res)
}
//获取周期
export const getTermWeek = (params) => {
  return request({
    url: baseUrl+'/sysTerm/getTermWeek',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//授课情况列表
export const skList = (params) => {
  return request({
    url: baseUrl+'/KpiTeach/teachKpiList',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//授课情况 授予权限列表
export const syqxList = (params) => {
  return request({
    url: baseUrl+'/teachKpiAuth/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//授权保存
export const sqSave = (params) => {
  return request({
    url: baseUrl+'/teachKpiAuth/saveTeachAuth',
    method: 'post',
    data:params
  }).then(res=>res.data)
}


