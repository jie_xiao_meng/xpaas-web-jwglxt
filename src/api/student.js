import request from '@/router/axios';
// import store from '../store'
let requestType = { headers: { 'content-type': 'application/x-www-form-urlencoded' } };
let baseUrl = '/api/xpaas-student'
//学员奖励信息列表
export const rewardList = (params) => {
  return request({
    url: baseUrl+'/xyRewardInfo/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//新增奖励信息
export const addRewardInfo = (params) => {
  return request({
    url: baseUrl+'/xyRewardInfo/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//学员奖励信息列表
export const rewardDetail = (params) => {
  return request({
    url: baseUrl+'/xyRewardInfo/detail',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//修改学员奖励信息
export const updateRewardDetail = (params) => {
  return request({
    url: baseUrl+'/xyRewardInfo/update',
    method: 'put',
    data:params
  }).then(res=>res.data)
}

//删除奖励信息
export const deleteReward = (params) => {
  return request({
    url: baseUrl+'/xyRewardInfo/delete',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
//学员奖励类别
export const rewardTypeList = () => {
  return request({
    url: baseUrl+'/xyRewardType/list',
    method: 'get'
  }).then(res=>res.data.data)
}
//学员奖励级别
export const rewardGradeList = () => {
  return request({
    url: baseUrl+'/xyRewardGrade/list',
    method: 'get'
  }).then(res=>res.data.data)
}

//学员个人信息查询
export const getStudentInfo = (params) => {
  return request({
    url: baseUrl+'/sysStudentInfo/getStudentInfo',
    method: 'get',
    params:params
  }).then(res=>res.data.data)
}

//选择学员查询----获得培养层次
export const getFirstLevel = (params) => {
  return request({
    url: baseUrl+'/xpaasDictBiz/getTrainingLevel',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//选择学员查询----获取年份
export const getlistYear = (pyLevel) => {
  return request({
    url: baseUrl+'/sysTerm/listYearSpecialty',
    method: 'get',
    params:{pyLevel}
  }).then(res=>res.data)
}
//选择学员查询----获得专业
export const getZY = (params) => {
  return request({
    url: baseUrl+'/studentView/getYearSpecialty',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//选择学员查询----获得专业
export const getNewZY = (params) => {
  return request({
    url: baseUrl+'/studentView/getRegisterYearSpecialty',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//选择学员查询----获得专业班次
export const getZYBC = (params) => {
  return request({
    url: baseUrl+'/jhSpecialtyClassInfo/querySpecialtyClassInfo',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//选择学员查询----获得学员花名册
export const getStudentList = (params) => {
  return request({
    url: baseUrl+'/studentView/queryStudentRegister',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
export const deleteStudent = (params) => {
  return request({
    url: baseUrl+'/sysStudentInfo/remove',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}

//获得处分类型
export const getPunishType = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentType/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询处分列表
export const getPunishList = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentInfo/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//删除处分列表数据
export const deletePunish = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentInfo/delete',
    method: 'delete',
    params:params,
    requestType
  }).then(res=>res.data)
}
//查询学员处分信息
export const getPunishDetail = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentInfo/detail',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//修改学员处分信息
export const updatePunishDetail = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentInfo/update',
    method: 'put',
    data:params
  }).then(res=>res.data)
}
//新增学员处分信息
export const addPunishInfo = (params) => {
  return request({
    url: baseUrl+'/xyPunishmentInfo/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}

//学员- 新增/编辑学生基础信息
export const addStudentBasic = (params) => {
  return request({
    url: baseUrl+'/sysStudentInfo/alter',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//学员-添加-入学毕业信息
export const addStudentRX = (params) => {
  return request({
    url: baseUrl+'/sysStudentInfo/addEntranceOrGraduateInfo',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//学员-添加-家庭成员信息
export const addStudentFamily = (params) => {
  return request({
    url: baseUrl+'/sysStudentInfo/addFamilyNumInfo',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//学籍信息维护
export const studentStatusInfo = (params) => {
  return request({
    url: baseUrl+'/xySchoolRoll/querySysStudentInfoPageForAudit',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//管理员-学员管理-学籍注册列表
export const studentStatusList = (params) => {
  return request({
    url: baseUrl+'/sysStudentStatus/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍注册列表 -- 新增/修改
export const studentStatusSave = (params) => {
  return request({
    url: baseUrl+'/sysStudentStatus/saveOrUpdateStudentStatus',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍注册详情
export const studentStatusDetail = (params) => {
  return request({
    url: baseUrl+'/sysStudentStatus/getSingle',
    method: 'get',
    params:params
  }).then(res=>res.data)
}

//管理员-学员管理-学籍注册 批量删除
export const studentStatusDelete = (params) => {
  return request({
    url: baseUrl+'/sysStudentStatus/remove',
    method: 'delete',
    params:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍注册 免审核
export const studentStatusAudit = (params) => {
  return request({
    url: baseUrl+'/xySchoolRoll/auditSchoolRoll',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//查询学员队
export const getStudentDui = (params) => {
  return request({
    url: baseUrl+'/xpaasDept/studentDui/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍异动 列表
export const studentStatusDiffList = (params) => {
  return request({
    url: baseUrl+'/xyChangeInfo/querySysStudentInfoPage',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍异动 删除
export const studentStatusDiffDel = (params) => {
  return request({
    url: baseUrl+'/xyChangeInfo/remove',
    method: 'delete',
    params:params,
  }).then(res=>res.data)
}
//管理员-学员管理-学籍异动 单体数据查看
export const studentStatusDiffInfo = (params) => {
  return request({
    url: baseUrl+'/xyChangeInfo/getSingleXyChangeInfo',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍异动 新增
export const studentStatusDiffAdd = (params) => {
  return request({
    url: baseUrl+'/xyChangeInfo/addChangeInfo',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//管理员-学员管理-学籍异动 修改
export const studentStatusDiffUpdate = (params) => {
  return request({
    url: baseUrl+'/xyChangeInfo/updateChangeInfo',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//历史学员名册
export const studentHistory = (params) => {
  return request({
    url: baseUrl+'/sysHistoryStudentInfo/getHistoryStudentInfoList',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//删除历史学员
export const studentHistoryDel = (params) => {
  return request({
    url: baseUrl+'/sysHistoryStudentInfo/remove',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//查询毕业学员名册-列表
export const graduationStudentList = (params) => {
  return request({
    url: baseUrl+'/graduationStudent/graduationStudentStatistics',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询毕业学员名册-查询毕业学员名册详情
export const graduationStudentDetail = (params) => {
  return request({
    url: baseUrl+'/graduationStudent/graduationStudentDetail',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询毕业学员名册-查询毕业学员名册详情 毕业、结业、初始化处理
export const studentHistoryDelStatus = (params) => {
  return request({
    url: baseUrl+'/graduationStudent/operateGraduationStatus',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//查询毕业条件设置情况列表-列表
export const getSettingList = (params) => {
  return request({
    url: baseUrl+'/graduationConditions/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询毕业可选条件列表
export const getSettingCheck = (params) => {
  return request({
    url: baseUrl+'/graduationConditions/optional/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//设置毕业条件
export const getSettingSave = (params) => {
  return request({
    url: baseUrl+'/graduationConditions/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
//-查询授位条件设置情况列表
export const getSettingSWList = (params) => {
  return request({
    url: baseUrl+'/bachelorConditions/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 查询授位可选条件列表
export const getSettingSWCheck = (params) => {
  return request({
    url: baseUrl+'/bachelorConditions/optional/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
///设置授位条件列表
export const getSettingSWSave = (params) => {
  return request({
    url: baseUrl+'/bachelorConditions/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 查询授位学员汇总列表
export const getSWList = (params) => {
  return request({
    url: baseUrl+'/bachelor/summary/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询授位学员详情列表
export const getSWDetail = (params) => {
  return request({
    url: baseUrl+'/bachelor/detail/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//学员授位状态初始化
export const getSWStatusInit = (params) => {
  return request({
    url: baseUrl+'/bachelor/initialization',
    method: 'put',
    data:params
  }).then(res=>res.data)
}
//学员授位状态 --给选中学员授位
export const getSWStatus = (params) => {
  return request({
    url: baseUrl+'/bachelor/degree',
    method: 'put',
    data:params
  }).then(res=>res.data)
}
//查询教员列表
export const getTeacherList = (params) => {
  return request({
    url: baseUrl+'/sysTeacherInfo/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//查询教员信息
export const getTeacherDetail = (params) => {
  return request({
    url: baseUrl+'/sysTeacherInfo/detail',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 查询下级部门
export const getTeacherDepart = (params) => {
  return request({
    url: baseUrl+'/xpaasDept/querySubDept/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 获取系列表
export const getTeacherXiList = (params) => {
  return request({
    url: baseUrl+'/xpaasDept/faculty/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
// 获取教研室列表
export const getTeacherJYSList = (params) => {
  return request({
    url: baseUrl+'/xpaasDept/section/list',
    method: 'get',
    params:params
  }).then(res=>res.data)
}
//修改教员个人信息
export const updateTeacherBasic = (params) => {
  return request({
    url: baseUrl+'/sysTeacherInfo/update',
    method: 'put',
    data:params
  }).then(res=>res.data)
}
//修改教员教育背景
export const updateTeacherJYBJ = (params) => {
  return request({
    url: baseUrl+'/sysTeacherEducation/save',
    method: 'post',
    data:params
  }).then(res=>res.data)
}
// 获取学期信息
export const getXQList = (params) => {
  return request({
    url: baseUrl+'/sysTerm/getSysTerm',
    method: 'get',
    params:params
  }).then(res=>res.data)
}


