import request from '@/router/axios';

export const queryList = (planid) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/Querycourseno',
    method: 'get',
    params: {planid}
  })
}

export const queryPostList = (planid) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/Querycourseno',
      method: 'get',
      params: {planid}
    })
  }
export const query = (obj) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/updatecjstudent',
      method: 'post',
      data: obj
    })
  }
  export const queryzc = (obj) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/updatecjstudent',
      method: 'post',
      data: obj
    })
  }
  export const querybkzc = (row) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/updatecjstudentZC',
      method: 'post',
      data: row
    })
  }