import request from '@/router/axios';

export const queryList = () => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryAllUpBhkCourse',
    method: 'get',
  })
}
//考试安排首页
export const adminSelectExamplan = (current,size,xuqi) => {
  return request({
    url: '/api/xpaas-course-service/examplan/adminSelectExamplan',
    method: 'get',
    params:{ current,size,xuqi }
  })
}
export const conditionPostList = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jjQueryUpBhkCourse',
    method: 'post',
    data: row,
  })
}
//新建考试批次
export const examplanSave = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/save',
    method: 'post',
    data: row,
  })
}
//更新单条批次时间
export const examplanUpdate = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/update',
    method: 'post',
    data: row,
  })
}
//删除单条批次
export const examplanDel = (ids) => {
  return request({
    url: '/api/xpaas-course-service/examplan/remove',
    method: 'post',
    params: {
      ids
    }
  })
}
//开始排考页面数据
export const queryExaDetail = (current,size,kpId,row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/queryExaDetail',
    method: 'post',
    data: row,
    params: {current,size,kpId}
  })
}
//开始排考-考试数据统计页面数据
export const countExamplan = (current,size,jc,proClass) => {
  return request({
    url: '/api/xpaas-course-service/examplan/countExamplan',
    method: 'get',
    params:{ current,size,jc,proClass }
  })
}
//开始排考-考试数据统计页面数据
export const ksdatacount = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplan/ksdatacount',
    method: 'post',
    data:row
  })
}
//设置考试课程页面数据
export const selectAllCourse = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/selectAllCourse',
    method: 'post',
    data: row,
  })
}
//设置考试课程右侧表格数据保存
export const addExaCourses = (pId ,cId) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/addExaCourses',
    method: 'post',
    params:{ pId ,cId }
  })
}
//设置考试课程右侧表格删除单条
export const removePlanId = (id) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/removePlanId',
    method: 'get',
    params:{ id }
  })
}
//安排考试中获取所有被考试占用的节次
export const occupyDate = () => {
  return request({
    url: '/api/xpaas-course-service/examplan/occupyDate',
    method: 'get'
  })
}
//安排考试获取所有的场地数据
export const getPlaceInfoList = (cdmc,ssjzw,zwsTop,zwsLow) => {
  return request({
    url: '/api/xpaas-course-service/placeinfo/getPlaceInfoList',
    method: 'get',
    params:{ cdmc,ssjzw,zwsTop,zwsLow }
  })
}
//获取占用场地列表ID
export const occupyPlaceInfoListId = (date,jc) => {
  return request({
    url: '/api/xpaas-course-service/placeinfo/occupyPlaceInfoListId',
    method: 'get',
    params: { date,jc }
  })
}
//安排考试页面获取所有监考员数据
export const selectJobName = () => {
  return request({
    url: '/api/xpaas-home-service/teacherinfo/selectJobName',
    method: 'get'
  })
}
export const selectDetailsJob = (row) => {
  return request({
    url: '/api/xpaas-home-service/teacherinfo/selectDetailsJob',
    method: 'post',
    data: row
  })
}
//设置监考员弹框内所有的单位数据
export const getDeptList = () => {
  return request({
    url: '/api/xpaas-home-service/dept/getDeptList',
    method: 'get'
  })
}
//安排考试页面修改日期和节次
export const updateDatejc = (row) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/update',
    method: 'post',
    data: row
  })
}
//开始排考页面备注修改
export const updateRemark = (row) => {
  return request({
    url: '/api/xpaas-course-service/specialtyclassinfo/update',
    method: 'post',
    data: row
  })
}
//开始排考页面批量下发核对
export const updateStatus = (ids) => {
  return request({
    url: '/api/xpaas-course-service/examplancourse/updateStatus',
    method: 'get',
    params: { ids }
  })
}
//补缓考安排考试管理员-权限
export const selectCourseId = (ids) => {
  return request({
    url: '/api/xpaas-course-service/courselist/selectCourseId',
    method: 'get',
    params: { ids }
  })
}
//老师录入补缓考成绩数据
export const queryListl = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/QuerycourseJY',
    method: 'post',
    data: obj
  })
}
//老师录入补缓考成绩数据查询
export const conditionPostListl = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/QuerycourseJY',
    method: 'post',
    data: obj
  })
}
//教研室主任录入补缓考成绩
export const queryListj = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/QuerycourseJYSZR',
    method: 'post',
    data: obj
  })
}
//教研室主任录入补缓考成绩数据查询
export const conditionPostListj = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/QuerycourseJY',
    method: 'post',
    data: obj
  })
}
//系领导录入补缓考成绩数据
export const queryListx = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/QuerycourseXLD',
    method: 'post',
    data:obj
  })
}
//系领导录入补缓考
export const conditionPostListx = (courseno) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/Querycourseno',
    method: 'get',
    params: {courseno}
  })
}

//补缓考安排考试修改日期节次,考场监考员-管理员
export const updateDateAndSection = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courselist/updateDateAndSection',
    method: 'post',
    data:obj
  })
}

// 教研室主任打回教员
export const teaCheckNoPass = (aId,nRe) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/teaCheckNoPass',
    method: 'post',
    params:{
      aId,nRe
    }
  })
}