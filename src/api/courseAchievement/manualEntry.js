import request from '@/router/axios';

export const queryList = (planid) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryCjIetKc',
    method: 'post',
    params: {planid}
  })
}
export const queryPostList = (planid) => {
    return request({
      url: '/api/xpaas-course-service/scorequery/querystudentstate2',
      method: 'post',
      params: {planid}
    })
}
export const preservePostList = (row) => {
    return request({
      url: '/api/xpaas-course-service/scorequery/updatecjstudent',
      method: 'post',
      data:  row,
    })
  }
  export const queryLists = (planid) => {
    return request({
      url: '/api/xpaas-course-service/courseimplementplan/queryAllUpBhkCourseCs',
      method: 'post',
      params: {planid}
    })
  }
  export const queryPostLists = (planid) => {
      return request({
        url: '/api/xpaas-course-service/scorequery/querybhkstudent',
        method: 'post',
        params: {planid}
      })
  }
  //管理员第二次补缓考上
  export const queryListr = (planid) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/Querycoursenotwo',
      method: 'get',
      params: {planid}
    })
  }
  //管理员第二次补缓考下
  export const queryPostListr = (planid) => {
      return request({
        url: '/api/xpaas-course-service/bkstudentlist/Querycoursenotwo',
        method: 'get',
        params: {planid}
      })
  }
  //管理员第二次补缓考保存
  export const queryr = (arr) => {
    return request({
      url: '/api/xpaas-course-service/bkstudentlist/updatecjstudenttwo',
      method: 'post',
      data:  arr,
    })
  }
  //管理员暂存
  export const queryzc = (row) => {
    return request({
      url: '/api/xpaas-course-service/scorequery/updatecjstudentZC',
      method: 'post',
      data:  row,
    })
  }
    //管理员第二次暂存
    export const queryzc2 = (row) => {
      return request({
        url: '/api/xpaas-course-service/bkstudentlist/updatecjstudenttwoZC',
        method: 'post',
        data:  row,
      })
    }
// 获取已录入成绩
export const queryScoreList = (planId) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/txminputscorelist?planid='+planId,
    method: 'get'
  })
}
// 扫描条形码录入成绩
export const updateScore = (data) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/update',
    method: 'post',
    data: data
  })
}