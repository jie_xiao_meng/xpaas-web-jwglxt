import request from '@/router/axios'

//管理员成绩录入
//查询全部
export const scoreAll = () => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryAllStudentKc',
    method: 'get',
  })
}

//成绩录入(管理员)
export const scorequery = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKc',
    method: 'post',
    data:row,
  })
}
//成绩录入（教研室角色）
export const scoreTeachingAll = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcJYSZR',
    method: 'post',
    data:row,
  })
}
//成绩录入（教员角色）
export const scoreTeaAll = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jyqueryTjStudentKc',
    method: 'post',
    data:row,
  })
}
//成绩录入（系领导角色）
export const scoreTjStudentAll = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcXLD',
    method: 'post',
    data:row,
  })
}


//录入补缓考成绩单
//查询全部
export const scoreBuAll = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/Querycourse',
    method: 'post',
    data:obj
  })
}

//按条件查询(管理员第一次录入)
export const scoreBuquery = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jjQueryUpBhkCourse',
    method: 'post',
    data:row,
  })
}

//录入第二次补缓考成绩
//查询全部
export const saveBukaoTwo = (obj) => {
  return request({
    url: '/api/xpaas-course-service/bkstudentlist/Querycoursetwo',
    method: 'post',
    data:obj
  })
}

//按条件查询
export const jjsaveBukaoTwo = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jjQueryUpBhkCourseTwo',
    method: 'post',
    data:obj,
  })
}
