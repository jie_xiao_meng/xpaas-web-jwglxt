import request from '@/router/axios';
import row from "element-ui/packages/row/src/row";

export const getList = () => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryAllStudentKc',
    method: 'get',

  })
}
//老师录入课程成绩数据
export const getLists = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jyqueryTjStudentKc',
    method: 'post',
    data:obj

  })
}
//系领导录入课程成绩数据
export const getListxld = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcXLD',
    method: 'post',
    data:obj

  })
}
//系领导录入课程成绩数据查询
export const getListxldcx = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcXLD',
    method: 'post',
    data:obj

  })
}
//教研室主任录入课程成绩数据
export const getListj = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcJYSZR',
    method: 'post',
    data:obj

  })
}
export const conditionPostList = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKc',
    method: 'post',
    data:row,
  })
}
//老师录入课程成绩查询
export const chax = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/jyqueryTjStudentKc',
    method: 'post',
    data:obj,
  })
}
//教研室主任录入课程成绩查询
export const conditionPostListj = (obj) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcJYSZR',
    method: 'post',
    data:obj,
  })
}
export const conditionPostLists = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/queryTjStudentKcWlr',
    method: 'post',
    data:row,
  })
}

export const savePListss = (planid) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/outoverallrating',
    method: 'post',
    data: planid
  })
}

// export const savePListss = (planid) => {
//   return request({
//     url: '/api/scorequery/outoverallrating',
//     method: 'post',
//     data: planid
//   })
// }


// export const saveBKallCJD = (planid) => {
//   return request({
//     url: '/api/xpaas-course-service/scorequeryPre/outBKallCJD',
//     method: 'post',
//     data: planid
//   })
// }


// // 导出补考成绩单new
// export const saveBKallCJD = (row) => {
//   return request({
//     url: '/api/scorequery/outBKallCJD',
//     method: 'post',
//     data: row
//   })
// }


//分项
export const savefx = (row) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/outoverFxrating',
    method: 'post',
    data: row
  })
}

// export const savefx = (classesid,fx,list) => {
//   return request({
//     url: '/api/scorequery/outoverFxrating',
//     method: 'post',
//     data: {classesid,fx,list}
//   })
// }


//总评
export const saveover = (row) => {
  return request({
    url: '/api/xpaas-course-service/scorequery/outoverallrating',
    method: 'get',
    data: row
  })
}
// 学员模板导入提交
export const tjExcelonObject = (row) => {
  return request({
    url: '/api/xpaas-course-service/courseimplementplan/tjExcelonObject',
    method: 'post',
    data: row
  })
}

// export const saveover = (classesid,planid) => {
//   return request({
//     url: '/api/scorequery/outoverallrating',
//     method: 'get',
//     data: {classesid,planid}
//   })
// }

