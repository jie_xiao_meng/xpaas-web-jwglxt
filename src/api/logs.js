import request from '@/router/axios';

export const getUsualList = (current, size, params) => {
  return request({
    url: '/api/xpaas-log/usual/list',
    method: 'get',
    params: {
      current,
      size,
      ...params
    }
  })
}

export const getApiList = (current, size, params) => {
  return request({
    url: '/api/xpaas-log/api/list',
    method: 'get',
    params: {
      current,
      size,
      ...params
    }
  })
}

export const getErrorList = (current, size, params) => {
  return request({
    url: '/api/xpaas-log/error/list',
    method: 'get',
    params: {
      current,
      size,
      ...params
    }
  })
}


export const getUsualLogs = (id) => {
  return request({
    url: '/api/xpaas-log/usual/detail',
    method: 'get',
    params: {
      id,
    }
  })
}
export const getApiLogs = (id) => {
  return request({
    url: '/api/xpaas-log/api/detail',
    method: 'get',
    params: {
      id,
    }
  })
}
export const getErrorLogs = (id) => {
  return request({
    url: '/api/xpaas-log/error/detail',
    method: 'get',
    params: {
      id,
    }
  })
}

