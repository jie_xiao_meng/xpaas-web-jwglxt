import request from '@/router/axios';
export const gettestpaper
= (current,size,itempool ) => {
  return request({
    url: '/api/xpaas-course-service/testpaper/selectListPage',
    method: 'post',
    params:{ current,size},
    data:itempool
  })
}
//s删除试卷
export const deltestpaper
= (ids ) => {
  return request({
    url: '/api/xpaas-course-service/testpaper/remove',
    method: 'post',
    params:{ids}
  })
}
//试卷详情
export const getselectListPager
= (id) => {
  return request({
    url: '/api/xpaas-course-service/andtestpapers/selectByIdList',
    method: 'post',
    params:{id},
  })
}
//添加一随机添加
export const addqaustion
= (testquestion ) => {
  return request({
    url: '/api/xpaas-course-service/testquestion/selectSavasByTestquestion',
    method: 'post',
    data:testquestion ,
  })
}
//添加--手动添加
export const addsaveBatch
= (testquestion ) => {
  return request({
    url: '/api/xpaas-course-service/andtestpapers/saveBatch',
    method: 'post',
    data:testquestion ,
  })
}
