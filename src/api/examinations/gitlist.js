import request from '@/router/axios';
//试题库列表
export const getQuestionlist
  = (current,size,data) => {
    return request({
      url: '/api/xpaas-course-service/itempool/SelectlistPage',
      method: 'post',
      params: { current:current, size:size},
      data:{...data}
    })
  }
  //修改
  export const setQuestionupdate
  = (data) => {
    return request({
      url: '/api/xpaas-course-service/itempool/update',
      method: 'post',
      data: data
    })
  }
  //新增
  export const setQuestionSave
  = (data) => {
    return request({
      url: '/api/xpaas-course-service/itempool/save',
      method: 'post',
      data: data
    })
  }
//详情
export const getlistPage
  = (id) => {
    return request({
      url: '/api/xpaas-course-service/testquestion/listPageByIdAll',
      method: 'post',
      params: {id}
    })
  }
 // 试题分页
 export const gettestlist
  = (current,size,stkid) => {
    return request({
      url: '/api/xpaas-course-service/testquestion/list',
      method: 'get',
      params:{current,size,stkid}
    })
  }
  //删除试题
  export const scttestremove
  = (ids) => {
    return request({
      url: '/api/xpaas-course-service/testquestion/remove',
      method: 'post',
      params:{ids}
    })
  }
  export const setQuestionDetail
  = (data) => {
    return request({
      url: '/api/xpaas-course-service/itempool/detail',
      method: 'get',
      data: data
    })
  }
  //删除
  export const setQuestionmove
  = (ids) => {
    return request({
      url: '/api/xpaas-course-service/itempool/remove',
      method: 'post',
      params:{ids}
    })
  }
//根据id查找
export const getlistById
  = (id) => {
    return request({
      url: '/api/xpaas-course-service/itempool/getById',
      method: 'post',
      params:{id}
    })
  }
  //题库添加试题
  export const setaddlist
  = (testquestion ) => {
    return request({
      url: '/api/xpaas-course-service/testquestion/save',
      method: 'post',
      data:testquestion 
    })
  }
  //新增类型
 
  export const setaddquestiontype
  = (questiontype) => {
    return request({
      url: '/api/xpaas-course-service/questiontype/save',
      method: 'post',
      data:questiontype  
    })
  }
  //查询所有题型
  export const getquestiontypeall
  = () => {
    return request({
      url: '/api/xpaas-course-service/questiontype/ByAll',
      method: 'get',
    
    })
  }
  //查询所有章节

  export const getchaptersByAll
  = () => {
    return request({
      url: '/api/xpaas-course-service/chapters/ByAll',
      method: 'get',
    })
  }
  //通过id查找章节
  
  export const getchaptersById
  = (id) => {
    return request({
      url: '/api/xpaas-course-service/chapters/ById',
      method: 'post',
      params:{id}
    })
  }
  //删除章节
  export const SetremoveZJ
  = (ids) => {
    return request({
      url: '/api/xpaas-course-service/chapters/remove',
      method: 'post',
      params:{ids}
    })
  }
  //新增章节
export const Setreesave
= (data) => {
  return request({
    url: '/api/xpaas-course-service/chapters/save',
    method: 'post',
    data:data
  })
}

export const selectByTestquestion
= (testquestion ) => {
  return request({
    url: '/api/xpaas-course-service/testquestion/selectByTestquestion',
    method: 'post',
    data:testquestion 
  })
}