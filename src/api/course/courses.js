import request from "@/router/axios";
//左边树状(课程库管理)
export const sz = (parentId) => {
  return request({
    url: "/api/xpaas-system/dept/lazy-list",
    method: "get",
    params: { parentId },
  });
};
//右边数据上
export const kkms = (deptName, teachingClassName) => {
  return request({
    url: "/api/xpaas-plan-service/courseinfo/querycoursnum",
    method: "get",
    params: { deptName, teachingClassName },
  });
};

// 课程库管理 ----/xpaas-system/dict-biz/dictionary?code=zhaoshengbuxi
export const reqCourseTree = (code) => {
  return request({
    url: "/api/xpaas-system/dict-biz/dictionary",
    method: "get",
    code,
  });
};
// 课程库管理 ----获奖课程维护  GET courseawarddetails/queryhjcourse
export const reqAwardCourseList = (
  pageNo,
  pageSize,
  courseName,
  hjmc,
  hjnf
) => {
  return request({
    url: "/api/xpaas-plan-service/courseawarddetails/queryhjcourse",
    method: "get",
    params: {
      pageNo,
      pageSize,
      courseName,
      hjmc,
      hjnf,
    },
  });
};
// 课程库管理 ----添加获奖课程  /courseawarddetails/save
export const reqAddAwardList = (data) => {
  return request({
    url: "/api/xpaas-plan-service/courseawarddetails/save",
    method: "post",
    data,
  });
};
// 课程库管理 ----修改课程
export const updateAddCourseList = (data) => {
  return request({
    url: "/api/xpaas-plan-service/courseinfo/update",
    method: "post",
    data,
  });
};
// 课程库管理 ----编辑  /courseawarddetails/queryhjcoursebyid
export const reqGetEditData = (id) => {
  return request({
    url: "/api/xpaas-plan-service/courseawarddetails/queryhjcoursebyid",
    method: "get",
    params: { id },
  });
};
// 课程库管理 ----编辑保存 /courseawarddetails/update
export const reqUpdateData = (data) => {
  return request({
    url: "/api/xpaas-plan-service/courseawarddetails/update",
    method: "post",
    data,
  });
};
// 课程库管理 ----删除 /courseawarddetails/remove
export const reqDeleteCourse = (ids) => {
  return request({
    url: "/api/xpaas-plan-service/courseawarddetails/remove",
    method: "post",
    params: { ids },
  });
};

// 课程库管理 ----课程设置（树） /api/xpaas-system/dept/lazy-list?parentId=0
export const reqgetTree = (parentId) => {
  return request({
    url: " /api/xpaas-system/dept/lazy-list",
    method: "get",
    params: { parentId },
  });
};
// 课程库管理 ----课程设置（树） 通过树节点查询表数据  /courseinfo/querycourseallbynum
export const reqGetTreeTableData = (
  eduLevel,
  bx,
  teachingClassName,
  pageNo,
  pageSize
) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseallbynum",
    method: "get",
    params: { eduLevel, bx, teachingClassName, pageNo, pageSize },
  });
};
export const reqGetTreeTableDatas = (teachingClassName, pageNo, pageSize) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseallset",
    method: "get",
    params: { teachingClassName, pageNo, pageSize },
  });
};
// 课程库管理 ----课程设置（树） 通过树节点查询表数据  /courseinfo/querycourseallbynum
export const reqGetTreeTableDatahj = (teachingClassName, pageNo, pageSize) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseallbyhj",
    method: "get",
    params: { teachingClassName, pageNo, pageSize },
  });
};
// 课程库管理 ----/courseinfo/querycoursnum
export const reqGetTreeTableList = (deptName, teachingClassName) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycoursnum",
    method: "get",
    params: { deptName, teachingClassName },
  });
};
// 课程库管理 ----点击开课门数查询下表数据/courseinfo/querycourseallbynum
export const reqCheckBottomTableList = (
  pageNo,
  pageSize,
  bx,
  eduLevel,
  teachingClassName
) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseallbynum",
    method: "get",
    params: { pageNo, pageSize, bx, eduLevel, teachingClassName },
  });
};
// 课程库管理 ----删除 /courseinfo/remove
export const reqDeleteBottomList = (ids) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/remove",
    method: "post",
    params: { ids },
  });
};
// 课程库管理 ----查询 /courseinfo/querycourseinfoTj
export const reqCheckBottomData = (
  pageNo,
  pageSize,
  xueqi,
  courseCode,
  courseName,
  subjectType
) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseinfoTj",
    method: "get",
    params: { pageNo, pageSize, xueqi, courseCode, courseName, subjectType },
  });
};
// 课程库管理 ----编辑 /courseinfo/querycourseinfobyid
export const reqeditById = (id) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/querycourseinfobyid",
    method: "get",
    params: { id },
  });
};


//通过课程编号查询课程
export const selectKCBH = (kcbm) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/selectKCBH",
    method: "get",
    params: { kcbm },
  });
};

// 添加课程 ------ 课程负责人teacherinfo/selectDetailsJob  /teacherinfo/getteacherinfolistteacher
export const reqCheckCourseTeacher = (name) => {
  return request({
    url: " /api/xpaas-home-service/teacherinfo/getteacherinfolistteacher",
    method: "get",
    params: { name },
  });
};
// 添加课程 ------ 课程分类  /coursetype/querycoursetype
export const reqCheckAllCourse = (typelevel) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/querycoursetype",
    method: "get",
    params: { typelevel }
  });
};
// 添加课程 ------ 课程分类--新增  /coursetype/save
export const reqAddNewRow = (data) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/save",
    method: "post",
    data,
  });
};
// 添加课程 ------ 课程分类---删除  /coursetype/remove
export const reqDeleteNewRow = (ids) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/remove",
    method: "post",
    params: { ids },
  });
};
// 添加课程 ------ 课程分类---重命名  /coursetype/update
export const reqSaveNewName = (data) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/update",
    method: "post",
    data,
  });
};
// 添加课程 ------ 课程分类---排序 /coursetype/querycoursetypetjbfl
export const reqSortRow = (data) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/querycoursetypetjbfl",
    method: "post",
    data,
  });
};
// 添加课程 ------ 课程分类--- coursetype/updatepaixu
export const reqSortRowp = (data) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/updatepaixu",
    method: "post",
    data,
  });
};
// 课程分类设置 --- 禁用启用状态修改
export const stateupdate = (id, qyzt,typedata) => {
  return request({
    url: " /api/xpaas-plan-service/coursetype/stateupdate",
    method: "post",
    params: { id, qyzt ,typedata},
  });
};
// 添加课程 ------ 文件上传/courseinfo/uploadFileCourse
export const reqUpload = (data) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/uploadFileCourse",
    method: "post",
    data,
  });
};
// 选择教员 ---- 点击树查询/teacherinfo/getteacherinfolistforteachroom
export const reqClickTreeCheckLeftTable = (teachroom, name) => {
  return request({
    url: " /api/xpaas-home-service/teacherinfo/getteacherinfolistforteachroom",
    method: "get",
    params: { teachroom, name },
  });
};
// 添加课程分类---保存/courseinfo/save
export const reqAddCourseList = (data) => {
  return request({
    url: " /api/xpaas-plan-service/courseinfo/save",
    method: "post",
    data,
  });
};
// 添加课程分类---课程分类  /dept/queryclassroom
export const reqCourseList = (data) => {
  return request({
    url: " /api/xpaas-plan-service/dept/queryclassroom",
    method: "post",
    data,
  });
};

export const setjyscx = (code) => {
  return request({
    url: "/api/xpaas-system/dict-biz/list",
    method: "get",
    params: { code },
  });
};

// 课程成绩单打印 搜索条件获取学期 check.js
// 页面数据
export const transcriptPrint = (data) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/adminquerycourseinput",
    method: "post",
    data,
  });
};
// 课程负责单位下拉框 --- 获取所有系部
export const getxb = () => {
  return request({
    url: " /api/xpaas-home-service/dept/getxb",
    method: "get",
  });
};
// 获取所有教研室
export const getClass = () => {
  return request({
    url: " /api/xpaas-home-service/dept/querytclassroom",
    method: "get",
  });
};
//根据系部获取对应的教研室
export const queryXBclassroom = (deptname) => {
  return request({
    url: " /api/xpaas-home-service/dept/querytclassroombyx",
    method: "get",
    params: { deptname },
  });
};
//查询大队
export const querydd = () => {
  return request({
    url: " /api/xpaas-home-service/dept/querydb",
    method: "get",
  });
};
// 导出分项成绩单
export const outoverFxrating = (classid1, fx, planid1) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outoverFxrating",
    method: "get",
    params: { classid1, fx, planid1 },
    responseType: "blob",
  });
};
// 学期接口
export const reqGetXqList = () => {
  return request({
    url: "/api/xpaas-home-service/term/getCurrentTermAll",
    method: "get",
  });
};

/* 成绩打印 */
//导出总评成绩单
export const outoverallrating = (classid1, planid1) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outoverallrating",
    method: "post",
    params: { classid1, planid1 },
    responseType: "blob",
  });
};
//导出分项成绩PDF
export const outoverFxratingforpdf = (classid1, fx, planid1) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outoverFxratingforpdf",
    method: "get",
    params: { classid1, fx, planid1 },
    responseType: "blob",
  });
};
//导出总评成绩PDF
export const outoverallratingforpdf = (classid1, planid1) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outoverallratingforpdf",
    method: "post",
    params: { classid1, planid1 },
    responseType: "blob",
  });
};
//导出补考成绩单 --- 打印
export const outBKallCJD = (planids) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outBKallCJD",
    method: "get",
    params: { planids },
    responseType: "blob",
  });
};
//二次补考---导出
export const outBKallCJDTwo = (planid1) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/outBKallCJDTwo",
    method: "get",
    params: { planid1 },
    responseType: "blob",
  });
};
/* 成绩打印 */

//模板导入课程成绩---下载模板
export const downLoadScoreExcel = (planid) => {
  return request({
    url: " /api/xpaas-course-service/scorequery/downLoadExcel",
    method: "get",
    params: { planid },
    responseType: "blob",
  });
};
//学员成绩单打印 --- 打印
export const outputstudent = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/outputstudentcj",
    method: "post",
    data,
    responseType: "blob",
  });
};

//专业班次成绩单打印
export const zybcscroeprint = (data) => {
  return request({
    url: "/api/xpaas-course-service/scorequery/zybcscroeprint",
    method: "post",
    data,
  });
};
// 教学任务审核衔接课程考核数据
export const createDataForCourse = (data) => {
  return request({
    url: "/api/xpaas-course-service/courseimplementplan/insertshujubyshixun",
    method: "post",
    data: data
  })
}
