import request from '@/router/axios';

export const postList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/personList',
    method: 'post',
    data: row
  }) 
}
export const abspostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/check',
    method: 'post',
    data: row
  }) 
}
export const getdepartmentOf = () => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getStaffRoom',
    method: 'post',
  
  }) 
}
export const getcheckStatus = () => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getCheckStatus',
    method: 'post',
  
  }) 
}
export const gethasCommonts = () => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getHasCommonts',
    method: 'post',
  
  }) 
}

export const reqGetXqList = () => {
  return request({
    url: "/api/xpaas-home-service/term/getCurrentTermAll",
    method: "get"
  });
};

export const getAllClassHour = () => {
  return request({
    url: '/api/xpaas-plan-service/sxksbzset/queryall',
    method: 'post',
  })
}
export const setClassHour = (data) => {
  return request({
    url: '/api/xpaas-plan-service/sxksbzset/update',
    method: 'post',
    data: data
  })
}
export const getClassByYear = () => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/queryyearleftmenu",
    method: "get"
  })
}
export const getSpecialtyList = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querysppecltybcbyzyid',
    method: 'post',
    data: data
  })
}
//新增或修改
export const submitSpecialty = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/submit',
    method: 'post',
    data: data
  })
}
// 点击修改或者详情回显的数据
export const querysppecltyId = (id) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querysppecltybcbyid',
    method: 'post',
    params: {id}
  })
}
//单个或者批量删除班次
export const queryRemoveId = (ids) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/remove',
    method: 'post',
    params: {ids}
  })
}
//班学员
export const queryclassstuenttjmessage = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/queryclassstuenttjmessage',
    method: 'post',
    data: data
  })
}
//现在学员
export const querystudentinfo = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfo',
    method: 'post',
    data: data
  })
}
//转专业
export const queryzhuanzyinfo = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/queryzhuanzyinfo',
    method: 'post',
    data: data
  })
}
//退学员
export const querystudentinfotx = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfotx',
    method: 'post',
    data: data
  })
}
//处罚学员
export const querystudentinfcf = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfcf',
    method: 'post',
    data: data
  })
}
//奖励学员
export const querystudentinfjl = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfjl',
    method: 'post',
    data: data
  })
}
//降级学员
export const querystudentinfojj = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfojj',
    method: 'post',
    data: data
  })
}
//转学员
export const querystudentinfoz = (data) => {
  return request({
    url: '/api/xpaas-plan-service/specialtyclassinfo/querystudentinfoz',
    method: 'post',
    data: data
  })
}

export const getClassRoomTree = (codeType) => {
  return request({
    url: "/api/xpaas-plan-service/dictbiz/selectdgplace",
    method: "post",
    params: {
      code: codeType
    }
  })
}
export const getDeptTree = () => {
  return request({
    url: "/api/xpaas-home-service/dept/selectdgplace",
    method: "post"
  })
}

//新增弹框内条件查询场地
export const tjqueryplaceinfo = (data) => {
  return request({
    url: '/api/xpaas-plan-service/placeinfo/tjqueryplaceinfo',
    method: 'post',
    data: data
  })
}
//批量专业班次
export const reqSpeByYear = (year) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/choosezylistbyyear?Year=" + year,
    method: "get"
  })
}
export const batchSave = (data) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyclassinfo/save",
    method: "post",
    data: data
  })
}

export const placelike = (data) => {
  return request({
    url: "/api/xpaas-plan-service/placeinfo/selectdgplacelike",
    method: "post",
    data: data
  })
}
