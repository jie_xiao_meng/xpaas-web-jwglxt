import request from '@/router/axios';

export const queryPostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/personList',
    method: 'post',
    data:row,
  })
}
export const getJxx = () => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getDf',
    method: 'post',

  })
}
export const getJys = () => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getStaffRoom',
    method: 'post',

  })
}
export const getXq = () => {
  return request({
    url: '/api/xpaas-home-service/term/getCurrentTermAll',
    method: 'get'
  })
}
export const getJysByJxx = (deptname) => {
  return request({
    url: '/api/xpaas-home-service/dept/querytclassroombyx',
    method: 'get',
    params: {deptname}
  })
}
