import request from '@/router/axios';

export const queryPostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/personOne',
    method: 'post',
    data:row,
  }) 
}
export const delPostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/remove',
    method: 'post',
    data:row,
  }) 
}
export const addPostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/saveorupdate',
    method: 'post',
    data:row,
  }) 
}
//获取承担任务
export const updatePostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getDicOfUndertakeTasks',
    method: 'post',
    data:row,
  }) 
}
//获取实装/实弹
export const upsetPostList = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getDicOfAmmunitionEquipment',
    method: 'post',
    data:row,
  }) 
}

//获取实装/实弹
export const getTeacher = (row) => {
  return request({
    url: '/api/xpaas-training-service/coursehourenterdetails/getTeacher',
    method: 'post',
    data:row,
  }) 
}