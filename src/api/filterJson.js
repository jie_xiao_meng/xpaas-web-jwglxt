import { getList as getUserList } from "@/api/system/user";
import { getDeptLazyTree } from "@/api/system/dept";
export const filterJsonTo = function (obj, objN) {
    let result;
    if (typeof obj === "object") {
        if (Array.isArray(obj)) {
            result = [];
            for (let i in obj) {
                result.push(filterJsonTo(obj[i]));
            }
        } else if (obj === null) {
            result = null;
        } else if (obj.constructor === RegExp) {
            result = obj;
        } else {
            result = {};
            for (let i in obj) {
                result[i] = filterJsonTo(obj[i], i);
            }
        }
    } else if (obj === "treeLoad") {
        result = function (node, resolve) {
            const parentId = node.level === 0 ? 0 : node.data.id;
            getDeptLazyTree(parentId).then((res) => {
                resolve(
                    res.data.data.map((item) => {
                        return {
                            ...item,
                            leaf: !item.hasChildren,
                        };
                    })
                );
            });
        };
    } else if (obj === "onLoad") {
        result = ({ page, value, data }, callback, param) => {
            if (page) {
                getUserList(page.currentPage, page.pageSize, data, param).then(
                    (res) => {
                        let data = res.data.data;
                        data.data = data.records;
                        callback(data);
                    }
                );
            }
        };
    } else {
        result = obj;
    }

    return result;
}
export const filterToJson = function (obj, objN) {
    let result;
    if (typeof obj === "object") {
        if (Array.isArray(obj)) {
            result = [];
            for (let i in obj) {
                result.push(filterToJson(obj[i]));
            }
        } else if (obj === null) {
            result = null;
        } else if (obj.constructor === RegExp) {
            result = obj;
        } else {
            result = {};
            for (let i in obj) {
                result[i] = filterToJson(obj[i], i);
            }
        }
    } else if (typeof obj == "function") {
        if (objN === "treeLoad") {
            result = "treeLoad";
        } else if (objN === "onLoad") {
            result = "onLoad";
        }
    } else {
        result = obj;
    }
    return result;
}