import request from "@/router/axios";

//管理员
//指标体系
//查询全部的标准等级
export const pjall = () => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/ByAll",
    method: "post"
  });
};

//删除标准等级
export const pjdelete = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/remove",
    method: "post",
    params: { ids }
  });
};

//标准体系
//查询全部标准体系
export const pjalller = () => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/ByAllRankAndGuide",
    method: "get"
  });
};

//删除标准体系
export const pjalllerdelete = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/remove",
    method: "post",
    params: { ids }
  });
};

//查看全部跳转
export const pjguideall = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/ByIdRankAndGuide",
    method: "post",
    params: { id }
  });
};
export const pjguideall1 = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/Bydetail",
    method: "post",
    params: { id }
  });
};

//查看上一级指标名称
export const IdUpOneLv = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/IdUpOneLv",
    method: "post",
    params: { id }
  });
};

//编辑标准体系
//先查看再修改
export const pjByIdGuide1 = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/ByIdRankAndGuide",
    method: "post",
    params: { id }
  });
};

//查看所有标准等级
export const pjguidesave = () => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/ByAll",
    method: "post"
  });
};

//添加标准体系
export const pjguideadd = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/submit",
    method: "post",
    data: row
  });
};

//调整标准体系
export const pjTiaozhengsave = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/submit",
    method: "post",
    data: row
  });
};

//查询设置参评对象
export const pjevall = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/ByAll",
    method: "post",
    data: row
  });
};

//批量审核
export const pjupByid = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/updatestate",
    method: "post",
    // data: ids
    params:{ ids }
  });
};
//审核驳回
export const updatestateNo = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/updatestateNo",
    method: "get",
    params: { ids }
  });
};

//发起评教信息核对
export const submitallput = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/updatestateAll",
    method: "get",
    params: { ids }
  });
};

//维护
//查看全部维护信息
export const pjitemBydetail = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/paixu",
    method: "post",
    params: { id }
  });
};
//查看一条维护信息
export const pjitemBydetail1 = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/ByDetailId",
    method: "post",
    params: { id }
  });
};
//删除维护信息
export const pjitemremove = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/remove",
    method: "get",
    params: { ids }
  });
};
//编辑维护信息
export const pjitemupdate = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/update",
    method: "post",
    data: row
  });
};
//添加维护信息
export const pjitemsave = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/save",
    method: "post",
    data: row
  });
};

//调整维护信息

//根据id查询参评对象
export const pjitemEval = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/ByIdEval",
    method: "post",
    data: row
  });
};

//修改保存
export const UpdateIdDesc = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalactor/UpdateIdDesc",
    method: "post",
    data: row
  });
};

//查看引用
export const pjevalByid = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalguide/Byid",
    method: "post",
    params: { id }
  });
};

//查看指定的标准等级
export const pjguiderankById = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/ById",
    method: "post",
    params: { id }
  });
};

//修改标准等级保存
export const pjguiderankupdate = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/update",
    method: "post",
    data: row
  });
};
export const submit = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguiderank/submit",
    method: "post",
    data: row
  });
};

//查看创建教学活动
export const pjevalByAll = time => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalproject/ByAll',
    method: 'post',
    params:{time}
  })
}
//编辑创建教学活动
export const bjevalByAll = (id) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalproject/selectById',
    method: 'get',
    params:{id}
  })
}

//创建教学活动评价
export const pjevalinsert = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/insert",
    method: "post",
    data: row
  });
};

//删除评价活动
export const pjevalremove = ids => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/remove",
    method: "post",
    params: { ids }
  });
};

//查询专业课
export const pjselectZuanYe = (current,size,row) => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/selectZuanYe",
    method: "post",
    data: row,
    params: { current,size }
  });
};

//查看评教结果（系参谋）
export const pjselectResult = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByObjectStudenIdYes2",
    method: "post",
    data: row
  });
};

// 通过参评对象ID查看评教结果报告
export const getByIdResult = (current,id,size) => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/getByIdResult",
    method: "post",
    params:{ current,id,size }
  });
};

// 授权角色权限
export const getSaveResult = (evalactorId,userIds) => {
  return request({
    url: "/api/xpaas-evaluate-service/evalactoruser/save",
    method: "post",
    params:{ evalactorId,userIds}
  });
};

//查询专业课通识课系领导
export const pjselectZuanYeXLD = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/selectZuanYeXLD",
    method: "post",
    data: row
  });
};

//查询平均分
export const pjselectAug = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/selectAug",
    method: "post",
    data: row
  });
};
//查询教员平均得分
export const pjselectAug1 = (current,size,row) => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/selectAug1",
    method: "post",
    params: { current,size },
    data: row
  });
};
//查询教员平均得分--系领导
export const pjselectAugXLD = (current,size,row) => {
  return request({
    url: "/api/xpaas-evaluate-service/pjresult/selectAug1XLD",
    method: "post",
    params: { current,size },
    data: row
  });
};

//学员评教
//查看已评价学员
export const queryallyes = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByObjectStudenIdYes",
    method: "post",
    data: row
  });
};

//查看未评价学员
export const queryallno = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByObjectStudenIdNo",
    method: "post",
    data: row
  });
};
//查看个性化指标（XXXXX）
export const selectIndex = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedmultiplet/selectIndex",
    method: "post",
    data: row
  });
};
//查看个性化指标
export const toSelectResultByObjectId = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedmultiplet/selectResultByObjectId",
    method: "post",
    data: row
  });
};
export const selectIndexs = data => {
  return request({
    url:
      "/api/xpaas-evaluate-service//pjindividualizedmultiplet/selectByEvalojectIdAll",
    method: "post",
    data
  });
};

//查询选择题
export const selectByTorId = data => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedmultiplet/selectByTorId",
    method: "get",
    params: data
  });
};
//添加选择题
export const inserts = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedmultiplet/inserts",
    method: "post",
    data: row
  });
};

//查询问答题
export const pjindividualizedqatselectByTorId = data => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedqat/selectByTorId",
    method: "get",
    params: data
  });
};
//添加问答题
export const pjindividualizedqatinserts = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjindividualizedqat/inserts",
    method: "post",
    data: row
  });
};

//查看评教结果查看
export const selectByObjectStudenIdYes2 = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByObjectStudenIdYes2",
    method: "post",
    data: row
  });
};

//整体分析（分数段）
export const selectByObjectFenXi = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByObjectFenXi",
    method: "post",
    data: row
  });
};

//整体分析（通识课 专业课）
export const selectByCourseFenXi = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/selectByCourseFenXi",
    method: "post",
    data: row
  });
};

//

//编辑期中教学评价活动先查询
export const pjevalprojectById = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/ById",
    method: "get",
    params: { id }
  });
};

//评教结果分析发布
export const upto = term => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/publish",
    method: "get",
    params: { term }
  });
};

//导入评教报告
export const pjevalprojectupload = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/upload",
    method: "post",
    data: row
  });
};
//删除评教报告
export const pjevalprojectRemoveupload = row => {
  return request({
    url: "/api/xpaas-evaluate-service/pjevalproject/Removeupload",
    method: "post",
    data: row
  });
};

//查看下级顺序
export const ByNeLvIdALL = id => {
  return request({
    url: "/api/xpaas-evaluate-service/pjguideitem/ByNeLvIdALL",
    method: "post",
    params: { id }
  });
};
export const updateinfoSelectAll = Trem => {
  return request({
    url: "/api/xpaas-evaluate-service/pjauthority/selectByTerm",
    method: "get",
    params: { Trem }
  });
};

//调整指标项的等级
export const updateShunXu = row => {
  return request({
    url: '/api/xpaas-evaluate-service/pjguideitem/updateShunXu',
    method: 'post',
    data:row
  })
}
export const srcPjguideadd = () => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalguide/ByAll',
    method: 'post',
  
  })
}
export const srcSaves = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/evaluatebatch/save',
    method: 'post',
    data:row  
  })
}
export const userSaves = () => {
  return request({
    url: '/api/xpaas-home-service/teacherinfo/getCurrentOneEntity',
    method: 'get',
    
  })
}
export const srcAbcd = () => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/toAdd',
    method: 'post',

  })
}
export const saveGroup = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/doSave',
    method: 'post',
    data:row
  })
}
export const selectGroup = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/toPersonList',
    method: 'post',
    data:row
  })
}
export const yueGroup = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/termBindMonth',
    method: 'post',
    data:row
  })
}
export const editGroup = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/toEditDetails',
    method: 'post',
    data:row
  })
}

export const deletGroup = (id) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/doDelete',
    method: 'get',
    params:{id}
  })
}
export const selectGroug = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/specialsupervisioninfo/toList',
    method: 'post',
    data:row
  })
}
export const seleGroug = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/statisticTeacherScore',
    method: 'post',
    data:row
  })
}
export const eleGroup = (row, query) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/statisticByDdzj',
    method: 'post',
    data:row,
    params: query
  })
}


export const saverGroup = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisionauth/auth',
    method: 'post',
    data:row
  })
}
//列表
export const getlist = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisionauth/toList',
    method: 'post',
    data:row
  })
}
//列表关闭操作
export const guan = (ids) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisionauth/remove',
    method: 'get',
    params:{ids}
  })
}
//管理员督导评教月份分析
export const fenxi = (obj) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/statisticByTimeAndDept',
    method: 'post',
    data:obj
  })
}
export const dbselct = () => {
  return request({
    url: "/api/xpaas-home-service/dept/querydb",
    method: "get"
  });
}

export const fetchUnevaluatedCourses = (obj) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/queryevluatedudaotonghang',
    method: 'post',
    data:obj
  })
}

export const updateSzhzBatch = (obj) => {
  return request({
    url: '/api/xpaas-evaluate-service/szhz/plupdate',
    method: 'post',
    data:obj
  })
}

//根据日期和教员id查询课程
export const selectPaike = (obj) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/paike/selectPaike',
    method: 'post',
    data:obj
  })
}


// 新增授权权限
export const evalactoruserS = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/evalactoruser/save',
    method: 'post',
    data:row
  })
}

// 通过EvaltorID找到这条数据授予了哪些用户
export const SelectUserByEID = (EVALACTORID) => {
  return request({
    url: '/api/xpaas-evaluate-service/evalactoruser/SelectUserByEID',
    method: 'get',
    params:{ EVALACTORID }
  })
}

// 查找当前使用的督导标准体系
export const getEvaluateBatch = () => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/evaluatebatch/getEvaluateBatch',
    method: 'get'
  })
}


// 推送消息
export const toSelectListByID = () => {
  return request({
    url: '/api/xpaas-evaluate-service/evalactoruser/SelectListByID',
    method: 'get',
  })
}


// 推送消息 --- 信息
export const toSelectList = () => {
  return request({
    url: '/api/xpaas-evaluate-service/evalactoruser/selectList',
    method: 'get',
  })
}



// 调整监考申请记录 --->> 审核通过
export const toTeaCheckPass = (aId) => {
  return request({
    url: '/api/xpaas-course-service/examtechercheck/teaCheckPass',
    method: 'post',
    params: {
      aId
    }
  })
}






