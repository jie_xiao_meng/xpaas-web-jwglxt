import request from '@/router/axios';

//教员查询到所有数据
export const supervisionlist = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/employee',
    method: 'get',
    data:row
  })
}


//填报督导信息（未完成）
export const supervisionAdd = () => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/save',
    method: 'post',
  })
}


//专项督导信息查看
export const  pjlist= (supervisorid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjspesupervisorinformation/pjlist',
    method: 'post',
    params: {
      supervisorid,
    }
  })
}


//手动录入上课信息
export const addcourse = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjcourseinformation/addInformation',
    method: 'post',
    data:row
  })
}


//系督导结果查看
export const supervisionteam = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/empleader',
    method: 'get',
    data:row
  })
}


//按教员统计（平均分）
export const supteamsta = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/employee',
    method: 'get',
    data:row
  })
}


//教员评教信息核对（查询）
export const  pjevalactor= (id) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/ById',
    method: 'post',
    params: {
      id
    }
  })
}

//教员评教信息核对(提交)
export const  pjesub= (eid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/updateById',
    method: 'post',
    params: {eid}
  })
}

//教员评教信息核对(修改)
export const  pjupdate= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/sysuserdetails/selectLike' ,
    method: 'post',
    data:row
  })
}

//修改参评对象
export const UpdateID = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/UpdateID',
    method: 'post',
    data:row
  })
}

//评教信息核对提交
export const submitID = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/updateById',
    method: 'post',
    data:row
  })
}


//教员评教信息核对(修改留言)
export const  textupdatesave= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjcheck/AddOrUpdateByIdText',
    method: 'post',
    data:row
  })
}

//删除留言
export const  txtdelete= (checkid) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjcheck/TextRemove',
    method: 'post',
    params: {checkid}
  })
}

//添加留言
export const  addText= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjcheck/AddOrUpdateByIdText',
    method: 'post',
    data:row
  })
}

//督导信息统计
export const  statistical= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/SelectAll',
    method: 'post',
    data:row,
  })
}


//系督导结果查看
export const supquery = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/empleader',
    method: 'get',
    data:row
  })
}


//按教员统计（平均分）
export const supqueryavg = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/employee',
    method: 'get',
    data:row
  })
}

//按督导专家分析
export const  stat= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/SelectAll',
    method: 'post',
    data:row,
  })
}

//按教员查询
export const  spequery= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjsupervisorinformation/SelectAll',
    method: 'post',
    data:row,
  })
}
//按教员查询
export const  spequerySelect = (row) => {
  return request({
    url: '/api/xpaas-evaluate-service-lwb/supervisioninfo/SelectAll',
    method: 'post',
    data:row,
  })
}


//查看参评课程
export const  elipj= (id) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/ByIdCourse2',
    method: 'post',
    params: {id}
  })
}

//从排课结果中生成参评对象
export const SaveEvalactor = (current,size,xueqi) => {
  return request({
    url: '/api/xpaas-sx/sh/list1',
    method: 'get',
    params:{current,size,xueqi}
  })
}


//设置个性化指标
export const  elipjevalactor= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/UpdateById',
    method: 'post',
    data:row
  })
}

//导出Excel
export const  excel= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjresult/Pjprint',
    method: 'post',
    data:row
  })
}


//查询所有角色的内容
export const  ByIdJueSe= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/ByIdJueSe',
    method: 'post',
    data:row
  })
}

//提交审核
export const  updateById= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/updateById',
    method: 'post',
    data:row
  })
}

//打回审核
export const  dahuijuese= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/dahuijuese',
    method: 'post',
    data:row,
  })
}

//打回确定
export const  ubohuipdateById= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjevalactor/ubohuipdateById',
    method: 'post',
    data:row,
  })
}


//授予角色
export const  pjauthoritysave= (row) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjauthority/save',
    method: 'post',
    data:row,
  })
}


//删除角色授权
export const  pjauthorityremove= (ids) => {
  return request({
    url: '/api/xpaas-evaluate-service/pjauthority/remove',
    method: 'post',
    params: {ids}
  })
}
