import request from '@/router/axios';

export const loginByUsername = (tenantId, username, password, type, key, code) => request({
    url: '/api/xpaas-auth/oauth/token',
    method: 'post',
    headers: {
        'Tenant-Id': tenantId,
        'Captcha-Key': key,
        'Captcha-Code': code,
    },
    params: {
        tenantId,
        username,
        password,
        grant_type: "captcha",
        scope: "all",
        type
    }
});

export const refreshToken = (refresh_token, tenantId) => request({
    url: '/api/xpaas-auth/oauth/token',
    method: 'post',
    headers: {
        'Tenant-Id': tenantId
    },
    params: {
        tenantId,
        refresh_token,
        grant_type: "refresh_token",
        scope: "all",
    }
});

export const getButtons = () => request({
    url: '/api/xpaas-system/menu/buttons',
    method: 'get'
});
export const getComponents = () => request({
  url: '/api/xpaas-system/menu/components',
  method: 'get'
});
export const getCaptcha = () => request({
    url: '/api/xpaas-auth/oauth/captcha',
    method: 'get'
});

// export const logout = () => request({
//     url: '/api/xpaas-auth/oauth/logout',
//     method: 'get'
// });

export const getUserInfo = (token) => request({
    url: '/api/xpaas-auth/oauth/user-info',
    method: 'get'
});

// export const sendLogs = (list) => request({
//     url: '/api/xpaas-auth/oauth/logout',
//     method: 'post',
//     data: list
// });
// 获取主题
export const getForum = (list) => {
    return request({
        url: '/api/bbs-model/column/forumTheme',
        method: 'get',
        params: list
    })
};
// 获取 我创建的主题
export const getMyForum = (list) => {
    return request({
        url: '/api/bbs-model/column/myTheme',
        method: 'get',
        params: list
    })
};
// 热门主题列表  /api/bbs-model/column/hotTheme
export const getHotForum = (list) => request({
    url: '/api/bbs-model/column/hotTheme',
    method: 'post',
    data: list
});
// hotColumn 热门版块
export const getHotColumn = (list) => request({
    url: '/api/bbs-model/column/hotColumn',
    method: 'get',
    data: list
});
// columnTheme 获取版块和主题列表
export const getColumnTheme = (list) => request({
    url: '/api/bbs-model/column/columnTheme',
    method: 'get',
    data: list
});
// getPostedList 获取发帖榜列表
export const getPostedList = (list) => request({
    url: '/api/bbs-model/theme/selectPostRank',
    method: 'get',
    data: list
});
// myDate 获取版块和主题列表
export const getMyDate = (list) => request({
    url: '/api/bbs-model/column/myDate',
    method: 'get',
    data: list
});
// 获取回复列表 myReply
export const getMyReply = (size) => request({
    url: '/api/bbs-model/column/myReply',
    method: 'get',
    params: size
});
// 获取查看回复 repltMore
export const getRepltMore = (params, current, size) => {
    return request({
        url: '/api/bbs-model/column/repltMore',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
};
// 获取回复列表 remove
export const getRemove = (list) => request({
    url: '/api/bbs-model/reply/remove',
    method: 'post',
    params: list
});
// 创建主题 save
export const getSave = (list) => {
    console.log(list)
    return request({
        url: '/api/bbs-model/column/saveTheme',
        method: 'post',
        data: list
    })
};
// xpaas-system/dept/tree 获取发布范围
export const getTree = (list) => request({
    url: '/api/xpaas-system/dept/tree',
    method: 'get',
    data: list
});
// theemeView 查看详情
export const getTheemeView = (list) => request({
    url: '/api/bbs-model/column/theemeView',
    method: 'get',
    params: list
});
//saveReply 评论回复
export const getSaveReply = (list) => request({
    url: '/api/bbs-model/column/saveReply',
    method: 'get',
    params: list
});
export const getSaveFabulousNumber = (list) => request({
    url: '/api/bbs-model/likerecord/saveFabulousNumber',
    method: 'get',
    params: list
});
export const getRemoveFabulousNumber = (list) => request({
    url: '/api/bbs-model/likerecord/removeFabulousNumber',
    method: 'get',
    params: list
});

// 我的创建列表
export const getMyTheme = (current, size, params) => {
    return request({
        url: '/api/bbs-model/column/myTheme',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

//我的创建删除
export const getCreateRemove = (ids) => request({
    url: '/api/bbs-model/theme/remove',
    method: 'post',
    params: {
        ids
    }
});

//我的创建添加
export const add = (row) => {
    return request({
        url: '/api/bbs-model/theme/submit',
        method: 'post',
        data: row
    })
}

//我的创建编辑
export const update = (row) => {
    return request({
        url: '/api/bbs-model/theme/submit',
        method: 'post',
        data: row
    })
}
//获取论坛的轮播图
export const pictureMap = (list) => request({
    url: '/api/bbs-model/theme/pictureMap',
    method: 'get',
    data: list
});
export const userRole = (userId) => {
  return request({
    url: '/api/xpaas-user/userRole',
    method: 'get',
    params: {
      userId,
    }
  })
}
