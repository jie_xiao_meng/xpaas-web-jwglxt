import request from '@/router/axios';



export const getSelectById = (id) => {
  return request({
    url: '/api/xpaas-course-service/mingtiplan/selectById',
    method: 'post',
    params: {
      id,
    }
  })
}

export const getSubmit = (row) => {
  return request({
    url: '/api/xpaas-course-service/mingtiplan/submit',
    method: 'post',
    data:row
  })
}

// 试卷和命题审批管理
export const updateStatus = (row) => {
  return request({
    url: '/api/xpaas-course-service/testpaperandprint/updateStatus',
    method: 'post',
    data:row
  })
}

