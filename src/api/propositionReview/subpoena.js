import request from '@/router/axios';



export const getSelectById = (id) => {
  return request({
    url: '/api/xpaas-course-service/sendprinting/selectById',
    method: 'post',
    params: {
      id,
    }
  })
}
export const getSubmit = (row) => {
  return request({
    url: '/api/xpaas-course-service/sendprinting/submit',
    method: 'post',
    data:row
  })
}

export const SelectById = (obj) => {
  return request({
    url: '/api/xpaas-course-service/sexamination/querybyid',
    method: 'post',
    data: obj
  })
}
export const sasgetSubmit = (obj) => {
  return request({
    url: '/api/xpaas-course-service/sexamination/downloadWorld',
    method: 'post',
    data: obj
  })
}