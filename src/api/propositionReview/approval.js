import request from '@/router/axios';


export const getSelectCondition = (row) => {
    return request({
        url: '/api/xpaas-course-service/testpaperandprint/selectCondition',
        method: 'post',
        data: row
    })
}

export const getSelectJob = (row) => {

    return request({
        url: '/api/xpaas-home-service/teacherinfo/selectJob',
        method: 'post',
        data: row
    })

}
// 培养方案管理 --- 管理员
export const getConditionList = (pageNo, pageSize, bb, pyLevel) => {
    return request({
        url: "/api/xpaas-plan-service/testtrainingprograminfo/getConditionList",
        method: "get",
        params: { pageNo, pageSize, bb, pyLevel }
    });
};
export const detail = id => {
    return request({
        url: "/api/xpaas-plan-service/testtrainingprograminfo/detail",
        method: "get",
        params: {
            id
        }
    });
};

export const submit = obj => {
    return request({
        url: "/api/xpaas-plan-service/testtrainingprograminfo/submit",
        method: "post",
        data: obj
    });
};
export const remove = ids => {
    return request({
        url: "/api/xpaas-plan-service/testtrainingprograminfo/remove",
        method: "post",
        params: {
            ids
        }
    });
};
// 试用专业 /yearspecialtyinfo/choosezylist
export const reqChoosezylist = () => {
    return request({
        url: "/api/xpaas-plan-service/yearspecialtyinfo/choosezylist",
        method: "get"
    });
};
export const getUpdate = (row) => {
    return request({
        url: '/api/xpaas-course-service/testpaperandprint/update',
        method: 'post',
        data: row
    })
}
export const getUpload = (row) => {
    return request({
        url: '/api/xpaas-course-service/testpaperandprint/upload',
        method: 'post',
        data: row
    })
}
export const getDelFile = (row) => {
    return request({
        url: '/api/xpaas-course-service/testpaperandprint/delFile',
        method: 'post',
        data: row
    })
}
export const getDownload = (id, position) => {
    return request({
        url: '/api/xpaas-course-service/testpaperandprint/download',
        method: 'get',
        params: {
            id,
            position
        }
    })
}
//课程设置 保存接口 /testtrainingprograminfo/setCourseSave
export const reqsetCourseSave = data => {
    return request({
      url: "/api/xpaas-plan-service/testtrainingprograminfo/setCourseSave",
      method: "post",
      data
    });
  };
  //人才培养方案首页点击课程设置
  export const setCourse = id => {
    return request({
      url: "/api/xpaas-plan-service/testtrainingprograminfo/setCourse",
      method: "get",
      params: {
        id
      }
    });
  };
  // /testtrainingprograminfo/getprograminfobyxueqi
  export const termView = id => {
    return request({
      url: "/api/xpaas-plan-service/testtrainingprograminfo/getprograminfobyxueqi",
      method: "post",
      params: { id }
    });
  };

    //新增人才培养方案
    export const reqAddnewTraining = data => {
        return request({
            url: "/api/xpaas-plan-service/testtrainingprograminfo/save",
            method: "post",
            data
        });
    };
    //编辑人才培养方案
    export const reqUpdateTraining = data => {
        return request({
            url: "/api/xpaas-plan-service/testtrainingprograminfo/update",
            method: "post",
            data
        });
    };

    //批量删除人才培养方案
    export const reqRemoveTraining = ids => {
        return request({
            url: "/api/xpaas-plan-service/testtrainingprograminfo/removepl",
            method: "post",
            params: {
                ids
            }
        });
    };

    //人才培养方案新增中上传附件
    export const getUploadTrain = (id,row) => {
        return request({
            url: '/api/xpaas-plan-service/testtrainingprograminfo/uploadFile',
            method: 'post',
            params:{ id },
            data: row,
        })
    }
    
    //人才培养方案新增中获取最顶级分类
    export const queryFirstcoursetype = (typedata) => {
        return request({
            url: '/api/xpaas-plan-service/coursetype/querycoursetypetjbflmax',
            method: 'get',
            params:{
                typedata
            }
        })
    }
    // 课程设置单个删除
    export const reqRemoveSetting = ids => {
        return request({
            url: "/api/xpaas-plan-service/trainingprogramcourse/remove",
            method: "post",
            params: {
                ids
            }
        });
    };

    //人才培养课程设置-新增课程查询
    export const trainCourseSetNew = (
        coursenum,
        coursename,current,size
    ) => {
        return request({
        url: " /api/xpaas-plan-service/courseinfo/querycourseallsettj",
        method: "get",
        params: {coursenum , coursename,current,size }
        });
    };
    //点击父级加载子级数据
    export const szChild = (pageNo,pageSize,teachingClassName)  => {
        return request({
        url: "/api/xpaas-plan-service/courseinfo/querycourseallset",
        method: "get",
        params: { pageNo,pageSize,teachingClassName  }
        });
    };
    //培养方案执行 -- 查看详情
    export const querycoursetypefu = id  => {
        return request({
        url: "/api/xpaas-plan-service/coursetype/querycoursetypefu",
        method: "get",
        params: {
            id
        }
        });
    };
    //培养方案管理首页查询 --- 版本下拉框
    export const querybb = () => {
        return request({
            url: '/api/xpaas-plan-service/testtrainingprograminfo/querybb',
            method: 'get'
        })
    }
    
