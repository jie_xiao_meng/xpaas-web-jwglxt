import request from '@/router/axios';



export const getSelectById = (id) => {
  return request({
    url: '/api/xpaas-course-service/shijuanfenxi/selectById',
    method: 'post',
    params: {
      id,
    }
  })
}
export const getSubmit = (row) => {
  return request({
    url: '/api/xpaas-course-service/shijuanfenxi/submit',
    method: 'post',
    data:row
  })
}
