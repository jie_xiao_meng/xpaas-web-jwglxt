import request from '@/router/axios';



export const getSelectById = (id) => {
  return request({
    url: '/api/xpaas-course-service/sendprinting/selectById',
    method: 'post',
    params: {
      id,
    }
  })
}
export const getSubmit = (row) => {
  return request({
    url: '/api/xpaas-course-service/sendprinting/submit',
    method: 'post',
    data:row
  })
}
