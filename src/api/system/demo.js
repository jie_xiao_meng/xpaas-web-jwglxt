import request from '@/router/axios';
export const getSelectWeek = () => {
  return request({
    url: '/api/xpaas-course-service/timetable/selectWeek',
    method: 'post',
    params: {
    }
  })
}
