import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/xpaas-system/role/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
export const grantTree = () => {
  return request({
    url: '/api/xpaas-system/menu/grant-tree',
    method: 'get',
  })
}
// /menu/thirdGrantTree 1.第三方  获取权限分配树形结构
export const thirdGrantTree = (ids, topId) => {
  return request({
    url: '/api/xpaas-system/menu/thirdGrantTree',
    method: 'get',
    params: {
      ids,
      topId
    }
  })
}
// /role/thirdGrant 第三方角色菜单授权
export const thirdGrant = (row) => {
  console.log(row)
  return request({
    url: '/api/xpaas-system/role/thirdGrant',
    method: 'post',
    data: row
  })
}
// /role/echoThirdGrant  3.第三方 角色菜单回显
export const echoThirdGrant = (topId,roleId) => {
  return request({
    url: '/api/xpaas-system/role/echoThirdGrant',
    method: 'Post',
    data: {
      topId,
      roleId
    }
  })
}

export const grant = (roleIds, menuIds, dataScopeIds, apiScopeIds) => {
  return request({
    url: '/api/xpaas-system/role/grant',
    method: 'post',
    data: {
      roleIds,
      menuIds,
      dataScopeIds,
      apiScopeIds
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/xpaas-system/role/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/xpaas-system/role/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/xpaas-system/role/submit',
    method: 'post',
    data: row
  })
}


export const getRole = (roleIds) => {
  return request({
    url: '/api/xpaas-system/menu/role-tree-keys',
    method: 'get',
    params: {
      roleIds,
    }
  })
}

export const getRoleTree = (tenantId) => {
  return request({
    url: '/api/xpaas-system/role/tree',
    method: 'get',
    params: {
      tenantId,
    }
  })
}
// 角色顶部菜单回显 /role/echoRoleTopMenu/
export const getEchoRoleTopMenu = (roleIds) => {
  return request({
    url: '/api/xpaas-system/role/echoRoleTopMenu/' + roleIds,
    method: 'get',
  })
}
// 角色添加顶部菜单 /role/roleBindTopmenus
export const getRoleBindTopmenus = (row) => {
  console.log(row)
  return request({
    url: '/api/xpaas-system/role/roleBindTopmenus',
    method: 'post',
    data: row
  })
}
