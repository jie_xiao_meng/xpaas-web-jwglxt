import request from '@/router/axios';

export const getList = (current, size, params, deptId) => {
  return request({
    url: '/api/xpaas-user/page',
    method: 'get',
    params: {
      ...params,
      current,
      size,
      deptId,
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/xpaas-user/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/xpaas-user/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/xpaas-user/update',
    method: 'post',
    data: row
  })
}

export const getUser = (id) => {
  return request({
    url: '/api/xpaas-user/detail',
    method: 'get',
    params: {
      id,
    }
  })
}

export const getUserInfo = () => {
  return request({
    url: '/api/xpaas-user/info',
    method: 'get',
  })
}
export const getUserInfoSso = () => {
  return request({
    url: '/api/xpaas-user/info-sso',
    method: 'get'
  })
}
export const listDic = () => {
  return request({
    url: '/api/xpaas-user/listDic',
    method: 'get'
  })
}
export const userDic = (userIds) => {
  return request({
    url: '/api/xpaas-user/userDic',
    method: 'get',
    params: {
      userIds,
    }
  })
}
export const resetPassword = (userIds) => {
  return request({
    url: '/api/xpaas-user/reset-password',
    method: 'post',
    params: {
      userIds,
    }
  })
}

export const updatePassword = (oldPassword, newPassword, newPassword1) => {
  return request({
    url: '/api/xpaas-user/update-password',
    method: 'post',
    params: {
      oldPassword,
      newPassword,
      newPassword1,
    }
  })
}
// /api/xpaas-resource/oss/endpoint/put-file
export const putFile = (file) => {
  console.log(file, "请求接口")
  return request({
    url: '/api/xpaas-resource/oss/endpoint/put-file',
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundarygTBBNBtPFuJAGL6q',
      // 'Authorization': 'Basic eHBhYXN1aTp4cGFhc3VpX3NlY3JldA=='
    },
    data: file
  })
}
// 根据 多个部门 获取user分页
export const getListByDepts = (current, size, params, deptIds) => {
  return request({
    url: '/api/xpaas-user/findPageUserByDept',
    method: 'get',
    params: {
      ...params,
      current,
      size,
      deptIds,
    }
  })
}
export const userRole = (userId) => {
  return request({
    url: '/api/xpaas-user/userRole',
    method: 'get',
    params: {
      userId,
    }
  })
}

