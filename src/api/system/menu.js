import request from '@/router/axios';

export const getList = (current, size, params) => {
    return request({
        url: '/api/xpaas-system/menu/list',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

export const getLazyList = (parentId, params) => {
    return request({
        url: '/api/xpaas-system/menu/lazy-list',
        method: 'get',
        params: {
            ...params,
            parentId
        }
    })
}
// /menu/thirdMenuList 第三方系统菜单列表
export const getThirdMenuList = (parentId, params) => {
    return request({
        url: '/api/xpaas-system/menu/thirdMenuList',
        method: 'get',
        params: {
            ...params,
            parentId
        }
    })
}

export const getLazyMenuList = (parentId, params) => {
    return request({
        url: '/api/xpaas-system/menu/lazy-menu-list',
        method: 'get',
        params: {
            ...params,
            parentId
        }
    })
}

export const getMenuList = (current, size, params) => {
    return request({
        url: '/api/xpaas-system/menu/menu-list',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

export const remove = (ids) => {
    return request({
        url: '/api/xpaas-system/menu/remove',
        method: 'post',
        params: {
            ids,
        }
    })
}
// 第三方删除 /menu/thirdRemove
export const thirdRemove = (ids, topId) => {
    return request({
        url: '/api/xpaas-system/menu/thirdRemove',
        method: 'post',
        params: {
            ids,
            topId
        }
    })
}

export const add = (row) => {
    return request({
        url: '/api/xpaas-system/menu/submit',
        method: 'post',
        data: row
    })
}
// 第三方菜单添加 /menu/addThirdMenu
export const addThirdMenu = (row) => {
    console.log(row)
    return request({
        url: '/api/xpaas-system/menu/addThirdMenu',
        method: 'post',
        params: row
    })
}

export const update = (row) => {
    return request({
        url: '/api/xpaas-system/menu/submit',
        method: 'post',
        data: row
    })
}
// /menu/editThirdMenu 第三方菜单编辑
export const editThirdMenu = (row) => {
    return request({
        url: '/api/xpaas-system/menu/editThirdMenu',
        method: 'post',
        params: row
    })
}

export const getMenu = (id) => {
    return request({
        url: '/api/xpaas-system/menu/detail',
        method: 'get',
        params: {
            id,
        }
    })
}

// 获取顶部菜单树
export const getTopMenu = () => request({
    // url: '/api/xpaas-system/menu/top-menu',
    url: '/api/xpaas-system/topmenu/grantTopTree',
    method: 'get'
});

export const getRoutes = (topMenuId) => request({
    url: '/api/xpaas-system/menu/routes',
    method: 'get',
    params: {
        topMenuId,
    }
});
// 封装 三层  树结构 接口
export const getWebRouters = (topMenuId) => request({
  url: '/api/xpaas-system/menu/getWebRouter',
  method: 'get',
  params: {
    topMenuId,
  }
});
export const getWebRouter = (topMenuId) => request({
    url: '/api/xpaas-system/topmenu/grantTopTree',
    method: 'get',
    params: {
        topMenuId,
    }
});
