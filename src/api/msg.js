import { Message } from 'element-ui'
export default  (x, res) =>{
    if (res.data.code == 200) {
      Message({
        message: x + '成功',
        type: 'success',
      })
    } else {
      Message({
        message: x + '失败',
        type: 'error',
      })
    }
  }