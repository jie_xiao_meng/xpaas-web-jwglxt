import request from "@/router/axios";
//专业详情
export const getSelectConditionPrint = (obj) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/queryspecialty",
    method: "post",
    data: obj,
  });
};
//承训专业任务
export const getlist = () => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/queryspecialtyLevel",
    method: "get",
  });
};
//新增培养层次保存
export const xzpyccsave = (obj) => {
  return request({
    url: "/api/xpaas-system/dict-biz/submit",
    method: "post",
    data: obj,
  });
};
//添加专业
export const tjzy = (obj) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/save",
    method: "post",
    data: obj,
  });
};
//删除培养层次
export const scpycc = (obj) => {
  return request({
    url: "/api/xpaas-system/dict-biz/remove",
    method: "post",
    params: obj,
  });
};
//删除专业详情
export const sczyxq = (obj) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/remove",
    method: "post",
    params: obj,
  });
};
//编辑专业详情保存
export const bjzyxqsave = (obj) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/update",
    method: "post",
    data: obj,
  });
};
//年度开设专业
export const ndkszy = () => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/queryyearkszy",
    method: "get",
  });
};
//设置招生专业(年度开设专业)
export const szzszy = () => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/queryallspecialty",
    method: "get",
  });
};
// 右侧已选的专业列表
export const queryrightspectily = (year) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/queryrightspectily",
    method: "get",
    params:{year}
  });
};
//设置学期信息(年度开设专业)
export const wc = (arr) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/save",
    method: "post",
    data: arr,
  });
};
//年度开设专业查询(年度开设专业)
export const ndkszycx = (current,size,obj) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/queryyearspecialty",
    method: "post",
    params:{ current,size },
    data: obj
  });
};
//修改年度开设专业(年度开设专业)
export const xgndkszy = (obj) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/update",
    method: "post",
    data: obj,
  });
};
//年度开设专业删除按钮(年度开设专业)
export const scndkszy = (id) => {
  return request({
    url: "/api/xpaas-plan-service/yearspecialtyinfo/remove",
    method: "post",
    params: { id },
  });
};
//培养层次更新
export const updatedPycc = (row) => {
  return request({
    url: "/api/xpaas-plan-service/dictbiz/update",
    method: "post",
    data: row,
  });
};
//培养层次新增
export const setPycc = (row) => {
  return request({
    url: "/api/xpaas-plan-service/dictbiz/save",
    method: "post",
    data: row,
  });
};
//年度开设专业删除按钮(年度开设专业)
export const delRemove = (ids) => {
  return request({
    url: "/api/xpaas-plan-service/dictbiz/remove",
    method: "post",
    params: { ids },
  });
};

export const slePro = (row) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/update",
    method: "post",
    data: row,
  });
};

export const savePro = (row) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/save",
    method: "post",
    data: row,
  });
};
export const removePro = (ids) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/remove",
    method: "post",
    params: { ids },
  });
};

export const pyccselect = (code) => {
  return request({
    url: "/api/xpaas-system/dict-biz/list",
    method: "get",
    params: { code },
  });
};

export const cdyqselect = (parentId) => {
  return request({
    url: "/api/xpaas-system/dept/lazy-list",
    method: "get",
    params: { parentId },
  });
};
export const ndselect = () => {
  return request({
    url: "/api/xpaas-plan-service/systerm/queryallteam",
    method: "get",
  });
};
export const zsxbselset = () => {
  return request({
    url: "/api/xpaas-system/dept/list?deptCategory=6",
    method: "get",
  });
};

export const queryClassReport = (pageno, pagesize) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyclassinfo/queryspecticytongji",
    method: "get",
    params: { pageno, pagesize },
  });
};


export const getSpeclist = (current,size,row) => {
  return request({
    url: "/api/xpaas-plan-service/specialtyinfo/SelectCcYq",
    method: "post",
    params: { current,size },
    data: row
  });
};