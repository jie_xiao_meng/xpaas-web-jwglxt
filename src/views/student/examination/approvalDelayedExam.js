import request from '@/router/axios';
// 课程考核与实施计划


export const getSelectCondition = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/selectCondition',
    method: 'post',
    data: row
  })
}
export const addMessage = (row) => {
  return request({
    url: '/api/xpaas-course-service/shspeed/addMessage',
    method: 'post',
    data: row
  })
}
export const updateStatus = (row) => {
  return request({
    url: '/api/xpaas-course-service/deferexam/updateStatus',
    method: 'post',
    data: row
  })
}

export const updateMessage = (row) => {
  return request({
    url: '/api/xpaas-course-service/shspeed/updateMessage',
    method: 'post',
    data: row
  })
}
