import Mock from 'mockjs'

const top = [{
        label: "首页",
        path: "/systemPages/chiefSpace",
        icon: 'el-icon-menu',
        meta: {
            i18n: 'dashboard',
        },
        parentId: 0
    },
    {
        label: "xqwl官网",
        icon: 'el-icon-document',
        meta: {
            i18n: 'website',
        },
        path: "",
        parentId: 1
    },
    {
        label: "xq官网",
        icon: 'el-icon-document',
        meta: {
            i18n: 'avuexwebsite',
        },
        path: "",
        parentId: 2
    },
    {
        label: "测试",
        icon: 'el-icon-document',
        path: "/test/index",
        meta: {
            i18n: 'test',
        },
        parentId: 3
    }
]
export default ({ mock }) => {
    if (!mock) return;
    Mock.mock('/user/getTopMenu', 'get', () => {
        return {
            data: top
        }
    })

}