import { initenv } from '../public/cdn/envjs.js';
initenv();
import Vue from 'vue';
import axios from './router/axios';
import VueAxios from 'vue-axios';
import App from './App';
import router from './router/router';
import './permission'; // 权限
import './error'; // 日志
import './cache'; //页面缓存
import store from './store';
import SSO from './util/sso.js';
import can from './util/can.js';
// import {loadStyle} from './util/util'
import * as urls from '@/config/env';
import Element from 'element-ui';
import * as commonApi from './util/commonData.js'
Vue.prototype.commonApi = commonApi
// import {
//   iconfontUrl,
//   iconfontVersion
// } from '@/config/env';
import './styles/init_customize.css'
import i18n from './lang' // Internationalization
import "@/styles/index.less";
import './styles/common.scss';
import Avue from "@smallwei/avue";
import "@smallwei/avue/lib/index.css";
import basicBlock from './components/basic-block/main'
import basicContainer from './components/basic-container/main'
import AvueUeditor from 'avue-plugin-ueditor'

import CKEditor from 'ckeditor4-vue';

import VueDND from 'awe-dnd' //元素拖动
import JsonExcel from 'vue-json-excel'
Vue.use( CKEditor );
Vue.use(VueDND)
Vue.use(Avue);
Vue.prototype.$can = can;
Vue.use(AvueUeditor)
Vue.use(router)
Vue.use(VueAxios, axios)
    // Vue.use(avueFormDesign)
Vue.use(Element, {
    i18n: (key, value) => i18n.t(key, value)
})
Vue.use(window.AVUE, {
        i18n: (key, value) => i18n.t(key, value)
    })
    //注册全局容器
Vue.component('basicContainer', basicContainer)
Vue.component('basicBlock', basicBlock)
    // 加载相关url地址
Object.keys(urls).forEach(key => {
    Vue.prototype[key] = urls[key];
})
Vue.component('downloadExcel', JsonExcel)

// 动态加载阿里云字体库
// iconfontVersion.forEach(ele => {
//   loadStyle(iconfontUrl.replace('$key', ele));
// })

Vue.config.productionTip = false;
SSO.init(() => {
    main();
});

function main() {
    new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    }).$mount('#app')
}
