import jq from "./jquery/jquery-3.5.1.min.js"

export const initenv = function getdata() {
  window._CONFIG = JSON.parse( jq.ajax( {
    url : "/cdn/env.json",
    async : false,
    dataType : "json",
  } ).responseText )[process.env.NODE_ENV];
}
